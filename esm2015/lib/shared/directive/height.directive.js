import { Directive, Input } from '@angular/core';
import * as i0 from "@angular/core";
export class HeightDirective {
    constructor(elRef) {
        this.elRef = elRef;
    }
    ngAfterViewInit() {
        this.elRef.nativeElement.style.height = this.height + 'px';
    }
}
HeightDirective.ɵfac = function HeightDirective_Factory(t) { return new (t || HeightDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
HeightDirective.ɵdir = i0.ɵɵdefineDirective({ type: HeightDirective, selectors: [["", "appHeight", ""]], inputs: { height: "height" } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(HeightDirective, [{
        type: Directive,
        args: [{
                selector: '[appHeight]',
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { height: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVpZ2h0LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL2hlaWdodC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYyxLQUFLLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQUs1RSxNQUFNLE9BQU8sZUFBZTtJQU0xQixZQUFvQixLQUFpQjtRQUFqQixVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQUcsQ0FBQztJQUV6QyxlQUFlO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUM3RCxDQUFDOzs4RUFWVSxlQUFlO29EQUFmLGVBQWU7a0RBQWYsZUFBZTtjQUgzQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGFBQWE7YUFDeEI7O2tCQUtFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIElucHV0LCBBZnRlclZpZXdJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1thcHBIZWlnaHRdJyxcbn0pXG5leHBvcnQgY2xhc3MgSGVpZ2h0RGlyZWN0aXZlIGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XG4gIC8qKlxuICAgKiBAaGVpZ2h0IC0gdSBweFxuICAgKi9cbiAgQElucHV0KCkgaGVpZ2h0OiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge31cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LnN0eWxlLmhlaWdodCA9IHRoaXMuaGVpZ2h0ICsgJ3B4JztcbiAgfVxufVxuIl19