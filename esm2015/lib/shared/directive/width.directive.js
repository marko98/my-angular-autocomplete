import { Directive, Input, } from '@angular/core';
import * as i0 from "@angular/core";
export class WidthDirective {
    constructor(elRef) {
        this.elRef = elRef;
    }
    ngAfterViewInit() {
        this.elRef.nativeElement.style.width = this.width + 'vw';
    }
}
WidthDirective.ɵfac = function WidthDirective_Factory(t) { return new (t || WidthDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
WidthDirective.ɵdir = i0.ɵɵdefineDirective({ type: WidthDirective, selectors: [["", "appWidth", ""]], inputs: { width: "width" } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(WidthDirective, [{
        type: Directive,
        args: [{
                selector: '[appWidth]',
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { width: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkdGguZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYXV0b2NvbXBsZXRlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmUvd2lkdGguZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsS0FBSyxHQUlOLE1BQU0sZUFBZSxDQUFDOztBQUt2QixNQUFNLE9BQU8sY0FBYztJQU16QixZQUFvQixLQUFpQjtRQUFqQixVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQUcsQ0FBQztJQUV6QyxlQUFlO1FBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUMzRCxDQUFDOzs0RUFWVSxjQUFjO21EQUFkLGNBQWM7a0RBQWQsY0FBYztjQUgxQixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7YUFDdkI7O2tCQUtFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBEaXJlY3RpdmUsXG4gIElucHV0LFxuICBPbkluaXQsXG4gIEFmdGVyVmlld0luaXQsXG4gIEVsZW1lbnRSZWYsXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbYXBwV2lkdGhdJyxcbn0pXG5leHBvcnQgY2xhc3MgV2lkdGhEaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcbiAgLyoqXG4gICAqIEB3aWR0aCAtIHUgdndcbiAgICovXG4gIEBJbnB1dCgpIHdpZHRoOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge31cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LnN0eWxlLndpZHRoID0gdGhpcy53aWR0aCArICd2dyc7XG4gIH1cbn1cbiJdfQ==