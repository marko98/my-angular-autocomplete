import { Directive, Input } from '@angular/core';
import * as gsap from 'gsap';
import * as i0 from "@angular/core";
export class OpacityTransitionDirective {
    constructor(_el) {
        this._el = _el;
        this.appOpacityDuration = 1;
        this._timeline = new gsap.TimelineMax();
    }
    ngAfterViewInit() {
        this._timeline.fromTo(this._el.nativeElement, this.appOpacityDuration, { opacity: 0 }, {
            opacity: 1,
            onComplete: () => {
                // console.log('animacija gotova :)');
            },
        });
    }
}
OpacityTransitionDirective.ɵfac = function OpacityTransitionDirective_Factory(t) { return new (t || OpacityTransitionDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
OpacityTransitionDirective.ɵdir = i0.ɵɵdefineDirective({ type: OpacityTransitionDirective, selectors: [["", "appOpacityTransition", ""]], inputs: { appOpacityDuration: "appOpacityDuration" } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(OpacityTransitionDirective, [{
        type: Directive,
        args: [{
                selector: '[appOpacityTransition]',
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { appOpacityDuration: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BhY2l0eS10cmFuc2l0aW9uLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL29wYWNpdHktdHJhbnNpdGlvbi5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBNkIsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sS0FBSyxJQUFJLE1BQU0sTUFBTSxDQUFDOztBQUs3QixNQUFNLE9BQU8sMEJBQTBCO0lBSXJDLFlBQW9CLEdBQWU7UUFBZixRQUFHLEdBQUgsR0FBRyxDQUFZO1FBSDFCLHVCQUFrQixHQUFXLENBQUMsQ0FBQztRQUNoQyxjQUFTLEdBQUcsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFFTCxDQUFDO0lBRXZDLGVBQWU7UUFDYixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQ3RCLElBQUksQ0FBQyxrQkFBa0IsRUFDdkIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQ2Q7WUFDRSxPQUFPLEVBQUUsQ0FBQztZQUNWLFVBQVUsRUFBRSxHQUFHLEVBQUU7Z0JBQ2Ysc0NBQXNDO1lBQ3hDLENBQUM7U0FDRixDQUNGLENBQUM7SUFDSixDQUFDOztvR0FsQlUsMEJBQTBCOytEQUExQiwwQkFBMEI7a0RBQTFCLDBCQUEwQjtjQUh0QyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjthQUNuQzs7a0JBRUUsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgQWZ0ZXJWaWV3SW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIGdzYXAgZnJvbSAnZ3NhcCc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1thcHBPcGFjaXR5VHJhbnNpdGlvbl0nLFxufSlcbmV4cG9ydCBjbGFzcyBPcGFjaXR5VHJhbnNpdGlvbkRpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuICBASW5wdXQoKSBhcHBPcGFjaXR5RHVyYXRpb246IG51bWJlciA9IDE7XG4gIHByaXZhdGUgX3RpbWVsaW5lID0gbmV3IGdzYXAuVGltZWxpbmVNYXgoKTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbDogRWxlbWVudFJlZikge31cblxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fdGltZWxpbmUuZnJvbVRvKFxuICAgICAgdGhpcy5fZWwubmF0aXZlRWxlbWVudCxcbiAgICAgIHRoaXMuYXBwT3BhY2l0eUR1cmF0aW9uLFxuICAgICAgeyBvcGFjaXR5OiAwIH0sXG4gICAgICB7XG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIG9uQ29tcGxldGU6ICgpID0+IHtcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZygnYW5pbWFjaWphIGdvdG92YSA6KScpO1xuICAgICAgICB9LFxuICAgICAgfVxuICAgICk7XG4gIH1cbn1cbiJdfQ==