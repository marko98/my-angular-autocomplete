/*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena

    primer observer-a:

export class AppComponent implements OnInit, OnDestroy {

    articleObserver = (observable: ArticleRepository): void => {
        console.log(observable);
        this.articles = observable.getArticles();
    }

    // interfaces
    ngOnInit() {
        this.articleRepository.attach(this.articleObserver);

        this.articles = this.articleRepository.getArticles();
        console.log("AppComponent init");
    }

    ngOnDestroy() {
        if (this.articleRepository)
            this.articleRepository.dettach(this.articleObserver);
        console.log("AppComponent destroyed");
    }
}
*/
export class Observable {
    constructor() {
        this.observers = new Set();
        this.attach = (observer) => {
            if (!this.observers.has(observer))
                this.observers.add(observer);
        };
        this.dettach = (observer) => {
            if (this.observers.has(observer))
                this.observers.delete(observer);
        };
        this.notify = () => {
            for (let observer of this.observers) {
                // ne znam bas koliko ima smisla
                if (observer) {
                    try {
                        observer.update();
                    }
                    catch (error) {
                        observer();
                    }
                }
                else
                    this.dettach(observer);
            }
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JzZXJ2YWJsZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvcGF0dGVybi9iZWhhdmlvdXJhbC9vYnNlcnZlci9vYnNlcnZhYmxlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUEyQkU7QUFJRixNQUFNLE9BQWdCLFVBQVU7SUFBaEM7UUFDWSxjQUFTLEdBQTZCLElBQUksR0FBRyxFQUFFLENBQUM7UUFFMUQsV0FBTSxHQUFHLENBQUMsUUFBNkIsRUFBUSxFQUFFO1lBQy9DLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7Z0JBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEUsQ0FBQyxDQUFDO1FBRUYsWUFBTyxHQUFHLENBQUMsUUFBNkIsRUFBUSxFQUFFO1lBQ2hELElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDO2dCQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BFLENBQUMsQ0FBQztRQUVRLFdBQU0sR0FBRyxHQUFTLEVBQUU7WUFDNUIsS0FBSyxJQUFJLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNuQyxnQ0FBZ0M7Z0JBQ2hDLElBQUksUUFBUSxFQUFFO29CQUNaLElBQUk7d0JBQ1MsUUFBUyxDQUFDLE1BQU0sRUFBRSxDQUFDO3FCQUMvQjtvQkFBQyxPQUFPLEtBQUssRUFBRTt3QkFDSCxRQUFTLEVBQUUsQ0FBQztxQkFDeEI7aUJBQ0Y7O29CQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUM7SUFDSixDQUFDO0NBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG4gICAgb2JzZXJ2ZXJzIG5pc3UgbmlzdGEgZHJ1Z28gZG8gc2FtZSBmdW5rY2lqZSBrb2phIHRyZWJhIGRhIHNlIHBvem92ZSBrYWRhIG5hZFxyXG4gICAgb2JqZWt0b20gdGlwYSBPYnNlcnZhYmxlIG5hIGtvanUgc21vIHN1YnNjcmliZS1vdmFuaSBkb2RqZSBkbyBwcm9tZW5hXHJcblxyXG4gICAgcHJpbWVyIG9ic2VydmVyLWE6XHJcblxyXG5leHBvcnQgY2xhc3MgQXBwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIGFydGljbGVPYnNlcnZlciA9IChvYnNlcnZhYmxlOiBBcnRpY2xlUmVwb3NpdG9yeSk6IHZvaWQgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKG9ic2VydmFibGUpO1xyXG4gICAgICAgIHRoaXMuYXJ0aWNsZXMgPSBvYnNlcnZhYmxlLmdldEFydGljbGVzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gaW50ZXJmYWNlc1xyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5hcnRpY2xlUmVwb3NpdG9yeS5hdHRhY2godGhpcy5hcnRpY2xlT2JzZXJ2ZXIpO1xyXG5cclxuICAgICAgICB0aGlzLmFydGljbGVzID0gdGhpcy5hcnRpY2xlUmVwb3NpdG9yeS5nZXRBcnRpY2xlcygpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiQXBwQ29tcG9uZW50IGluaXRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXJ0aWNsZVJlcG9zaXRvcnkpXHJcbiAgICAgICAgICAgIHRoaXMuYXJ0aWNsZVJlcG9zaXRvcnkuZGV0dGFjaCh0aGlzLmFydGljbGVPYnNlcnZlcik7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJBcHBDb21wb25lbnQgZGVzdHJveWVkXCIpO1xyXG4gICAgfVxyXG59XHJcbiovXHJcblxyXG5pbXBvcnQgeyBPYnNlcnZlciB9IGZyb20gJy4vb2JzZXJ2ZXIuaW50ZXJmYWNlJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBPYnNlcnZhYmxlIHtcclxuICBwcm90ZWN0ZWQgb2JzZXJ2ZXJzOiBTZXQ8T2JzZXJ2ZXIgfCBGdW5jdGlvbj4gPSBuZXcgU2V0KCk7XHJcblxyXG4gIGF0dGFjaCA9IChvYnNlcnZlcjogT2JzZXJ2ZXIgfCBGdW5jdGlvbik6IHZvaWQgPT4ge1xyXG4gICAgaWYgKCF0aGlzLm9ic2VydmVycy5oYXMob2JzZXJ2ZXIpKSB0aGlzLm9ic2VydmVycy5hZGQob2JzZXJ2ZXIpO1xyXG4gIH07XHJcblxyXG4gIGRldHRhY2ggPSAob2JzZXJ2ZXI6IE9ic2VydmVyIHwgRnVuY3Rpb24pOiB2b2lkID0+IHtcclxuICAgIGlmICh0aGlzLm9ic2VydmVycy5oYXMob2JzZXJ2ZXIpKSB0aGlzLm9ic2VydmVycy5kZWxldGUob2JzZXJ2ZXIpO1xyXG4gIH07XHJcblxyXG4gIHByb3RlY3RlZCBub3RpZnkgPSAoKTogdm9pZCA9PiB7XHJcbiAgICBmb3IgKGxldCBvYnNlcnZlciBvZiB0aGlzLm9ic2VydmVycykge1xyXG4gICAgICAvLyBuZSB6bmFtIGJhcyBrb2xpa28gaW1hIHNtaXNsYVxyXG4gICAgICBpZiAob2JzZXJ2ZXIpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgKDxPYnNlcnZlcj5vYnNlcnZlcikudXBkYXRlKCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcclxuICAgICAgICAgICg8RnVuY3Rpb24+b2JzZXJ2ZXIpKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgdGhpcy5kZXR0YWNoKG9ic2VydmVyKTtcclxuICAgIH1cclxuICB9O1xyXG59XHJcbiJdfQ==