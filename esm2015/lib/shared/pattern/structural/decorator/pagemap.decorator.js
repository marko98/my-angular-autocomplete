export class PageMap {
    constructor() {
        this.map = new Map();
        /**
         * prva implementacija metode nikada ne prazni mapu iako se promeni kriterijum za
         * dobavljanje entiteta
         */
        // has = (key: PageKey): boolean => {
        //   // iteriramo kroz sve elemente
        //   for (const [existingKey, value] of this.map) {
        //     if (
        //       existingKey.pageNumber === key.pageNumber &&
        //       existingKey.pageSize === key.pageSize &&
        //       existingKey.sort.empty === key.sort.empty &&
        //       existingKey.sort.sorted === key.sort.sorted &&
        //       existingKey.sort.unsorted === key.sort.unsorted
        //     )
        //       return true;
        //   }
        //   return false;
        // };
        /**
         * druga implementacija metode prazni mapu ako dodje do promene kriterijuma za dobavljanje entiteta
         */
        this.has = (key) => {
            if (!this._hasCriteriaChanged(key)) {
                for (const [existingKey, value] of this.map) {
                    if (existingKey.pageNumber === key.pageNumber)
                        return true;
                }
            }
            else {
                this.map.clear();
                return false;
            }
        };
        this._hasCriteriaChanged = (key) => {
            for (const [existingKey, value] of this.map) {
                if (existingKey.pageSize === key.pageSize &&
                    existingKey.sort.empty === key.sort.empty &&
                    existingKey.sort.sorted === key.sort.sorted &&
                    existingKey.sort.unsorted === key.sort.unsorted)
                    return false;
                else
                    return true;
            }
        };
        this.getMap = () => {
            return this.map;
        };
        this.clone = () => {
            // vracamo clone
            return new Map(this.map);
        };
        this.clear = () => {
            this.map.clear();
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZW1hcC5kZWNvcmF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hdXRvY29tcGxldGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3BhdHRlcm4vc3RydWN0dXJhbC9kZWNvcmF0b3IvcGFnZW1hcC5kZWNvcmF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUEsTUFBTSxPQUFPLE9BQU87SUFBcEI7UUFDVSxRQUFHLEdBQThCLElBQUksR0FBRyxFQUFFLENBQUM7UUFFbkQ7OztXQUdHO1FBQ0gscUNBQXFDO1FBQ3JDLG1DQUFtQztRQUNuQyxtREFBbUQ7UUFDbkQsV0FBVztRQUNYLHFEQUFxRDtRQUNyRCxpREFBaUQ7UUFDakQscURBQXFEO1FBQ3JELHVEQUF1RDtRQUN2RCx3REFBd0Q7UUFDeEQsUUFBUTtRQUNSLHFCQUFxQjtRQUNyQixNQUFNO1FBQ04sa0JBQWtCO1FBQ2xCLEtBQUs7UUFFTDs7V0FFRztRQUNILFFBQUcsR0FBRyxDQUFDLEdBQVksRUFBVyxFQUFFO1lBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ2xDLEtBQUssTUFBTSxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFO29CQUMzQyxJQUFJLFdBQVcsQ0FBQyxVQUFVLEtBQUssR0FBRyxDQUFDLFVBQVU7d0JBQUUsT0FBTyxJQUFJLENBQUM7aUJBQzVEO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsT0FBTyxLQUFLLENBQUM7YUFDZDtRQUNILENBQUMsQ0FBQztRQUVNLHdCQUFtQixHQUFHLENBQUMsR0FBWSxFQUFXLEVBQUU7WUFDdEQsS0FBSyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7Z0JBQzNDLElBQ0UsV0FBVyxDQUFDLFFBQVEsS0FBSyxHQUFHLENBQUMsUUFBUTtvQkFDckMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLO29CQUN6QyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU07b0JBQzNDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUTtvQkFFL0MsT0FBTyxLQUFLLENBQUM7O29CQUNWLE9BQU8sSUFBSSxDQUFDO2FBQ2xCO1FBQ0gsQ0FBQyxDQUFDO1FBRUYsV0FBTSxHQUFHLEdBQThCLEVBQUU7WUFDdkMsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ2xCLENBQUMsQ0FBQztRQUVGLFVBQUssR0FBRyxHQUE4QixFQUFFO1lBQ3RDLGdCQUFnQjtZQUNoQixPQUFPLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixDQUFDLENBQUM7UUFFRixVQUFLLEdBQUcsR0FBUyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRW50aXRldCB9IGZyb20gJy4uLy4uLy4uL21vZGVsL2ludGVyZmFjZS9lbnRpdGV0LmludGVyZmFjZSc7XHJcbmltcG9ydCB7IFByb3RvdHlwZSB9IGZyb20gJy4uLy4uL2NyZWF0aW9uYWwvcHJvdG90eXBlL3Byb3RvdHlwZS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBQYWdlS2V5LCBQYWdlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZS9wYWdlYWJsZS1yZWFkLnNlcnZpY2UnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBhZ2VNYXA8VCBleHRlbmRzIEVudGl0ZXQ8VCwgSUQ+LCBJRD4gaW1wbGVtZW50cyBQcm90b3R5cGUge1xyXG4gIHByaXZhdGUgbWFwOiBNYXA8UGFnZUtleSwgUGFnZTxULCBJRD4+ID0gbmV3IE1hcCgpO1xyXG5cclxuICAvKipcclxuICAgKiBwcnZhIGltcGxlbWVudGFjaWphIG1ldG9kZSBuaWthZGEgbmUgcHJhem5pIG1hcHUgaWFrbyBzZSBwcm9tZW5pIGtyaXRlcmlqdW0gemFcclxuICAgKiBkb2JhdmxqYW5qZSBlbnRpdGV0YVxyXG4gICAqL1xyXG4gIC8vIGhhcyA9IChrZXk6IFBhZ2VLZXkpOiBib29sZWFuID0+IHtcclxuICAvLyAgIC8vIGl0ZXJpcmFtbyBrcm96IHN2ZSBlbGVtZW50ZVxyXG4gIC8vICAgZm9yIChjb25zdCBbZXhpc3RpbmdLZXksIHZhbHVlXSBvZiB0aGlzLm1hcCkge1xyXG4gIC8vICAgICBpZiAoXHJcbiAgLy8gICAgICAgZXhpc3RpbmdLZXkucGFnZU51bWJlciA9PT0ga2V5LnBhZ2VOdW1iZXIgJiZcclxuICAvLyAgICAgICBleGlzdGluZ0tleS5wYWdlU2l6ZSA9PT0ga2V5LnBhZ2VTaXplICYmXHJcbiAgLy8gICAgICAgZXhpc3RpbmdLZXkuc29ydC5lbXB0eSA9PT0ga2V5LnNvcnQuZW1wdHkgJiZcclxuICAvLyAgICAgICBleGlzdGluZ0tleS5zb3J0LnNvcnRlZCA9PT0ga2V5LnNvcnQuc29ydGVkICYmXHJcbiAgLy8gICAgICAgZXhpc3RpbmdLZXkuc29ydC51bnNvcnRlZCA9PT0ga2V5LnNvcnQudW5zb3J0ZWRcclxuICAvLyAgICAgKVxyXG4gIC8vICAgICAgIHJldHVybiB0cnVlO1xyXG4gIC8vICAgfVxyXG4gIC8vICAgcmV0dXJuIGZhbHNlO1xyXG4gIC8vIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIGRydWdhIGltcGxlbWVudGFjaWphIG1ldG9kZSBwcmF6bmkgbWFwdSBha28gZG9kamUgZG8gcHJvbWVuZSBrcml0ZXJpanVtYSB6YSBkb2JhdmxqYW5qZSBlbnRpdGV0YVxyXG4gICAqL1xyXG4gIGhhcyA9IChrZXk6IFBhZ2VLZXkpOiBib29sZWFuID0+IHtcclxuICAgIGlmICghdGhpcy5faGFzQ3JpdGVyaWFDaGFuZ2VkKGtleSkpIHtcclxuICAgICAgZm9yIChjb25zdCBbZXhpc3RpbmdLZXksIHZhbHVlXSBvZiB0aGlzLm1hcCkge1xyXG4gICAgICAgIGlmIChleGlzdGluZ0tleS5wYWdlTnVtYmVyID09PSBrZXkucGFnZU51bWJlcikgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubWFwLmNsZWFyKCk7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBwcml2YXRlIF9oYXNDcml0ZXJpYUNoYW5nZWQgPSAoa2V5OiBQYWdlS2V5KTogYm9vbGVhbiA9PiB7XHJcbiAgICBmb3IgKGNvbnN0IFtleGlzdGluZ0tleSwgdmFsdWVdIG9mIHRoaXMubWFwKSB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICBleGlzdGluZ0tleS5wYWdlU2l6ZSA9PT0ga2V5LnBhZ2VTaXplICYmXHJcbiAgICAgICAgZXhpc3RpbmdLZXkuc29ydC5lbXB0eSA9PT0ga2V5LnNvcnQuZW1wdHkgJiZcclxuICAgICAgICBleGlzdGluZ0tleS5zb3J0LnNvcnRlZCA9PT0ga2V5LnNvcnQuc29ydGVkICYmXHJcbiAgICAgICAgZXhpc3RpbmdLZXkuc29ydC51bnNvcnRlZCA9PT0ga2V5LnNvcnQudW5zb3J0ZWRcclxuICAgICAgKVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgZWxzZSByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBnZXRNYXAgPSAoKTogTWFwPFBhZ2VLZXksIFBhZ2U8VCwgSUQ+PiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5tYXA7XHJcbiAgfTtcclxuXHJcbiAgY2xvbmUgPSAoKTogTWFwPFBhZ2VLZXksIFBhZ2U8VCwgSUQ+PiA9PiB7XHJcbiAgICAvLyB2cmFjYW1vIGNsb25lXHJcbiAgICByZXR1cm4gbmV3IE1hcCh0aGlzLm1hcCk7XHJcbiAgfTtcclxuXHJcbiAgY2xlYXIgPSAoKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLm1hcC5jbGVhcigpO1xyXG4gIH07XHJcbn1cclxuIl19