import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { HeightDirective } from './directive/height.directive';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { WidthDirective } from './directive/width.directive';
import { OpacityTransitionDirective } from './directive/opacity-transition.directive';
import * as i0 from "@angular/core";
export class SharedModule {
}
SharedModule.ɵmod = i0.ɵɵdefineNgModule({ type: SharedModule });
SharedModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MaterialModule,
            FlexLayoutModule,
            ScrollingModule,
            DragDropModule,
        ],
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        ScrollingModule,
        DragDropModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SharedModule, { declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        ScrollingModule,
        DragDropModule], exports: [HeightDirective,
        WidthDirective,
        OpacityTransitionDirective,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        ScrollingModule,
        DragDropModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SharedModule, [{
        type: NgModule,
        args: [{
                declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                    ScrollingModule,
                    DragDropModule,
                ],
                exports: [
                    HeightDirective,
                    WidthDirective,
                    OpacityTransitionDirective,
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                    ScrollingModule,
                    DragDropModule,
                ],
                entryComponents: [],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2hhcmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRXhELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQzs7QUE0QnRGLE1BQU0sT0FBTyxZQUFZOztnREFBWixZQUFZO3VHQUFaLFlBQVksa0JBeEJkO1lBQ1AsWUFBWTtZQUNaLFdBQVc7WUFDWCxtQkFBbUI7WUFDbkIsY0FBYztZQUNkLGdCQUFnQjtZQUNoQixlQUFlO1lBQ2YsY0FBYztTQUNmO1FBTUMsWUFBWTtRQUNaLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsY0FBYztRQUNkLGdCQUFnQjtRQUNoQixlQUFlO1FBQ2YsY0FBYzt3RkFJTCxZQUFZLG1CQXpCUixlQUFlLEVBQUUsY0FBYyxFQUFFLDBCQUEwQixhQUV4RSxZQUFZO1FBQ1osV0FBVztRQUNYLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsZ0JBQWdCO1FBQ2hCLGVBQWU7UUFDZixjQUFjLGFBR2QsZUFBZTtRQUNmLGNBQWM7UUFDZCwwQkFBMEI7UUFFMUIsWUFBWTtRQUNaLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsY0FBYztRQUNkLGdCQUFnQjtRQUNoQixlQUFlO1FBQ2YsY0FBYztrREFJTCxZQUFZO2NBMUJ4QixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsZUFBZSxFQUFFLGNBQWMsRUFBRSwwQkFBMEIsQ0FBQztnQkFDM0UsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGNBQWM7b0JBQ2QsZ0JBQWdCO29CQUNoQixlQUFlO29CQUNmLGNBQWM7aUJBQ2Y7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLGVBQWU7b0JBQ2YsY0FBYztvQkFDZCwwQkFBMEI7b0JBRTFCLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGNBQWM7b0JBQ2QsZ0JBQWdCO29CQUNoQixlQUFlO29CQUNmLGNBQWM7aUJBQ2Y7Z0JBQ0QsZUFBZSxFQUFFLEVBQUU7YUFDcEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgRmxleExheW91dE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IEhlaWdodERpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlL2hlaWdodC5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTY3JvbGxpbmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jZGsvc2Nyb2xsaW5nJztcclxuaW1wb3J0IHsgRHJhZ0Ryb3BNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcclxuXHJcbmltcG9ydCB7IFdpZHRoRGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmUvd2lkdGguZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgT3BhY2l0eVRyYW5zaXRpb25EaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZS9vcGFjaXR5LXRyYW5zaXRpb24uZGlyZWN0aXZlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbSGVpZ2h0RGlyZWN0aXZlLCBXaWR0aERpcmVjdGl2ZSwgT3BhY2l0eVRyYW5zaXRpb25EaXJlY3RpdmVdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgRmxleExheW91dE1vZHVsZSxcclxuICAgIFNjcm9sbGluZ01vZHVsZSxcclxuICAgIERyYWdEcm9wTW9kdWxlLFxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgSGVpZ2h0RGlyZWN0aXZlLFxyXG4gICAgV2lkdGhEaXJlY3RpdmUsXHJcbiAgICBPcGFjaXR5VHJhbnNpdGlvbkRpcmVjdGl2ZSxcclxuXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSxcclxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIEZsZXhMYXlvdXRNb2R1bGUsXHJcbiAgICBTY3JvbGxpbmdNb2R1bGUsXHJcbiAgICBEcmFnRHJvcE1vZHVsZSxcclxuICBdLFxyXG4gIGVudHJ5Q29tcG9uZW50czogW10sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaGFyZWRNb2R1bGUge31cclxuIl19