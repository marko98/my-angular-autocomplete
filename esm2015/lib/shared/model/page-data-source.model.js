import { DataSource, } from '@angular/cdk/collections';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
export class PageDataSource extends DataSource {
    /**
     * @param pageableCrudService posto ce nam trebati service(tipa PageableCrudService) za dobavljanje
     * stranica zahtevamo ga kao argument u konstruktoru
     *
     * @param _pageSize koliko item-a zelimo da prikazemo po stranici
     */
    constructor(pageableCrudService, _pageSize = 10) {
        super();
        this.pageableCrudService = pageableCrudService;
        this._pageSize = _pageSize;
        this._cachedData = [];
        this._fetchedPages = new Set(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
        this._dataStream = new BehaviorSubject(this._cachedData);
        this._sentRequests = 0;
        this._subscription = new Subscription();
        this._hasDataToShowSubject = new Subject();
        this._isLoadingSubject = new Subject();
        this._hasFetchedDataSubject = new Subject();
        this._totalItemsSubject = new Subject();
        this._sortParams = [];
        this.isLoading = () => {
            return this._isLoadingSubject;
        };
        this.getHasDataToShowSubject = () => {
            return this._hasDataToShowSubject;
        };
        this.getHasFetchedDataSubject = () => {
            return this._hasFetchedDataSubject;
        };
        this.getTotalItemsSubject = () => {
            return this._totalItemsSubject;
        };
        this.changePath = (path) => {
            this.pageableCrudService.setPath(path);
        };
        this.changeCriteriaPath = (criteriaPath) => {
            this.pageableCrudService.setCriteriaPath(criteriaPath);
        };
        this.resetAndFetch = (criteria, sortParams, pageSize) => {
            if (pageSize)
                this._pageSize = pageSize;
            if (sortParams)
                this._sortParams = sortParams;
            this._cachedData.splice(0, this._cachedData.length);
            this._fetchedPages.clear(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
            this._dataStream.next(this._cachedData);
            this._hasFetchedDataSubject.next(false);
            // console.log(criteria, this._pageSize);
            this._fetchPage(0, criteria);
        };
        // this._fetchPage(0);
    }
    connect(collectionViewer) {
        this._subscription.add(collectionViewer.viewChange.subscribe((range) => {
            // console.log('Range: ', range);
            const startPage = this._getPageForIndex(range.start);
            // console.log('Start page: ', startPage);
            const endPage = this._getPageForIndex(range.end - 1);
            // console.log('End page: ', endPage);
            for (let pageNumber = startPage; pageNumber <= endPage; pageNumber++) {
                // console.log('pageNumber: ', pageNumber);
                this._fetchPage(pageNumber);
            }
        }));
        return this._dataStream;
    }
    disconnect() {
        // console.log(
        //   'disconnected, obrati paznju na *ngIf nad cdk-virtual-scroll-viewport diskonektovaces se, koristi hidden ili napisi svoju metodu za diskonektovanje'
        // );
        // this._subscription.unsubscribe();
    }
    myDisconnect() {
        this._subscription.unsubscribe();
    }
    _getPageForIndex(index) {
        return Math.floor(index / this._pageSize);
    }
    _fetchPage(pageNumber, criteria) {
        if (this._fetchedPages.has(pageNumber)) {
            return;
        }
        // console.log(
        //   'Fetching a next page, page number: ',
        //   pageNumber,
        //   ', criteria: ',
        //   criteria
        // );
        this._fetchedPages.add(pageNumber);
        this._sentRequests += 1;
        // console.log('zahtevi na cekanju: ', this._sentRequests);
        this._isLoadingSubject.next(this._sentRequests > 0);
        this.pageableCrudService
            .findPageWithPageNumberAndSizeAndSort(pageNumber, this._pageSize, this._sortParams, criteria)
            .subscribe((page) => {
            // console.log(page);
            // trenutno tu da bismo napravili loading
            setTimeout(() => {
                if (!(this._cachedData.length > 0)) {
                    this._cachedData = Array.from({
                        length: page.totalElements,
                    });
                    this._hasFetchedDataSubject.next(true);
                    this._hasDataToShowSubject.next(page.totalElements > 0);
                    this._totalItemsSubject.next(page.totalElements);
                }
                if (page.content) {
                    this._cachedData.splice(pageNumber * this._pageSize, this._pageSize, ...page.content.map((g) => {
                        return this.pageableCrudService.buildEntitet(g);
                    }));
                }
                this._dataStream.next(this._cachedData);
                // console.log(this._cachedData);
                this._sentRequests -= 1;
                // console.log('zahtevi na cekanju: ', this._sentRequests);
                this._isLoadingSubject.next(this._sentRequests > 0);
            }, 200);
        }, (err) => {
            this._hasFetchedDataSubject.next(true);
            console.log(err);
            this._sentRequests -= 1;
            // console.log('zahtevi na cekanju: ', this._sentRequests);
            this._isLoadingSubject.next(this._sentRequests > 0);
        }, () => {
            // completed subscription
        });
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS1kYXRhLXNvdXJjZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvcGFnZS1kYXRhLXNvdXJjZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsVUFBVSxHQUdYLE1BQU0sMEJBQTBCLENBQUM7QUFDbEMsT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBTzFFLE1BQU0sT0FBTyxjQUlYLFNBQVEsVUFBc0I7SUFhOUI7Ozs7O09BS0c7SUFDSCxZQUNZLG1CQUF3RCxFQUN4RCxZQUFZLEVBQUU7UUFFeEIsS0FBSyxFQUFFLENBQUM7UUFIRSx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFDO1FBQ3hELGNBQVMsR0FBVCxTQUFTLENBQUs7UUFwQmhCLGdCQUFXLEdBQWlCLEVBQUUsQ0FBQztRQUMvQixrQkFBYSxHQUFHLElBQUksR0FBRyxFQUFVLENBQUMsQ0FBQyw4REFBOEQ7UUFDakcsZ0JBQVcsR0FBRyxJQUFJLGVBQWUsQ0FBZSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbEUsa0JBQWEsR0FBVyxDQUFDLENBQUM7UUFDMUIsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXJDLDBCQUFxQixHQUFxQixJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3hELHNCQUFpQixHQUFxQixJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BELDJCQUFzQixHQUFxQixJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3pELHVCQUFrQixHQUFvQixJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BELGdCQUFXLEdBQWdCLEVBQUUsQ0FBQztRQXFIL0IsY0FBUyxHQUFHLEdBQXFCLEVBQUU7WUFDeEMsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDaEMsQ0FBQyxDQUFDO1FBRUssNEJBQXVCLEdBQUcsR0FBcUIsRUFBRTtZQUN0RCxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztRQUNwQyxDQUFDLENBQUM7UUFFSyw2QkFBd0IsR0FBRyxHQUFxQixFQUFFO1lBQ3ZELE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDO1FBQ3JDLENBQUMsQ0FBQztRQUVLLHlCQUFvQixHQUFHLEdBQW9CLEVBQUU7WUFDbEQsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDakMsQ0FBQyxDQUFDO1FBRUssZUFBVSxHQUFHLENBQUMsSUFBWSxFQUFRLEVBQUU7WUFDekMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUM7UUFFSyx1QkFBa0IsR0FBRyxDQUFDLFlBQW9CLEVBQVEsRUFBRTtZQUN6RCxJQUFJLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3pELENBQUMsQ0FBQztRQUVLLGtCQUFhLEdBQUcsQ0FDckIsUUFBZ0IsRUFDaEIsVUFBd0IsRUFDeEIsUUFBaUIsRUFDWCxFQUFFO1lBQ1IsSUFBSSxRQUFRO2dCQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1lBRXhDLElBQUksVUFBVTtnQkFBRSxJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztZQUU5QyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsOERBQThEO1lBQzFGLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUV4QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRXhDLHlDQUF5QztZQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUM7UUFqSkEsc0JBQXNCO0lBQ3hCLENBQUM7SUFFRCxPQUFPLENBQUMsZ0JBQWtDO1FBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUNwQixnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBZ0IsRUFBRSxFQUFFO1lBQ3pELGlDQUFpQztZQUNqQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3JELDBDQUEwQztZQUMxQyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNyRCxzQ0FBc0M7WUFDdEMsS0FBSyxJQUFJLFVBQVUsR0FBRyxTQUFTLEVBQUUsVUFBVSxJQUFJLE9BQU8sRUFBRSxVQUFVLEVBQUUsRUFBRTtnQkFDcEUsMkNBQTJDO2dCQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzdCO1FBQ0gsQ0FBQyxDQUFDLENBQ0gsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUMxQixDQUFDO0lBRUQsVUFBVTtRQUNSLGVBQWU7UUFDZix5SkFBeUo7UUFDekosS0FBSztRQUNMLG9DQUFvQztJQUN0QyxDQUFDO0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVTLGdCQUFnQixDQUFDLEtBQWE7UUFDdEMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVTLFVBQVUsQ0FBQyxVQUFrQixFQUFFLFFBQWlCO1FBQ3hELElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDdEMsT0FBTztTQUNSO1FBQ0QsZUFBZTtRQUNmLDJDQUEyQztRQUMzQyxnQkFBZ0I7UUFDaEIsb0JBQW9CO1FBQ3BCLGFBQWE7UUFDYixLQUFLO1FBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFbkMsSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLENBQUM7UUFDeEIsMkRBQTJEO1FBQzNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsbUJBQW1CO2FBQ3JCLG9DQUFvQyxDQUNuQyxVQUFVLEVBQ1YsSUFBSSxDQUFDLFNBQVMsRUFDZCxJQUFJLENBQUMsV0FBVyxFQUNoQixRQUFRLENBQ1Q7YUFDQSxTQUFTLENBQ1IsQ0FBQyxJQUFpQixFQUFFLEVBQUU7WUFDcEIscUJBQXFCO1lBQ3JCLHlDQUF5QztZQUN6QyxVQUFVLENBQUMsR0FBRyxFQUFFO2dCQUNkLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQWE7d0JBQ3hDLE1BQU0sRUFBRSxJQUFJLENBQUMsYUFBYTtxQkFDM0IsQ0FBQyxDQUFDO29CQUVILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3ZDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7aUJBQ2xEO2dCQUVELElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtvQkFDaEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQ3JCLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUMzQixJQUFJLENBQUMsU0FBUyxFQUNkLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFJLEVBQUUsRUFBRTt3QkFDM0IsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNsRCxDQUFDLENBQUMsQ0FDSCxDQUFDO2lCQUNIO2dCQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDeEMsaUNBQWlDO2dCQUVqQyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsQ0FBQztnQkFDeEIsMkRBQTJEO2dCQUMzRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdEQsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1YsQ0FBQyxFQUNELENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDTixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFakIsSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLENBQUM7WUFDeEIsMkRBQTJEO1lBQzNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN0RCxDQUFDLEVBQ0QsR0FBRyxFQUFFO1lBQ0gseUJBQXlCO1FBQzNCLENBQUMsQ0FDRixDQUFDO0lBQ04sQ0FBQztDQTRDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgRGF0YVNvdXJjZSxcclxuICBDb2xsZWN0aW9uVmlld2VyLFxyXG4gIExpc3RSYW5nZSxcclxufSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIFN1YnNjcmlwdGlvbiwgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQYWdlLCBTb3J0UGFyYW0gfSBmcm9tICcuLi9zZXJ2aWNlL3BhZ2VhYmxlLXJlYWQuc2VydmljZSc7XHJcbmltcG9ydCB7IE9wdGlvbkl0ZW0gfSBmcm9tICcuL2ludGVyZmFjZS9vcHRpb24taXRlbS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBFbnRpdGV0IH0gZnJvbSAnLi9pbnRlcmZhY2UvZW50aXRldC5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBFTlRJVEVUX01PREVMX0lOVEVSRkFDRSB9IGZyb20gJy4vaW50ZXJmYWNlL2VudGl0ZXQubW9kZWwtaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgUGFnZWFibGVSZWFkU2VydmljZUludGVyZmFjZSB9IGZyb20gJy4uL3NlcnZpY2UvaW50ZXJmYWNlL3BhZ2VhYmxlLXJlYWQtc2VydmljZS5pbnRlcmZhY2UnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBhZ2VEYXRhU291cmNlPFxyXG4gIFQgZXh0ZW5kcyBFbnRpdGV0PFQsIElEPixcclxuICBJRCxcclxuICBHIGV4dGVuZHMgRU5USVRFVF9NT0RFTF9JTlRFUkZBQ0VcclxuPiBleHRlbmRzIERhdGFTb3VyY2U8T3B0aW9uSXRlbT4ge1xyXG4gIHByb3RlY3RlZCBfY2FjaGVkRGF0YTogT3B0aW9uSXRlbVtdID0gW107XHJcbiAgcHJvdGVjdGVkIF9mZXRjaGVkUGFnZXMgPSBuZXcgU2V0PG51bWJlcj4oKTsgLy8gc2t1cCBicm9qZXZhIGtvamkgcHJlZHN0YXZsamFqdSBicm9qZXZlIGRvaHZhY2VuaWggc3RyYW5pY2FcclxuICBwcm90ZWN0ZWQgX2RhdGFTdHJlYW0gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PE9wdGlvbkl0ZW1bXT4odGhpcy5fY2FjaGVkRGF0YSk7XHJcbiAgcHJvdGVjdGVkIF9zZW50UmVxdWVzdHM6IG51bWJlciA9IDA7XHJcbiAgcHJvdGVjdGVkIF9zdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKCk7XHJcblxyXG4gIHByaXZhdGUgX2hhc0RhdGFUb1Nob3dTdWJqZWN0OiBTdWJqZWN0PGJvb2xlYW4+ID0gbmV3IFN1YmplY3QoKTtcclxuICBwcml2YXRlIF9pc0xvYWRpbmdTdWJqZWN0OiBTdWJqZWN0PGJvb2xlYW4+ID0gbmV3IFN1YmplY3QoKTtcclxuICBwcml2YXRlIF9oYXNGZXRjaGVkRGF0YVN1YmplY3Q6IFN1YmplY3Q8Ym9vbGVhbj4gPSBuZXcgU3ViamVjdCgpO1xyXG4gIHByaXZhdGUgX3RvdGFsSXRlbXNTdWJqZWN0OiBTdWJqZWN0PG51bWJlcj4gPSBuZXcgU3ViamVjdCgpO1xyXG4gIHByaXZhdGUgX3NvcnRQYXJhbXM6IFNvcnRQYXJhbVtdID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIEBwYXJhbSBwYWdlYWJsZUNydWRTZXJ2aWNlIHBvc3RvIGNlIG5hbSB0cmViYXRpIHNlcnZpY2UodGlwYSBQYWdlYWJsZUNydWRTZXJ2aWNlKSB6YSBkb2JhdmxqYW5qZVxyXG4gICAqIHN0cmFuaWNhIHphaHRldmFtbyBnYSBrYW8gYXJndW1lbnQgdSBrb25zdHJ1a3RvcnVcclxuICAgKlxyXG4gICAqIEBwYXJhbSBfcGFnZVNpemUga29saWtvIGl0ZW0tYSB6ZWxpbW8gZGEgcHJpa2F6ZW1vIHBvIHN0cmFuaWNpXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcm90ZWN0ZWQgcGFnZWFibGVDcnVkU2VydmljZTogUGFnZWFibGVSZWFkU2VydmljZUludGVyZmFjZTxULCBJRD4sXHJcbiAgICBwcm90ZWN0ZWQgX3BhZ2VTaXplID0gMTBcclxuICApIHtcclxuICAgIHN1cGVyKCk7XHJcbiAgICAvLyB0aGlzLl9mZXRjaFBhZ2UoMCk7XHJcbiAgfVxyXG5cclxuICBjb25uZWN0KGNvbGxlY3Rpb25WaWV3ZXI6IENvbGxlY3Rpb25WaWV3ZXIpOiBPYnNlcnZhYmxlPE9wdGlvbkl0ZW1bXT4ge1xyXG4gICAgdGhpcy5fc3Vic2NyaXB0aW9uLmFkZChcclxuICAgICAgY29sbGVjdGlvblZpZXdlci52aWV3Q2hhbmdlLnN1YnNjcmliZSgocmFuZ2U6IExpc3RSYW5nZSkgPT4ge1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdSYW5nZTogJywgcmFuZ2UpO1xyXG4gICAgICAgIGNvbnN0IHN0YXJ0UGFnZSA9IHRoaXMuX2dldFBhZ2VGb3JJbmRleChyYW5nZS5zdGFydCk7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ1N0YXJ0IHBhZ2U6ICcsIHN0YXJ0UGFnZSk7XHJcbiAgICAgICAgY29uc3QgZW5kUGFnZSA9IHRoaXMuX2dldFBhZ2VGb3JJbmRleChyYW5nZS5lbmQgLSAxKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnRW5kIHBhZ2U6ICcsIGVuZFBhZ2UpO1xyXG4gICAgICAgIGZvciAobGV0IHBhZ2VOdW1iZXIgPSBzdGFydFBhZ2U7IHBhZ2VOdW1iZXIgPD0gZW5kUGFnZTsgcGFnZU51bWJlcisrKSB7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZygncGFnZU51bWJlcjogJywgcGFnZU51bWJlcik7XHJcbiAgICAgICAgICB0aGlzLl9mZXRjaFBhZ2UocGFnZU51bWJlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICAgIHJldHVybiB0aGlzLl9kYXRhU3RyZWFtO1xyXG4gIH1cclxuXHJcbiAgZGlzY29ubmVjdCgpOiB2b2lkIHtcclxuICAgIC8vIGNvbnNvbGUubG9nKFxyXG4gICAgLy8gICAnZGlzY29ubmVjdGVkLCBvYnJhdGkgcGF6bmp1IG5hICpuZ0lmIG5hZCBjZGstdmlydHVhbC1zY3JvbGwtdmlld3BvcnQgZGlza29uZWt0b3ZhY2VzIHNlLCBrb3Jpc3RpIGhpZGRlbiBpbGkgbmFwaXNpIHN2b2p1IG1ldG9kdSB6YSBkaXNrb25la3RvdmFuamUnXHJcbiAgICAvLyApO1xyXG4gICAgLy8gdGhpcy5fc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgfVxyXG5cclxuICBteURpc2Nvbm5lY3QoKTogdm9pZCB7XHJcbiAgICB0aGlzLl9zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBfZ2V0UGFnZUZvckluZGV4KGluZGV4OiBudW1iZXIpOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIE1hdGguZmxvb3IoaW5kZXggLyB0aGlzLl9wYWdlU2l6ZSk7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgX2ZldGNoUGFnZShwYWdlTnVtYmVyOiBudW1iZXIsIGNyaXRlcmlhPzogc3RyaW5nKSB7XHJcbiAgICBpZiAodGhpcy5fZmV0Y2hlZFBhZ2VzLmhhcyhwYWdlTnVtYmVyKSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICAvLyBjb25zb2xlLmxvZyhcclxuICAgIC8vICAgJ0ZldGNoaW5nIGEgbmV4dCBwYWdlLCBwYWdlIG51bWJlcjogJyxcclxuICAgIC8vICAgcGFnZU51bWJlcixcclxuICAgIC8vICAgJywgY3JpdGVyaWE6ICcsXHJcbiAgICAvLyAgIGNyaXRlcmlhXHJcbiAgICAvLyApO1xyXG4gICAgdGhpcy5fZmV0Y2hlZFBhZ2VzLmFkZChwYWdlTnVtYmVyKTtcclxuXHJcbiAgICB0aGlzLl9zZW50UmVxdWVzdHMgKz0gMTtcclxuICAgIC8vIGNvbnNvbGUubG9nKCd6YWh0ZXZpIG5hIGNla2FuanU6ICcsIHRoaXMuX3NlbnRSZXF1ZXN0cyk7XHJcbiAgICB0aGlzLl9pc0xvYWRpbmdTdWJqZWN0Lm5leHQodGhpcy5fc2VudFJlcXVlc3RzID4gMCk7XHJcbiAgICB0aGlzLnBhZ2VhYmxlQ3J1ZFNlcnZpY2VcclxuICAgICAgLmZpbmRQYWdlV2l0aFBhZ2VOdW1iZXJBbmRTaXplQW5kU29ydChcclxuICAgICAgICBwYWdlTnVtYmVyLFxyXG4gICAgICAgIHRoaXMuX3BhZ2VTaXplLFxyXG4gICAgICAgIHRoaXMuX3NvcnRQYXJhbXMsXHJcbiAgICAgICAgY3JpdGVyaWFcclxuICAgICAgKVxyXG4gICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgIChwYWdlOiBQYWdlPFQsIElEPikgPT4ge1xyXG4gICAgICAgICAgLy8gY29uc29sZS5sb2cocGFnZSk7XHJcbiAgICAgICAgICAvLyB0cmVudXRubyB0dSBkYSBiaXNtbyBuYXByYXZpbGkgbG9hZGluZ1xyXG4gICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghKHRoaXMuX2NhY2hlZERhdGEubGVuZ3RoID4gMCkpIHtcclxuICAgICAgICAgICAgICB0aGlzLl9jYWNoZWREYXRhID0gQXJyYXkuZnJvbTxPcHRpb25JdGVtPih7XHJcbiAgICAgICAgICAgICAgICBsZW5ndGg6IHBhZ2UudG90YWxFbGVtZW50cyxcclxuICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgdGhpcy5faGFzRmV0Y2hlZERhdGFTdWJqZWN0Lm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgdGhpcy5faGFzRGF0YVRvU2hvd1N1YmplY3QubmV4dChwYWdlLnRvdGFsRWxlbWVudHMgPiAwKTtcclxuICAgICAgICAgICAgICB0aGlzLl90b3RhbEl0ZW1zU3ViamVjdC5uZXh0KHBhZ2UudG90YWxFbGVtZW50cyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChwYWdlLmNvbnRlbnQpIHtcclxuICAgICAgICAgICAgICB0aGlzLl9jYWNoZWREYXRhLnNwbGljZShcclxuICAgICAgICAgICAgICAgIHBhZ2VOdW1iZXIgKiB0aGlzLl9wYWdlU2l6ZSxcclxuICAgICAgICAgICAgICAgIHRoaXMuX3BhZ2VTaXplLFxyXG4gICAgICAgICAgICAgICAgLi4ucGFnZS5jb250ZW50Lm1hcCgoZzogRykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5wYWdlYWJsZUNydWRTZXJ2aWNlLmJ1aWxkRW50aXRldChnKTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLl9kYXRhU3RyZWFtLm5leHQodGhpcy5fY2FjaGVkRGF0YSk7XHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuX2NhY2hlZERhdGEpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fc2VudFJlcXVlc3RzIC09IDE7XHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCd6YWh0ZXZpIG5hIGNla2FuanU6ICcsIHRoaXMuX3NlbnRSZXF1ZXN0cyk7XHJcbiAgICAgICAgICAgIHRoaXMuX2lzTG9hZGluZ1N1YmplY3QubmV4dCh0aGlzLl9zZW50UmVxdWVzdHMgPiAwKTtcclxuICAgICAgICAgIH0sIDIwMCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLl9oYXNGZXRjaGVkRGF0YVN1YmplY3QubmV4dCh0cnVlKTtcclxuXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xyXG5cclxuICAgICAgICAgIHRoaXMuX3NlbnRSZXF1ZXN0cyAtPSAxO1xyXG4gICAgICAgICAgLy8gY29uc29sZS5sb2coJ3phaHRldmkgbmEgY2VrYW5qdTogJywgdGhpcy5fc2VudFJlcXVlc3RzKTtcclxuICAgICAgICAgIHRoaXMuX2lzTG9hZGluZ1N1YmplY3QubmV4dCh0aGlzLl9zZW50UmVxdWVzdHMgPiAwKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgICgpID0+IHtcclxuICAgICAgICAgIC8vIGNvbXBsZXRlZCBzdWJzY3JpcHRpb25cclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgaXNMb2FkaW5nID0gKCk6IFN1YmplY3Q8Ym9vbGVhbj4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2lzTG9hZGluZ1N1YmplY3Q7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldEhhc0RhdGFUb1Nob3dTdWJqZWN0ID0gKCk6IFN1YmplY3Q8Ym9vbGVhbj4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2hhc0RhdGFUb1Nob3dTdWJqZWN0O1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRIYXNGZXRjaGVkRGF0YVN1YmplY3QgPSAoKTogU3ViamVjdDxib29sZWFuPiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5faGFzRmV0Y2hlZERhdGFTdWJqZWN0O1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRUb3RhbEl0ZW1zU3ViamVjdCA9ICgpOiBTdWJqZWN0PG51bWJlcj4gPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX3RvdGFsSXRlbXNTdWJqZWN0O1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBjaGFuZ2VQYXRoID0gKHBhdGg6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5wYWdlYWJsZUNydWRTZXJ2aWNlLnNldFBhdGgocGF0aCk7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGNoYW5nZUNyaXRlcmlhUGF0aCA9IChjcml0ZXJpYVBhdGg6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5wYWdlYWJsZUNydWRTZXJ2aWNlLnNldENyaXRlcmlhUGF0aChjcml0ZXJpYVBhdGgpO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyByZXNldEFuZEZldGNoID0gKFxyXG4gICAgY3JpdGVyaWE6IHN0cmluZyxcclxuICAgIHNvcnRQYXJhbXM/OiBTb3J0UGFyYW1bXSxcclxuICAgIHBhZ2VTaXplPzogbnVtYmVyXHJcbiAgKTogdm9pZCA9PiB7XHJcbiAgICBpZiAocGFnZVNpemUpIHRoaXMuX3BhZ2VTaXplID0gcGFnZVNpemU7XHJcblxyXG4gICAgaWYgKHNvcnRQYXJhbXMpIHRoaXMuX3NvcnRQYXJhbXMgPSBzb3J0UGFyYW1zO1xyXG5cclxuICAgIHRoaXMuX2NhY2hlZERhdGEuc3BsaWNlKDAsIHRoaXMuX2NhY2hlZERhdGEubGVuZ3RoKTtcclxuICAgIHRoaXMuX2ZldGNoZWRQYWdlcy5jbGVhcigpOyAvLyBza3VwIGJyb2pldmEga29qaSBwcmVkc3RhdmxqYWp1IGJyb2pldmUgZG9odmFjZW5paCBzdHJhbmljYVxyXG4gICAgdGhpcy5fZGF0YVN0cmVhbS5uZXh0KHRoaXMuX2NhY2hlZERhdGEpO1xyXG5cclxuICAgIHRoaXMuX2hhc0ZldGNoZWREYXRhU3ViamVjdC5uZXh0KGZhbHNlKTtcclxuXHJcbiAgICAvLyBjb25zb2xlLmxvZyhjcml0ZXJpYSwgdGhpcy5fcGFnZVNpemUpO1xyXG4gICAgdGhpcy5fZmV0Y2hQYWdlKDAsIGNyaXRlcmlhKTtcclxuICB9O1xyXG59XHJcbiJdfQ==