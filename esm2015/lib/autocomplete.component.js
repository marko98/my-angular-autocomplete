import { Component, ChangeDetectionStrategy, Input, HostListener, } from '@angular/core';
import { transferArrayItem, moveItemInArray, } from '@angular/cdk/drag-drop';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "./shared/directive/opacity-transition.directive";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/forms";
import * as i6 from "@angular/common";
import * as i7 from "@angular/material/slide-toggle";
import * as i8 from "@angular/cdk/drag-drop";
import * as i9 from "@angular/material/progress-bar";
import * as i10 from "@angular/cdk/scrolling";
import * as i11 from "./shared/directive/height.directive";
import * as i12 from "@angular/material/list";
import * as i13 from "@angular/material/progress-spinner";
function AutocompleteComponent_mat_slide_toggle_8_Template(rf, ctx) { if (rf & 1) {
    const _r6 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-slide-toggle", 8);
    i0.ɵɵlistener("change", function AutocompleteComponent_mat_slide_toggle_8_Template_mat_slide_toggle_change_0_listener($event) { i0.ɵɵrestoreView(_r6); const ctx_r5 = i0.ɵɵnextContext(); return ctx_r5.onSlideToggleChange($event); });
    i0.ɵɵtext(1, "sort?");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("appOpacityDuration", 1)("color", "primary")("checked", false)("disabled", false);
} }
function AutocompleteComponent_section_9_div_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r13 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r13);
} }
function AutocompleteComponent_section_9_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r14 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r14);
} }
function AutocompleteComponent_section_9_div_18_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r15 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r15);
} }
const _c0 = function (a0, a1) { return [a0, a1]; };
function AutocompleteComponent_section_9_Template(rf, ctx) { if (rf & 1) {
    const _r17 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 9);
    i0.ɵɵelementStart(1, "section", 10);
    i0.ɵɵelementStart(2, "h2");
    i0.ɵɵtext(3, "criteria");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 11, 12);
    i0.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_4_listener($event) { i0.ɵɵrestoreView(_r17); const ctx_r16 = i0.ɵɵnextContext(); return ctx_r16.drop($event); });
    i0.ɵɵtemplate(6, AutocompleteComponent_section_9_div_6_Template, 2, 1, "div", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "section", 10);
    i0.ɵɵelementStart(8, "h2");
    i0.ɵɵtext(9, "asc");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 11, 14);
    i0.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_10_listener($event) { i0.ɵɵrestoreView(_r17); const ctx_r18 = i0.ɵɵnextContext(); return ctx_r18.drop($event); });
    i0.ɵɵtemplate(12, AutocompleteComponent_section_9_div_12_Template, 2, 1, "div", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "section", 10);
    i0.ɵɵelementStart(14, "h2");
    i0.ɵɵtext(15, "desc");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(16, "div", 11, 15);
    i0.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_16_listener($event) { i0.ɵɵrestoreView(_r17); const ctx_r19 = i0.ɵɵnextContext(); return ctx_r19.drop($event); });
    i0.ɵɵtemplate(18, AutocompleteComponent_section_9_div_18_Template, 2, 1, "div", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const _r7 = i0.ɵɵreference(5);
    const _r9 = i0.ɵɵreference(11);
    const _r11 = i0.ɵɵreference(17);
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵproperty("appOpacityDuration", 1);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("cdkDropListData", ctx_r3.autocomplete.sortingOptions)("cdkDropListConnectedTo", i0.ɵɵpureFunction2(10, _c0, _r9, _r11));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r3.autocomplete.sortingOptions);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("cdkDropListData", ctx_r3.ascList)("cdkDropListConnectedTo", i0.ɵɵpureFunction2(13, _c0, _r7, _r11));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r3.ascList);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("cdkDropListData", ctx_r3.descList)("cdkDropListConnectedTo", i0.ɵɵpureFunction2(16, _c0, _r7, _r9));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r3.descList);
} }
function AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-progress-bar", 20);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h2");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r25 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r25.getContextToShow());
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-spinner", 26);
} if (rf & 2) {
    i0.ɵɵproperty("diameter", 20);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template(rf, ctx) { if (rf & 1) {
    const _r36 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-list-item", 24);
    i0.ɵɵlistener("click", function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template_mat_list_item_click_0_listener() { i0.ɵɵrestoreView(_r36); const item_r25 = ctx.$implicit; const ctx_r35 = i0.ɵɵnextContext(3); return ctx_r35.itemSelected(item_r25); });
    i0.ɵɵtemplate(1, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template, 2, 1, "h2", 7);
    i0.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template, 1, 1, "mat-spinner", 25);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const item_r25 = ctx.$implicit;
    const observables_r20 = i0.ɵɵnextContext(2).ngIf;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !observables_r20.isLoading && item_r25);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", observables_r20.isLoading);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "cdk-virtual-scroll-viewport", 21);
    i0.ɵɵelementStart(1, "mat-list", 22);
    i0.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template, 3, 2, "mat-list-item", 23);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const observables_r20 = i0.ɵɵnextContext().ngIf;
    const ctx_r22 = i0.ɵɵnextContext();
    i0.ɵɵproperty("appOpacityDuration", 1)("itemSize", observables_r20.totalItems)("height", observables_r20.totalItems * 60 > 200 ? 200 : observables_r20.totalItems * 60);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("cdkVirtualForOf", ctx_r22.autocomplete.pageDataSource)("cdkVirtualForTemplateCacheSize", 0);
} }
function AutocompleteComponent_ng_container_10_h2_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h2", 27);
    i0.ɵɵtext(1, "No data to display");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("appOpacityDuration", 1);
} }
function AutocompleteComponent_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template, 1, 0, "mat-progress-bar", 17);
    i0.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template, 3, 5, "cdk-virtual-scroll-viewport", 18);
    i0.ɵɵtemplate(3, AutocompleteComponent_ng_container_10_h2_3_Template, 2, 1, "h2", 19);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const observables_r20 = ctx.ngIf;
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.showScrollView && !observables_r20.hasFetchedData);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && observables_r20.hasDataToShow);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && !observables_r20.hasDataToShow);
} }
const _c1 = function (a0, a1, a2, a3) { return { hasFetchedData: a0, hasDataToShow: a1, isLoading: a2, totalItems: a3 }; };
export class AutocompleteComponent {
    constructor(elRef) {
        this.elRef = elRef;
        this.showScrollView = false;
        this.sort = false;
        this.ascList = [];
        this.descList = [];
        this._previousCriteria = undefined;
        this.onFocus = (el) => {
            // console.log('focus on ', el);
            this.autocomplete.pageDataSource.resetAndFetch(this.criteria);
            this.showScrollView = true;
        };
        this.onModelChange = (criteria, ngModel) => {
            this.autocomplete.criteria = this.criteria;
            if (this._timeoutForFetching)
                clearTimeout(this._timeoutForFetching);
            this._timeoutForFetching = setTimeout(() => {
                if (this._previousCriteria !== this.criteria) {
                    this.autocomplete.pageDataSource.resetAndFetch(this.criteria, this._getSortingParams());
                }
                this._previousCriteria = this.criteria;
            }, 500);
            this.autocomplete.value = undefined;
            this.autocomplete.valueSubject.next(this.autocomplete.value);
            // console.log(this.autocomplete);
        };
        this.itemSelected = (optionItem) => {
            // console.log(optionItem);
            if (optionItem) {
                // console.log(optionItem);
                this.autocomplete.criteria = optionItem.getContextToShow();
                this.autocomplete.value = optionItem;
                this.autocomplete.valueSubject.next(this.autocomplete.value);
                this.criteria = this.autocomplete.criteria;
                this.showScrollView = false;
                this.sort = false;
                // console.log(this.autocomplete);
            }
        };
        this.onSlideToggleChange = (matSlideToggleChange) => {
            this.sort = matSlideToggleChange.checked;
            // console.log(this.sort);
            if (!this.sort)
                this.autocomplete.pageDataSource.resetAndFetch(this.criteria, []);
        };
        this._getSortingParams = () => {
            let sortParams = [];
            this.ascList.forEach((field) => {
                sortParams.push({ field: field, order: 'asc' });
            });
            this.descList.forEach((field) => {
                sortParams.push({ field: field, order: 'desc' });
            });
            return sortParams;
        };
    }
    clickout(event) {
        if (this.elRef.nativeElement.contains(event.target)) {
            // console.log('focus on Autocomplete2Component');
        }
        else {
            // console.log('blur on Autocomplete2Component');
            this.showScrollView = false;
            this.sort = false;
        }
    }
    drop(event) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.autocomplete.pageDataSource.resetAndFetch(this.criteria, this._getSortingParams());
        }
    }
    ngOnInit() {
        this.criteria = this.autocomplete.criteria;
        this._previousCriteria = this.criteria;
        // console.log('Autocomplete2Component init');
    }
    ngOnDestroy() {
        if (this._timeoutForFetching)
            clearTimeout(this._timeoutForFetching);
        if (this.autocomplete.pageDataSource)
            this.autocomplete.pageDataSource.myDisconnect();
        // console.log('Autocomplete2Component destroyed');
    }
}
AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
AutocompleteComponent.ɵcmp = i0.ɵɵdefineComponent({ type: AutocompleteComponent, selectors: [["lib-autocomplete"]], hostBindings: function AutocompleteComponent_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("click", function AutocompleteComponent_click_HostBindingHandler($event) { return ctx.clickout($event); }, false, i0.ɵɵresolveDocument);
    } }, inputs: { autocomplete: "autocomplete" }, decls: 15, vars: 22, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 1, "full-width"], ["fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "4vw", 1, "full-width"], ["fxFlex", "", "appOpacityTransition", "", 3, "appearance", "appOpacityDuration"], ["type", "text", "matInput", "", 3, "ngModel", "disabled", "autocomplete", "ngModelChange", "focus"], ["inputCriteria", "ngModel", "inputEl", ""], ["class", "slide-toggle", "appOpacityTransition", "", 3, "appOpacityDuration", "color", "checked", "disabled", "change", 4, "ngIf"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration", 4, "ngIf"], [4, "ngIf"], ["appOpacityTransition", "", 1, "slide-toggle", 3, "appOpacityDuration", "color", "checked", "disabled", "change"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration"], ["fxLayout", "column", "fxLayoutAlign", "start center", 1, "example-container"], ["cdkDropList", "", 1, "example-list", 3, "cdkDropListData", "cdkDropListConnectedTo", "cdkDropListDropped"], ["criteriaDropList", "cdkDropList"], ["class", "example-box", "cdkDrag", "", 4, "ngFor", "ngForOf"], ["ascDropList", "cdkDropList"], ["descDropList", "cdkDropList"], ["cdkDrag", "", 1, "example-box"], ["mode", "indeterminate", 4, "ngIf"], ["appOpacityTransition", "", "class", "my-viewport", "appHeight", "", 3, "appOpacityDuration", "itemSize", "height", 4, "ngIf"], ["appOpacityTransition", "", 3, "appOpacityDuration", 4, "ngIf"], ["mode", "indeterminate"], ["appOpacityTransition", "", "appHeight", "", 1, "my-viewport", 3, "appOpacityDuration", "itemSize", "height"], ["role", "list"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click", 4, "cdkVirtualFor", "cdkVirtualForOf", "cdkVirtualForTemplateCacheSize"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click"], [3, "diameter", 4, "ngIf"], [3, "diameter"], ["appOpacityTransition", "", 3, "appOpacityDuration"]], template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
        const _r39 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "section", 0);
        i0.ɵɵelementStart(1, "section", 1);
        i0.ɵɵelementStart(2, "mat-form-field", 2);
        i0.ɵɵelementStart(3, "mat-label");
        i0.ɵɵtext(4);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "input", 3, 4);
        i0.ɵɵlistener("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { return ctx.criteria = $event; })("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { i0.ɵɵrestoreView(_r39); const _r0 = i0.ɵɵreference(6); return ctx.onModelChange($event, _r0); })("focus", function AutocompleteComponent_Template_input_focus_5_listener() { i0.ɵɵrestoreView(_r39); const _r1 = i0.ɵɵreference(7); return ctx.onFocus(_r1); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(8, AutocompleteComponent_mat_slide_toggle_8_Template, 2, 4, "mat-slide-toggle", 5);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(9, AutocompleteComponent_section_9_Template, 19, 19, "section", 6);
        i0.ɵɵtemplate(10, AutocompleteComponent_ng_container_10_Template, 4, 3, "ng-container", 7);
        i0.ɵɵpipe(11, "async");
        i0.ɵɵpipe(12, "async");
        i0.ɵɵpipe(13, "async");
        i0.ɵɵpipe(14, "async");
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("appearance", ctx.autocomplete.appearance ? ctx.autocomplete.appearance : "fill")("appOpacityDuration", 1);
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.autocomplete.inputPlaceHolder);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngModel", ctx.criteria)("disabled", ctx.autocomplete.disabled)("autocomplete", "off");
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("ngIf", ctx.showScrollView && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.showScrollView && ctx.sort && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", i0.ɵɵpureFunction4(17, _c1, i0.ɵɵpipeBind1(11, 9, ctx.autocomplete.pageDataSource.getHasFetchedDataSubject()), i0.ɵɵpipeBind1(12, 11, ctx.autocomplete.pageDataSource.getHasDataToShowSubject()), i0.ɵɵpipeBind1(13, 13, ctx.autocomplete.pageDataSource.isLoading()), i0.ɵɵpipeBind1(14, 15, ctx.autocomplete.pageDataSource.getTotalItemsSubject())));
    } }, directives: [i1.DefaultLayoutDirective, i1.DefaultLayoutAlignDirective, i1.DefaultLayoutGapDirective, i2.MatFormField, i1.DefaultFlexDirective, i3.OpacityTransitionDirective, i2.MatLabel, i4.MatInput, i5.DefaultValueAccessor, i5.NgControlStatus, i5.NgModel, i6.NgIf, i7.MatSlideToggle, i8.CdkDropList, i6.NgForOf, i8.CdkDrag, i9.MatProgressBar, i10.CdkVirtualScrollViewport, i10.CdkFixedSizeVirtualScroll, i11.HeightDirective, i12.MatList, i10.CdkVirtualForOf, i12.MatListItem, i13.MatSpinner], pipes: [i6.AsyncPipe], styles: [".my-viewport[_ngcontent-%COMP%] {\n        height: 150px;\n        width: 100%;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }\n\n    h2[_ngcontent-%COMP%] {\n        font: inherit;\n    }\n\n    mat-list-item[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    cdk-virtual-scroll-viewport[_ngcontent-%COMP%] {\n        box-shadow: 1px 2px 3px grey;\n    }\n\n    .full-width[_ngcontent-%COMP%] {\n        width: 100%;\n    }\n\n    \n\n    .example-container[_ngcontent-%COMP%] {\n        \n        margin: 0 25px 25px 0;\n        display: inline-block;\n        vertical-align: top;\n    }\n\n    .example-list[_ngcontent-%COMP%] {\n        border: solid 1px #ccc;\n        min-height: 60px;\n        min-width: 60px;\n        background: white;\n        border-radius: 4px;\n        overflow: hidden;\n        display: block;\n    }\n\n    .example-box[_ngcontent-%COMP%] {\n        padding: 20px 10px;\n        border-bottom: solid 1px #ccc;\n        color: rgba(0, 0, 0, 0.87);\n        display: flex;\n        flex-direction: row;\n        align-items: center;\n        justify-content: space-between;\n        box-sizing: border-box;\n        cursor: move;\n        background: white;\n        font-size: 14px;\n    }\n\n    .cdk-drag-preview[_ngcontent-%COMP%] {\n        box-sizing: border-box;\n        border-radius: 4px;\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\n    }\n\n    .cdk-drag-placeholder[_ngcontent-%COMP%] {\n        opacity: 0;\n    }\n\n    .cdk-drag-animating[_ngcontent-%COMP%] {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }\n\n    .example-box[_ngcontent-%COMP%]:last-child {\n        border: none;\n    }\n\n    .example-list.cdk-drop-list-dragging[_ngcontent-%COMP%]   .example-box[_ngcontent-%COMP%]:not(.cdk-drag-placeholder) {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }"], changeDetection: 0 });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AutocompleteComponent, [{
        type: Component,
        args: [{
                selector: 'lib-autocomplete',
                templateUrl: './autocomplete.component.html',
                styles: [],
                changeDetection: ChangeDetectionStrategy.OnPush,
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { autocomplete: [{
            type: Input
        }], clickout: [{
            type: HostListener,
            args: ['document:click', ['$event']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9hdXRvY29tcGxldGUuY29tcG9uZW50LnRzIiwibGliL2F1dG9jb21wbGV0ZS5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULHVCQUF1QixFQUN2QixLQUFLLEVBR0wsWUFBWSxHQUNiLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFDTCxpQkFBaUIsRUFDakIsZUFBZSxHQUVoQixNQUFNLHdCQUF3QixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztJQ3NHcEIsMkNBUWdEO0lBQTVDLHVPQUEyQztJQUFDLHFCQUFLO0lBQUEsaUJBQW1COztJQUpwRSxzQ0FBd0Isb0JBQUEsa0JBQUEsbUJBQUE7OztJQTRCaEIsK0JBQXVGO0lBQUEsWUFBUTtJQUFBLGlCQUFNOzs7SUFBZCxlQUFRO0lBQVIsOEJBQVE7OztJQWdCL0YsK0JBQW1FO0lBQUEsWUFBUTtJQUFBLGlCQUFNOzs7SUFBZCxlQUFRO0lBQVIsOEJBQVE7OztJQWdCM0UsK0JBQW9FO0lBQUEsWUFBUTtJQUFBLGlCQUFNOzs7SUFBZCxlQUFRO0lBQVIsOEJBQVE7Ozs7O0lBcERoRyxrQ0FRUTtJQUNBLG1DQUdRO0lBQUEsMEJBQUk7SUFBQSx3QkFBUTtJQUFBLGlCQUFLO0lBQ2pCLG1DQU9JO0lBREEsNk5BQW1DO0lBQ25DLGlGQUF1RjtJQUMzRixpQkFBTTtJQUNkLGlCQUFVO0lBR1YsbUNBR1E7SUFBQSwwQkFBSTtJQUFBLG1CQUFHO0lBQUEsaUJBQUs7SUFDWixvQ0FPSTtJQURBLDhOQUFtQztJQUNuQyxtRkFBbUU7SUFDdkUsaUJBQU07SUFDZCxpQkFBVTtJQUdWLG9DQUdRO0lBQUEsMkJBQUk7SUFBQSxxQkFBSTtJQUFBLGlCQUFLO0lBQ2Isb0NBT0k7SUFEQSw4TkFBbUM7SUFDbkMsbUZBQW9FO0lBQ3hFLGlCQUFNO0lBQ2QsaUJBQVU7SUFFbEIsaUJBQVU7Ozs7OztJQXJETixzQ0FBd0I7SUFhUixlQUFvRDtJQUFwRCxvRUFBb0Qsa0VBQUE7SUFJM0IsZUFBcUQ7SUFBckQsNERBQXFEO0lBWTlFLGVBQWdDO0lBQWhDLGdEQUFnQyxrRUFBQTtJQUlQLGVBQWlDO0lBQWpDLHdDQUFpQztJQVkxRCxlQUFpQztJQUFqQyxpREFBaUMsaUVBQUE7SUFJUixlQUFrQztJQUFsQyx5Q0FBa0M7OztJQWF2RSx1Q0FFa0Y7OztJQTBCMUQsMEJBQTJDO0lBQUEsWUFBMkI7SUFBQSxpQkFBSzs7O0lBQWhDLGVBQTJCO0lBQTNCLGlEQUEyQjs7O0lBQ3RFLGtDQUF5RTs7SUFBOUIsNkJBQWU7Ozs7SUFkbEUseUNBYVE7SUFESixzU0FBaUM7SUFDN0Isa0lBQTJDO0lBQzNDLHFKQUEyRDtJQUNuRSxpQkFBZ0I7Ozs7SUFGSixlQUFzQztJQUF0Qyw2REFBc0M7SUFDN0IsZUFBNkI7SUFBN0IsZ0RBQTZCOzs7SUF4QmxFLHVEQVFRO0lBQUEsb0NBRVE7SUFBQSx5SUFhUTtJQUdoQixpQkFBVztJQUNuQixpQkFBOEI7Ozs7SUF4QjFCLHNDQUF3Qix3Q0FBQSx5RkFBQTtJQVNSLGVBT3FDO0lBUHJDLHFFQU9xQyxxQ0FBQTs7O0lBVXpELDhCQUc2QjtJQUFBLGtDQUFrQjtJQUFBLGlCQUFLOztJQUFoRCxzQ0FBd0I7OztJQTVDcEMsNkJBT1E7SUFBQSxpSEFFK0Q7SUFHL0QsdUlBUVE7SUFxQlIscUZBRzZCO0lBRXJDLDBCQUFlOzs7O0lBckNILGVBQTBEO0lBQTFELCtFQUEwRDtJQUkxRCxlQUFzRjtJQUF0RiwrR0FBc0Y7SUE2QnRGLGVBQXVGO0lBQXZGLGdIQUF1Rjs7O0FEMU12RyxNQUFNLE9BQU8scUJBQXFCO0lBWWhDLFlBQW9CLEtBQWlCO1FBQWpCLFVBQUssR0FBTCxLQUFLLENBQVk7UUFUOUIsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsU0FBSSxHQUFZLEtBQUssQ0FBQztRQUN0QixZQUFPLEdBQWEsRUFBRSxDQUFDO1FBQ3ZCLGFBQVEsR0FBYSxFQUFFLENBQUM7UUFJdkIsc0JBQWlCLEdBQVcsU0FBUyxDQUFDO1FBZXZDLFlBQU8sR0FBRyxDQUFDLEVBQW9CLEVBQVEsRUFBRTtZQUM5QyxnQ0FBZ0M7WUFFaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUU5RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUM3QixDQUFDLENBQUM7UUFFSyxrQkFBYSxHQUFHLENBQUMsUUFBYSxFQUFFLE9BQWdCLEVBQVEsRUFBRTtZQUMvRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBRTNDLElBQUksSUFBSSxDQUFDLG1CQUFtQjtnQkFBRSxZQUFZLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ3pDLElBQUksSUFBSSxDQUFDLGlCQUFpQixLQUFLLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQzVDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FDNUMsSUFBSSxDQUFDLFFBQVEsRUFDYixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FDekIsQ0FBQztpQkFDSDtnQkFFRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUN6QyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFFUixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7WUFDcEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0Qsa0NBQWtDO1FBQ3BDLENBQUMsQ0FBQztRQUVLLGlCQUFZLEdBQUcsQ0FBQyxVQUFzQixFQUFRLEVBQUU7WUFDckQsMkJBQTJCO1lBQzNCLElBQUksVUFBVSxFQUFFO2dCQUNkLDJCQUEyQjtnQkFDM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQzNELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQztnQkFDckMsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzdELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7Z0JBRTNDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO2dCQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztnQkFFbEIsa0NBQWtDO2FBQ25DO1FBQ0gsQ0FBQyxDQUFDO1FBRUssd0JBQW1CLEdBQUcsQ0FDM0Isb0JBQTBDLEVBQ3BDLEVBQUU7WUFDUixJQUFJLENBQUMsSUFBSSxHQUFHLG9CQUFvQixDQUFDLE9BQU8sQ0FBQztZQUN6QywwQkFBMEI7WUFFMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJO2dCQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3RFLENBQUMsQ0FBQztRQXVCTSxzQkFBaUIsR0FBRyxHQUFnQixFQUFFO1lBQzVDLElBQUksVUFBVSxHQUFnQixFQUFFLENBQUM7WUFDakMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDN0IsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUM5QixVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztZQUNuRCxDQUFDLENBQUMsQ0FBQztZQUVILE9BQU8sVUFBVSxDQUFDO1FBQ3BCLENBQUMsQ0FBQztJQW5Hc0MsQ0FBQztJQUd6QyxRQUFRLENBQUMsS0FBSztRQUNaLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNuRCxrREFBa0Q7U0FDbkQ7YUFBTTtZQUNMLGlEQUFpRDtZQUNqRCxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztZQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztTQUNuQjtJQUNILENBQUM7SUF3REQsSUFBSSxDQUFDLEtBQTRCO1FBQy9CLElBQUksS0FBSyxDQUFDLGlCQUFpQixLQUFLLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDL0MsZUFBZSxDQUNiLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUNwQixLQUFLLENBQUMsYUFBYSxFQUNuQixLQUFLLENBQUMsWUFBWSxDQUNuQixDQUFDO1NBQ0g7YUFBTTtZQUNMLGlCQUFpQixDQUNmLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQzVCLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUNwQixLQUFLLENBQUMsYUFBYSxFQUNuQixLQUFLLENBQUMsWUFBWSxDQUNuQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUM1QyxJQUFJLENBQUMsUUFBUSxFQUNiLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUN6QixDQUFDO1NBQ0g7SUFDSCxDQUFDO0lBZUQsUUFBUTtRQUNOLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7UUFDM0MsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFFdkMsOENBQThDO0lBQ2hELENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsbUJBQW1CO1lBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3JFLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjO1lBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRWxELG1EQUFtRDtJQUNyRCxDQUFDOzswRkE5SFUscUJBQXFCOzBEQUFyQixxQkFBcUI7d0dBQXJCLG9CQUFnQjs7O1FDNEQ3QixrQ0FNSTtRQUFBLGtDQU1RO1FBQUEseUNBS1E7UUFBQSxpQ0FBVztRQUFBLFlBQXNDO1FBQUEsaUJBQVk7UUFDN0QsbUNBVVI7UUFQWSxnSkFBMkIsaUtBRVYsOEJBQXlDLElBRi9CLDJJQUlsQixnQkFBcUIsSUFKSDtRQUgvQixpQkFVUjtRQUFBLGlCQUFpQjtRQUVqQixnR0FRZ0Q7UUFFeEQsaUJBQVU7UUFFVixnRkFRUTtRQWtEUiwwRkFPUTs7Ozs7UUF5Q1osaUJBQVU7O1FBdklNLGVBQW1GO1FBQW5GLCtGQUFtRix5QkFBQTtRQUlwRSxlQUFzQztRQUF0Qyx1REFBc0M7UUFJN0MsZUFBMkI7UUFBM0Isc0NBQTJCLHVDQUFBLHVCQUFBO1FBV25DLGVBQW1JO1FBQW5JLDhJQUFtSTtRQVczSSxlQUFnSjtRQUFoSiwwSkFBZ0o7UUF5RHRJLGVBS087UUFMUCw2V0FLTzs7a0REcktaLHFCQUFxQjtjQU5qQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsV0FBVyxFQUFFLCtCQUErQjtnQkFDNUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07YUFDaEQ7O2tCQUVFLEtBQUs7O2tCQWFMLFlBQVk7bUJBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIE9uSW5pdCxcbiAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksXG4gIElucHV0LFxuICBPbkRlc3Ryb3ksXG4gIEVsZW1lbnRSZWYsXG4gIEhvc3RMaXN0ZW5lcixcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTb3J0UGFyYW0gfSBmcm9tICcuL3NoYXJlZC9zZXJ2aWNlL3BhZ2VhYmxlLXJlYWQuc2VydmljZSc7XG5pbXBvcnQge1xuICB0cmFuc2ZlckFycmF5SXRlbSxcbiAgbW92ZUl0ZW1JbkFycmF5LFxuICBDZGtEcmFnRHJvcCxcbn0gZnJvbSAnQGFuZ3VsYXIvY2RrL2RyYWctZHJvcCc7XG5pbXBvcnQgeyBNYXRTbGlkZVRvZ2dsZUNoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NsaWRlLXRvZ2dsZSc7XG5pbXBvcnQgeyBPcHRpb25JdGVtIH0gZnJvbSAnLi9zaGFyZWQvbW9kZWwvaW50ZXJmYWNlL29wdGlvbi1pdGVtLmludGVyZmFjZSc7XG5pbXBvcnQgeyBBdXRvY29tcGxldGVJbnRlcmZhY2UgfSBmcm9tICcuL3NoYXJlZC9tb2RlbC9pbnRlcmZhY2UvYXV0b2NvbXBsZXRlLmludGVyZmFjZSc7XG5pbXBvcnQgeyBOZ01vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItYXV0b2NvbXBsZXRlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2F1dG9jb21wbGV0ZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlczogW10sXG4gIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxufSlcbmV4cG9ydCBjbGFzcyBBdXRvY29tcGxldGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIGF1dG9jb21wbGV0ZTogQXV0b2NvbXBsZXRlSW50ZXJmYWNlO1xuXG4gIHB1YmxpYyBzaG93U2Nyb2xsVmlldzogYm9vbGVhbiA9IGZhbHNlO1xuICBwdWJsaWMgc29ydDogYm9vbGVhbiA9IGZhbHNlO1xuICBwdWJsaWMgYXNjTGlzdDogc3RyaW5nW10gPSBbXTtcbiAgcHVibGljIGRlc2NMaXN0OiBzdHJpbmdbXSA9IFtdO1xuICBwdWJsaWMgY3JpdGVyaWE6IHN0cmluZztcblxuICBwcml2YXRlIF90aW1lb3V0Rm9yRmV0Y2hpbmc7XG4gIHByaXZhdGUgX3ByZXZpb3VzQ3JpdGVyaWE6IHN0cmluZyA9IHVuZGVmaW5lZDtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmKSB7fVxuXG4gIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmNsaWNrJywgWyckZXZlbnQnXSlcbiAgY2xpY2tvdXQoZXZlbnQpIHtcbiAgICBpZiAodGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LmNvbnRhaW5zKGV2ZW50LnRhcmdldCkpIHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKCdmb2N1cyBvbiBBdXRvY29tcGxldGUyQ29tcG9uZW50Jyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKCdibHVyIG9uIEF1dG9jb21wbGV0ZTJDb21wb25lbnQnKTtcbiAgICAgIHRoaXMuc2hvd1Njcm9sbFZpZXcgPSBmYWxzZTtcbiAgICAgIHRoaXMuc29ydCA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBvbkZvY3VzID0gKGVsOiBIVE1MSW5wdXRFbGVtZW50KTogdm9pZCA9PiB7XG4gICAgLy8gY29uc29sZS5sb2coJ2ZvY3VzIG9uICcsIGVsKTtcblxuICAgIHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLnJlc2V0QW5kRmV0Y2godGhpcy5jcml0ZXJpYSk7XG5cbiAgICB0aGlzLnNob3dTY3JvbGxWaWV3ID0gdHJ1ZTtcbiAgfTtcblxuICBwdWJsaWMgb25Nb2RlbENoYW5nZSA9IChjcml0ZXJpYTogYW55LCBuZ01vZGVsOiBOZ01vZGVsKTogdm9pZCA9PiB7XG4gICAgdGhpcy5hdXRvY29tcGxldGUuY3JpdGVyaWEgPSB0aGlzLmNyaXRlcmlhO1xuXG4gICAgaWYgKHRoaXMuX3RpbWVvdXRGb3JGZXRjaGluZykgY2xlYXJUaW1lb3V0KHRoaXMuX3RpbWVvdXRGb3JGZXRjaGluZyk7XG4gICAgdGhpcy5fdGltZW91dEZvckZldGNoaW5nID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBpZiAodGhpcy5fcHJldmlvdXNDcml0ZXJpYSAhPT0gdGhpcy5jcml0ZXJpYSkge1xuICAgICAgICB0aGlzLmF1dG9jb21wbGV0ZS5wYWdlRGF0YVNvdXJjZS5yZXNldEFuZEZldGNoKFxuICAgICAgICAgIHRoaXMuY3JpdGVyaWEsXG4gICAgICAgICAgdGhpcy5fZ2V0U29ydGluZ1BhcmFtcygpXG4gICAgICAgICk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3ByZXZpb3VzQ3JpdGVyaWEgPSB0aGlzLmNyaXRlcmlhO1xuICAgIH0sIDUwMCk7XG5cbiAgICB0aGlzLmF1dG9jb21wbGV0ZS52YWx1ZSA9IHVuZGVmaW5lZDtcbiAgICB0aGlzLmF1dG9jb21wbGV0ZS52YWx1ZVN1YmplY3QubmV4dCh0aGlzLmF1dG9jb21wbGV0ZS52YWx1ZSk7XG4gICAgLy8gY29uc29sZS5sb2codGhpcy5hdXRvY29tcGxldGUpO1xuICB9O1xuXG4gIHB1YmxpYyBpdGVtU2VsZWN0ZWQgPSAob3B0aW9uSXRlbTogT3B0aW9uSXRlbSk6IHZvaWQgPT4ge1xuICAgIC8vIGNvbnNvbGUubG9nKG9wdGlvbkl0ZW0pO1xuICAgIGlmIChvcHRpb25JdGVtKSB7XG4gICAgICAvLyBjb25zb2xlLmxvZyhvcHRpb25JdGVtKTtcbiAgICAgIHRoaXMuYXV0b2NvbXBsZXRlLmNyaXRlcmlhID0gb3B0aW9uSXRlbS5nZXRDb250ZXh0VG9TaG93KCk7XG4gICAgICB0aGlzLmF1dG9jb21wbGV0ZS52YWx1ZSA9IG9wdGlvbkl0ZW07XG4gICAgICB0aGlzLmF1dG9jb21wbGV0ZS52YWx1ZVN1YmplY3QubmV4dCh0aGlzLmF1dG9jb21wbGV0ZS52YWx1ZSk7XG4gICAgICB0aGlzLmNyaXRlcmlhID0gdGhpcy5hdXRvY29tcGxldGUuY3JpdGVyaWE7XG5cbiAgICAgIHRoaXMuc2hvd1Njcm9sbFZpZXcgPSBmYWxzZTtcbiAgICAgIHRoaXMuc29ydCA9IGZhbHNlO1xuXG4gICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmF1dG9jb21wbGV0ZSk7XG4gICAgfVxuICB9O1xuXG4gIHB1YmxpYyBvblNsaWRlVG9nZ2xlQ2hhbmdlID0gKFxuICAgIG1hdFNsaWRlVG9nZ2xlQ2hhbmdlOiBNYXRTbGlkZVRvZ2dsZUNoYW5nZVxuICApOiB2b2lkID0+IHtcbiAgICB0aGlzLnNvcnQgPSBtYXRTbGlkZVRvZ2dsZUNoYW5nZS5jaGVja2VkO1xuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuc29ydCk7XG5cbiAgICBpZiAoIXRoaXMuc29ydClcbiAgICAgIHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLnJlc2V0QW5kRmV0Y2godGhpcy5jcml0ZXJpYSwgW10pO1xuICB9O1xuXG4gIGRyb3AoZXZlbnQ6IENka0RyYWdEcm9wPHN0cmluZ1tdPikge1xuICAgIGlmIChldmVudC5wcmV2aW91c0NvbnRhaW5lciA9PT0gZXZlbnQuY29udGFpbmVyKSB7XG4gICAgICBtb3ZlSXRlbUluQXJyYXkoXG4gICAgICAgIGV2ZW50LmNvbnRhaW5lci5kYXRhLFxuICAgICAgICBldmVudC5wcmV2aW91c0luZGV4LFxuICAgICAgICBldmVudC5jdXJyZW50SW5kZXhcbiAgICAgICk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRyYW5zZmVyQXJyYXlJdGVtKFxuICAgICAgICBldmVudC5wcmV2aW91c0NvbnRhaW5lci5kYXRhLFxuICAgICAgICBldmVudC5jb250YWluZXIuZGF0YSxcbiAgICAgICAgZXZlbnQucHJldmlvdXNJbmRleCxcbiAgICAgICAgZXZlbnQuY3VycmVudEluZGV4XG4gICAgICApO1xuICAgICAgdGhpcy5hdXRvY29tcGxldGUucGFnZURhdGFTb3VyY2UucmVzZXRBbmRGZXRjaChcbiAgICAgICAgdGhpcy5jcml0ZXJpYSxcbiAgICAgICAgdGhpcy5fZ2V0U29ydGluZ1BhcmFtcygpXG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgX2dldFNvcnRpbmdQYXJhbXMgPSAoKTogU29ydFBhcmFtW10gPT4ge1xuICAgIGxldCBzb3J0UGFyYW1zOiBTb3J0UGFyYW1bXSA9IFtdO1xuICAgIHRoaXMuYXNjTGlzdC5mb3JFYWNoKChmaWVsZCkgPT4ge1xuICAgICAgc29ydFBhcmFtcy5wdXNoKHsgZmllbGQ6IGZpZWxkLCBvcmRlcjogJ2FzYycgfSk7XG4gICAgfSk7XG5cbiAgICB0aGlzLmRlc2NMaXN0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBzb3J0UGFyYW1zLnB1c2goeyBmaWVsZDogZmllbGQsIG9yZGVyOiAnZGVzYycgfSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gc29ydFBhcmFtcztcbiAgfTtcblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmNyaXRlcmlhID0gdGhpcy5hdXRvY29tcGxldGUuY3JpdGVyaWE7XG4gICAgdGhpcy5fcHJldmlvdXNDcml0ZXJpYSA9IHRoaXMuY3JpdGVyaWE7XG5cbiAgICAvLyBjb25zb2xlLmxvZygnQXV0b2NvbXBsZXRlMkNvbXBvbmVudCBpbml0Jyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5fdGltZW91dEZvckZldGNoaW5nKSBjbGVhclRpbWVvdXQodGhpcy5fdGltZW91dEZvckZldGNoaW5nKTtcbiAgICBpZiAodGhpcy5hdXRvY29tcGxldGUucGFnZURhdGFTb3VyY2UpXG4gICAgICB0aGlzLmF1dG9jb21wbGV0ZS5wYWdlRGF0YVNvdXJjZS5teURpc2Nvbm5lY3QoKTtcblxuICAgIC8vIGNvbnNvbGUubG9nKCdBdXRvY29tcGxldGUyQ29tcG9uZW50IGRlc3Ryb3llZCcpO1xuICB9XG59XG4iLCI8c3R5bGU+XHJcbiAgICAubXktdmlld3BvcnQge1xyXG4gICAgICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICAgIG1pbi13aWR0aDogMjUwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgaDIge1xyXG4gICAgICAgIGZvbnQ6IGluaGVyaXQ7XHJcbiAgICB9XHJcblxyXG4gICAgbWF0LWxpc3QtaXRlbSB7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgfVxyXG5cclxuICAgIGNkay12aXJ0dWFsLXNjcm9sbC12aWV3cG9ydCB7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMXB4IDJweCAzcHggZ3JleTtcclxuICAgIH1cclxuXHJcbiAgICAuZnVsbC13aWR0aCB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgLyogemEgZHJhZyBhbmQgZHJvcCAqL1xyXG5cclxuICAgIC5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgICAgICAgLyogd2lkdGg6IDQwMHB4O1xyXG4gICAgICAgIG1heC13aWR0aDogMTAwJTsgKi9cclxuICAgICAgICBtYXJnaW46IDAgMjVweCAyNXB4IDA7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbiAgICB9XHJcblxyXG4gICAgLmV4YW1wbGUtbGlzdCB7XHJcbiAgICAgICAgYm9yZGVyOiBzb2xpZCAxcHggI2NjYztcclxuICAgICAgICBtaW4taGVpZ2h0OiA2MHB4O1xyXG4gICAgICAgIG1pbi13aWR0aDogNjBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIH1cclxuXHJcbiAgICAuZXhhbXBsZS1ib3gge1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHggMTBweDtcclxuICAgICAgICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2NjYztcclxuICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg3KTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgICAgICBjdXJzb3I6IG1vdmU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5jZGstZHJhZy1wcmV2aWV3IHtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDVweCA1cHggLTNweCByZ2JhKDAsIDAsIDAsIDAuMiksXHJcbiAgICAgICAgICAgICAgICAgICAgMCA4cHggMTBweCAxcHggcmdiYSgwLCAwLCAwLCAwLjE0KSxcclxuICAgICAgICAgICAgICAgICAgICAwIDNweCAxNHB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG4gICAgfVxyXG5cclxuICAgIC5jZGstZHJhZy1wbGFjZWhvbGRlciB7XHJcbiAgICAgICAgb3BhY2l0eTogMDtcclxuICAgIH1cclxuXHJcbiAgICAuY2RrLWRyYWctYW5pbWF0aW5nIHtcclxuICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XHJcbiAgICB9XHJcblxyXG4gICAgLmV4YW1wbGUtYm94Omxhc3QtY2hpbGQge1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgIH1cclxuXHJcbiAgICAuZXhhbXBsZS1saXN0LmNkay1kcm9wLWxpc3QtZHJhZ2dpbmcgLmV4YW1wbGUtYm94Om5vdCguY2RrLWRyYWctcGxhY2Vob2xkZXIpIHtcclxuICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XHJcbiAgICB9XHJcblxyXG48L3N0eWxlPlxyXG5cclxuPHNlY3Rpb25cclxuICAgIGNsYXNzPVwiZnVsbC13aWR0aFwiXHJcbiAgICBmeExheW91dD1cImNvbHVtblwiXHJcbiAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXHJcbiAgICBmeExheW91dEdhcD1cIjJ2d1wiPlxyXG5cclxuICAgIDxzZWN0aW9uXHJcbiAgICAgICAgY2xhc3M9XCJmdWxsLXdpZHRoXCJcclxuICAgICAgICBmeExheW91dFxyXG4gICAgICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCJcclxuICAgICAgICBmeExheW91dEdhcD1cIjR2d1wiPlxyXG5cclxuICAgICAgICAgICAgPG1hdC1mb3JtLWZpZWxkIFxyXG4gICAgICAgICAgICAgICAgW2FwcGVhcmFuY2VdPVwidGhpcy5hdXRvY29tcGxldGUuYXBwZWFyYW5jZSA/IHRoaXMuYXV0b2NvbXBsZXRlLmFwcGVhcmFuY2UgOiAnZmlsbCdcIiBcclxuICAgICAgICAgICAgICAgIGZ4RmxleFxyXG4gICAgICAgICAgICAgICAgYXBwT3BhY2l0eVRyYW5zaXRpb25cclxuICAgICAgICAgICAgICAgIFthcHBPcGFjaXR5RHVyYXRpb25dPVwiMVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxtYXQtbGFiZWw+e3t0aGlzLmF1dG9jb21wbGV0ZS5pbnB1dFBsYWNlSG9sZGVyfX08L21hdC1sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8aW5wdXQgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCIgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hdElucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFsobmdNb2RlbCldPVwidGhpcy5jcml0ZXJpYVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICNpbnB1dENyaXRlcmlhPVwibmdNb2RlbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChuZ01vZGVsQ2hhbmdlKT1cInRoaXMub25Nb2RlbENoYW5nZSgkZXZlbnQsIGlucHV0Q3JpdGVyaWEpXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cInRoaXMuYXV0b2NvbXBsZXRlLmRpc2FibGVkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGZvY3VzKT1cInRoaXMub25Gb2N1cyhpbnB1dEVsKVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFthdXRvY29tcGxldGVdPVwiJ29mZidcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAjaW5wdXRFbD5cclxuICAgICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuICAgICAgICBcclxuICAgICAgICAgICAgPG1hdC1zbGlkZS10b2dnbGVcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwic2xpZGUtdG9nZ2xlXCJcclxuICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zaG93U2Nyb2xsVmlldyAmJiAodGhpcy5hdXRvY29tcGxldGUuc29ydGluZ09wdGlvbnMubGVuZ3RoID4gMCB8fCB0aGlzLmFzY0xpc3QubGVuZ3RoID4gMCB8fCB0aGlzLmRlc2NMaXN0Lmxlbmd0aCA+IDApXCJcclxuICAgICAgICAgICAgICAgIGFwcE9wYWNpdHlUcmFuc2l0aW9uXHJcbiAgICAgICAgICAgICAgICBbYXBwT3BhY2l0eUR1cmF0aW9uXT1cIjFcIlxyXG4gICAgICAgICAgICAgICAgW2NvbG9yXT1cIidwcmltYXJ5J1wiXHJcbiAgICAgICAgICAgICAgICBbY2hlY2tlZF09J2ZhbHNlJ1xyXG4gICAgICAgICAgICAgICAgW2Rpc2FibGVkXT0nZmFsc2UnXHJcbiAgICAgICAgICAgICAgICAoY2hhbmdlKT1cInRoaXMub25TbGlkZVRvZ2dsZUNoYW5nZSgkZXZlbnQpXCI+c29ydD88L21hdC1zbGlkZS10b2dnbGU+XHJcblxyXG4gICAgPC9zZWN0aW9uPlxyXG5cclxuICAgIDxzZWN0aW9uXHJcbiAgICAgICAgKm5nSWY9XCJ0aGlzLnNob3dTY3JvbGxWaWV3ICYmIHRoaXMuc29ydCAmJiAodGhpcy5hdXRvY29tcGxldGUuc29ydGluZ09wdGlvbnMubGVuZ3RoID4gMCB8fCB0aGlzLmFzY0xpc3QubGVuZ3RoID4gMCB8fCB0aGlzLmRlc2NMaXN0Lmxlbmd0aCA+IDApXCJcclxuICAgICAgICBhcHBPcGFjaXR5VHJhbnNpdGlvblxyXG4gICAgICAgIFthcHBPcGFjaXR5RHVyYXRpb25dPVwiMVwiXHJcbiAgICAgICAgZnhMYXlvdXRcclxuICAgICAgICBmeExheW91dEFsaWduPVwiY2VudGVyIGNlbnRlclwiXHJcbiAgICAgICAgZnhMYXlvdXRHYXA9XCIydndcIj5cclxuXHJcbiAgICAgICAgICAgIDwhLS0gY3JpdGVyaWEgc2VjdGlvbiAtLT5cclxuICAgICAgICAgICAgPHNlY3Rpb24gY2xhc3M9XCJleGFtcGxlLWNvbnRhaW5lclwiXHJcbiAgICAgICAgICAgICAgICBmeExheW91dD1cImNvbHVtblwiXHJcbiAgICAgICAgICAgICAgICBmeExheW91dEFsaWduPVwic3RhcnQgY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgyPmNyaXRlcmlhPC9oMj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNka0Ryb3BMaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICNjcml0ZXJpYURyb3BMaXN0PVwiY2RrRHJvcExpc3RcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbY2RrRHJvcExpc3REYXRhXT1cInRoaXMuYXV0b2NvbXBsZXRlLnNvcnRpbmdPcHRpb25zXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgW2Nka0Ryb3BMaXN0Q29ubmVjdGVkVG9dPVwiW2FzY0Ryb3BMaXN0LCBkZXNjRHJvcExpc3RdXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJleGFtcGxlLWxpc3RcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoY2RrRHJvcExpc3REcm9wcGVkKT1cImRyb3AoJGV2ZW50KVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZXhhbXBsZS1ib3hcIiAqbmdGb3I9XCJsZXQgaXRlbSBvZiB0aGlzLmF1dG9jb21wbGV0ZS5zb3J0aW5nT3B0aW9uc1wiIGNka0RyYWc+e3tpdGVtfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBhc2Mgc2VjdGlvbiAtLT5cclxuICAgICAgICAgICAgPHNlY3Rpb24gY2xhc3M9XCJleGFtcGxlLWNvbnRhaW5lclwiXHJcbiAgICAgICAgICAgICAgICBmeExheW91dD1cImNvbHVtblwiXHJcbiAgICAgICAgICAgICAgICBmeExheW91dEFsaWduPVwic3RhcnQgY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgyPmFzYzwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjZGtEcm9wTGlzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAjYXNjRHJvcExpc3Q9XCJjZGtEcm9wTGlzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtjZGtEcm9wTGlzdERhdGFdPVwidGhpcy5hc2NMaXN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgW2Nka0Ryb3BMaXN0Q29ubmVjdGVkVG9dPVwiW2NyaXRlcmlhRHJvcExpc3QsIGRlc2NEcm9wTGlzdF1cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImV4YW1wbGUtbGlzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjZGtEcm9wTGlzdERyb3BwZWQpPVwiZHJvcCgkZXZlbnQpXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJleGFtcGxlLWJveFwiICpuZ0Zvcj1cImxldCBpdGVtIG9mIHRoaXMuYXNjTGlzdFwiIGNka0RyYWc+e3tpdGVtfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBkZXNjIHNlY3Rpb24gLS0+XHJcbiAgICAgICAgICAgIDxzZWN0aW9uIGNsYXNzPVwiZXhhbXBsZS1jb250YWluZXJcIlxyXG4gICAgICAgICAgICAgICAgZnhMYXlvdXQ9XCJjb2x1bW5cIlxyXG4gICAgICAgICAgICAgICAgZnhMYXlvdXRBbGlnbj1cInN0YXJ0IGNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoMj5kZXNjPC9oMj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNka0Ryb3BMaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICNkZXNjRHJvcExpc3Q9XCJjZGtEcm9wTGlzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtjZGtEcm9wTGlzdERhdGFdPVwidGhpcy5kZXNjTGlzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtjZGtEcm9wTGlzdENvbm5lY3RlZFRvXT1cIltjcml0ZXJpYURyb3BMaXN0LCBhc2NEcm9wTGlzdF1cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImV4YW1wbGUtbGlzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjZGtEcm9wTGlzdERyb3BwZWQpPVwiZHJvcCgkZXZlbnQpXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJleGFtcGxlLWJveFwiICpuZ0Zvcj1cImxldCBpdGVtIG9mIHRoaXMuZGVzY0xpc3RcIiBjZGtEcmFnPnt7aXRlbX19PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvc2VjdGlvbj5cclxuICAgICAgICAgICAgXHJcbiAgICA8L3NlY3Rpb24+XHJcblxyXG4gICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cIntcclxuICAgICAgICAgICAgaGFzRmV0Y2hlZERhdGE6IHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLmdldEhhc0ZldGNoZWREYXRhU3ViamVjdCgpIHwgYXN5bmMsXHJcbiAgICAgICAgICAgIGhhc0RhdGFUb1Nob3c6IHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLmdldEhhc0RhdGFUb1Nob3dTdWJqZWN0KCkgfCBhc3luYyxcclxuICAgICAgICAgICAgaXNMb2FkaW5nOiB0aGlzLmF1dG9jb21wbGV0ZS5wYWdlRGF0YVNvdXJjZS5pc0xvYWRpbmcoKSB8IGFzeW5jLFxyXG4gICAgICAgICAgICB0b3RhbEl0ZW1zOiB0aGlzLmF1dG9jb21wbGV0ZS5wYWdlRGF0YVNvdXJjZS5nZXRUb3RhbEl0ZW1zU3ViamVjdCgpIHwgYXN5bmNcclxuICAgICAgICB9IGFzIG9ic2VydmFibGVzXCI+XHJcblxyXG4gICAgICAgICAgICA8bWF0LXByb2dyZXNzLWJhciBcclxuICAgICAgICAgICAgICAgIG1vZGU9XCJpbmRldGVybWluYXRlXCJcclxuICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zaG93U2Nyb2xsVmlldyAmJiAhb2JzZXJ2YWJsZXMuaGFzRmV0Y2hlZERhdGFcIj48L21hdC1wcm9ncmVzcy1iYXI+XHJcblxyXG4gICAgICAgICAgICA8IS0tIFtpdGVtU2l6ZV0gLT4gVGhlIHNpemUgb2YgdGhlIGl0ZW1zIGluIHRoZSBsaXN0IChpbiBwaXhlbHMpLiAtLT5cclxuICAgICAgICAgICAgPGNkay12aXJ0dWFsLXNjcm9sbC12aWV3cG9ydCBcclxuICAgICAgICAgICAgICAgICpuZ0lmPVwidGhpcy5zaG93U2Nyb2xsVmlldyAmJiBvYnNlcnZhYmxlcy5oYXNGZXRjaGVkRGF0YSAmJiBvYnNlcnZhYmxlcy5oYXNEYXRhVG9TaG93XCJcclxuICAgICAgICAgICAgICAgIGFwcE9wYWNpdHlUcmFuc2l0aW9uXHJcbiAgICAgICAgICAgICAgICBbYXBwT3BhY2l0eUR1cmF0aW9uXT1cIjFcIlxyXG4gICAgICAgICAgICAgICAgW2l0ZW1TaXplXT1cIm9ic2VydmFibGVzLnRvdGFsSXRlbXNcIiBcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwibXktdmlld3BvcnRcIlxyXG4gICAgICAgICAgICAgICAgYXBwSGVpZ2h0XHJcbiAgICAgICAgICAgICAgICBbaGVpZ2h0XT1cIm9ic2VydmFibGVzLnRvdGFsSXRlbXMgKiA2MCA+IDIwMCA/IDIwMCA6IG9ic2VydmFibGVzLnRvdGFsSXRlbXMgKiA2MFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxtYXQtbGlzdCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9sZT1cImxpc3RcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxtYXQtbGlzdC1pdGVtIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvbGU9XCJsaXN0aXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKmNka1ZpcnR1YWxGb3I9XCJsZXQgaXRlbSBvZiB0aGlzLmF1dG9jb21wbGV0ZS5wYWdlRGF0YVNvdXJjZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGluZGV4ID0gaW5kZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBjb3VudCA9IGNvdW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZmlyc3QgPSBmaXJzdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGxhc3QgPSBsYXN0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZXZlbiA9IGV2ZW47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBvZGQgPSBvZGQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlQ2FjaGVTaXplOiAwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmeExheW91dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ4TGF5b3V0QWxpZ249XCJzdGFydCBjZW50ZXJcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJ0aGlzLml0ZW1TZWxlY3RlZChpdGVtKVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aDIgKm5nSWY9XCIhb2JzZXJ2YWJsZXMuaXNMb2FkaW5nICYmIGl0ZW1cIj57e2l0ZW0uZ2V0Q29udGV4dFRvU2hvdygpfX08L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bWF0LXNwaW5uZXIgKm5nSWY9XCJvYnNlcnZhYmxlcy5pc0xvYWRpbmdcIiBbZGlhbWV0ZXJdPVwiMjBcIj48L21hdC1zcGlubmVyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9tYXQtbGlzdC1pdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvbWF0LWxpc3Q+XHJcbiAgICAgICAgICAgIDwvY2RrLXZpcnR1YWwtc2Nyb2xsLXZpZXdwb3J0PlxyXG5cclxuICAgICAgICAgICAgPGgyIFxyXG4gICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNob3dTY3JvbGxWaWV3ICYmIG9ic2VydmFibGVzLmhhc0ZldGNoZWREYXRhICYmICFvYnNlcnZhYmxlcy5oYXNEYXRhVG9TaG93XCJcclxuICAgICAgICAgICAgICAgIGFwcE9wYWNpdHlUcmFuc2l0aW9uXHJcbiAgICAgICAgICAgICAgICBbYXBwT3BhY2l0eUR1cmF0aW9uXT1cIjFcIj5ObyBkYXRhIHRvIGRpc3BsYXk8L2gyPlxyXG5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgXHJcbjwvc2VjdGlvbj4iXX0=