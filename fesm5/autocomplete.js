import { ɵɵdirectiveInject, ElementRef, ɵɵdefineDirective, ɵsetClassMetadata, Directive, Input, ɵɵdefineInjectable, Injectable, ɵɵgetCurrentView, ɵɵelementStart, ɵɵlistener, ɵɵrestoreView, ɵɵnextContext, ɵɵtext, ɵɵelementEnd, ɵɵproperty, ɵɵadvance, ɵɵtextInterpolate, ɵɵtemplate, ɵɵreference, ɵɵpureFunction2, ɵɵelement, ɵɵelementContainerStart, ɵɵelementContainerEnd, ɵɵdefineComponent, ɵɵresolveDocument, ɵɵpipe, ɵɵpureFunction4, ɵɵpipeBind1, Component, ChangeDetectionStrategy, HostListener, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { TimelineMax } from 'gsap';
import { __extends, __spread, __values, __read } from 'tslib';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
import { HttpParams, HttpClientModule } from '@angular/common/http';
import { moveItemInArray, transferArrayItem, CdkDropList, CdkDrag, DragDropModule } from '@angular/cdk/drag-drop';
import { DefaultLayoutDirective, DefaultLayoutAlignDirective, DefaultLayoutGapDirective, DefaultFlexDirective } from '@angular/flex-layout/flex';
import { MatFormField, MatLabel, MatFormFieldModule } from '@angular/material/form-field';
import { MatInput, MatInputModule } from '@angular/material/input';
import { DefaultValueAccessor, NgControlStatus, NgModel, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgIf, NgForOf, AsyncPipe, CommonModule } from '@angular/common';
import { MatSlideToggle, MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressBar, MatProgressBarModule } from '@angular/material/progress-bar';
import { CdkVirtualScrollViewport, CdkFixedSizeVirtualScroll, CdkVirtualForOf, ScrollingModule } from '@angular/cdk/scrolling';
import { MatList, MatListItem, MatListModule } from '@angular/material/list';
import { MatSpinner, MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

var HeightDirective = /** @class */ (function () {
    function HeightDirective(elRef) {
        this.elRef = elRef;
    }
    HeightDirective.prototype.ngAfterViewInit = function () {
        this.elRef.nativeElement.style.height = this.height + 'px';
    };
    HeightDirective.ɵfac = function HeightDirective_Factory(t) { return new (t || HeightDirective)(ɵɵdirectiveInject(ElementRef)); };
    HeightDirective.ɵdir = ɵɵdefineDirective({ type: HeightDirective, selectors: [["", "appHeight", ""]], inputs: { height: "height" } });
    return HeightDirective;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(HeightDirective, [{
        type: Directive,
        args: [{
                selector: '[appHeight]',
            }]
    }], function () { return [{ type: ElementRef }]; }, { height: [{
            type: Input
        }] }); })();

var WidthDirective = /** @class */ (function () {
    function WidthDirective(elRef) {
        this.elRef = elRef;
    }
    WidthDirective.prototype.ngAfterViewInit = function () {
        this.elRef.nativeElement.style.width = this.width + 'vw';
    };
    WidthDirective.ɵfac = function WidthDirective_Factory(t) { return new (t || WidthDirective)(ɵɵdirectiveInject(ElementRef)); };
    WidthDirective.ɵdir = ɵɵdefineDirective({ type: WidthDirective, selectors: [["", "appWidth", ""]], inputs: { width: "width" } });
    return WidthDirective;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(WidthDirective, [{
        type: Directive,
        args: [{
                selector: '[appWidth]',
            }]
    }], function () { return [{ type: ElementRef }]; }, { width: [{
            type: Input
        }] }); })();

var OpacityTransitionDirective = /** @class */ (function () {
    function OpacityTransitionDirective(_el) {
        this._el = _el;
        this.appOpacityDuration = 1;
        this._timeline = new TimelineMax();
    }
    OpacityTransitionDirective.prototype.ngAfterViewInit = function () {
        this._timeline.fromTo(this._el.nativeElement, this.appOpacityDuration, { opacity: 0 }, {
            opacity: 1,
            onComplete: function () {
                // console.log('animacija gotova :)');
            },
        });
    };
    OpacityTransitionDirective.ɵfac = function OpacityTransitionDirective_Factory(t) { return new (t || OpacityTransitionDirective)(ɵɵdirectiveInject(ElementRef)); };
    OpacityTransitionDirective.ɵdir = ɵɵdefineDirective({ type: OpacityTransitionDirective, selectors: [["", "appOpacityTransition", ""]], inputs: { appOpacityDuration: "appOpacityDuration" } });
    return OpacityTransitionDirective;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(OpacityTransitionDirective, [{
        type: Directive,
        args: [{
                selector: '[appOpacityTransition]',
            }]
    }], function () { return [{ type: ElementRef }]; }, { appOpacityDuration: [{
            type: Input
        }] }); })();

var StanjeModela;
(function (StanjeModela) {
    StanjeModela["AKTIVAN"] = "AKTIVAN";
    StanjeModela["NEAKTIVAN"] = "NEAKTIVAN";
    StanjeModela["OBRISAN"] = "OBRISAN";
})(StanjeModela || (StanjeModela = {}));

var PageDataSource = /** @class */ (function (_super) {
    __extends(PageDataSource, _super);
    /**
     * @param pageableCrudService posto ce nam trebati service(tipa PageableCrudService) za dobavljanje
     * stranica zahtevamo ga kao argument u konstruktoru
     *
     * @param _pageSize koliko item-a zelimo da prikazemo po stranici
     */
    function PageDataSource(pageableCrudService, _pageSize) {
        if (_pageSize === void 0) { _pageSize = 10; }
        var _this = _super.call(this) || this;
        _this.pageableCrudService = pageableCrudService;
        _this._pageSize = _pageSize;
        _this._cachedData = [];
        _this._fetchedPages = new Set(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
        _this._dataStream = new BehaviorSubject(_this._cachedData);
        _this._sentRequests = 0;
        _this._subscription = new Subscription();
        _this._hasDataToShowSubject = new Subject();
        _this._isLoadingSubject = new Subject();
        _this._hasFetchedDataSubject = new Subject();
        _this._totalItemsSubject = new Subject();
        _this._sortParams = [];
        _this.isLoading = function () {
            return _this._isLoadingSubject;
        };
        _this.getHasDataToShowSubject = function () {
            return _this._hasDataToShowSubject;
        };
        _this.getHasFetchedDataSubject = function () {
            return _this._hasFetchedDataSubject;
        };
        _this.getTotalItemsSubject = function () {
            return _this._totalItemsSubject;
        };
        _this.changePath = function (path) {
            _this.pageableCrudService.setPath(path);
        };
        _this.changeCriteriaPath = function (criteriaPath) {
            _this.pageableCrudService.setCriteriaPath(criteriaPath);
        };
        _this.resetAndFetch = function (criteria, sortParams, pageSize) {
            if (pageSize)
                _this._pageSize = pageSize;
            if (sortParams)
                _this._sortParams = sortParams;
            _this._cachedData.splice(0, _this._cachedData.length);
            _this._fetchedPages.clear(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
            _this._dataStream.next(_this._cachedData);
            _this._hasFetchedDataSubject.next(false);
            // console.log(criteria, this._pageSize);
            _this._fetchPage(0, criteria);
        };
        return _this;
        // this._fetchPage(0);
    }
    PageDataSource.prototype.connect = function (collectionViewer) {
        var _this = this;
        this._subscription.add(collectionViewer.viewChange.subscribe(function (range) {
            // console.log('Range: ', range);
            var startPage = _this._getPageForIndex(range.start);
            // console.log('Start page: ', startPage);
            var endPage = _this._getPageForIndex(range.end - 1);
            // console.log('End page: ', endPage);
            for (var pageNumber = startPage; pageNumber <= endPage; pageNumber++) {
                // console.log('pageNumber: ', pageNumber);
                _this._fetchPage(pageNumber);
            }
        }));
        return this._dataStream;
    };
    PageDataSource.prototype.disconnect = function () {
        // console.log(
        //   'disconnected, obrati paznju na *ngIf nad cdk-virtual-scroll-viewport diskonektovaces se, koristi hidden ili napisi svoju metodu za diskonektovanje'
        // );
        // this._subscription.unsubscribe();
    };
    PageDataSource.prototype.myDisconnect = function () {
        this._subscription.unsubscribe();
    };
    PageDataSource.prototype._getPageForIndex = function (index) {
        return Math.floor(index / this._pageSize);
    };
    PageDataSource.prototype._fetchPage = function (pageNumber, criteria) {
        var _this = this;
        if (this._fetchedPages.has(pageNumber)) {
            return;
        }
        // console.log(
        //   'Fetching a next page, page number: ',
        //   pageNumber,
        //   ', criteria: ',
        //   criteria
        // );
        this._fetchedPages.add(pageNumber);
        this._sentRequests += 1;
        // console.log('zahtevi na cekanju: ', this._sentRequests);
        this._isLoadingSubject.next(this._sentRequests > 0);
        this.pageableCrudService
            .findPageWithPageNumberAndSizeAndSort(pageNumber, this._pageSize, this._sortParams, criteria)
            .subscribe(function (page) {
            // console.log(page);
            // trenutno tu da bismo napravili loading
            setTimeout(function () {
                var _a;
                if (!(_this._cachedData.length > 0)) {
                    _this._cachedData = Array.from({
                        length: page.totalElements,
                    });
                    _this._hasFetchedDataSubject.next(true);
                    _this._hasDataToShowSubject.next(page.totalElements > 0);
                    _this._totalItemsSubject.next(page.totalElements);
                }
                if (page.content) {
                    (_a = _this._cachedData).splice.apply(_a, __spread([pageNumber * _this._pageSize,
                        _this._pageSize], page.content.map(function (g) {
                        return _this.pageableCrudService.buildEntitet(g);
                    })));
                }
                _this._dataStream.next(_this._cachedData);
                // console.log(this._cachedData);
                _this._sentRequests -= 1;
                // console.log('zahtevi na cekanju: ', this._sentRequests);
                _this._isLoadingSubject.next(_this._sentRequests > 0);
            }, 200);
        }, function (err) {
            _this._hasFetchedDataSubject.next(true);
            console.log(err);
            _this._sentRequests -= 1;
            // console.log('zahtevi na cekanju: ', this._sentRequests);
            _this._isLoadingSubject.next(_this._sentRequests > 0);
        }, function () {
            // completed subscription
        });
    };
    return PageDataSource;
}(DataSource));

/*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena

    primer observer-a:

export class AppComponent implements OnInit, OnDestroy {

    articleObserver = (observable: ArticleRepository): void => {
        console.log(observable);
        this.articles = observable.getArticles();
    }

    // interfaces
    ngOnInit() {
        this.articleRepository.attach(this.articleObserver);

        this.articles = this.articleRepository.getArticles();
        console.log("AppComponent init");
    }

    ngOnDestroy() {
        if (this.articleRepository)
            this.articleRepository.dettach(this.articleObserver);
        console.log("AppComponent destroyed");
    }
}
*/
var Observable = /** @class */ (function () {
    function Observable() {
        var _this = this;
        this.observers = new Set();
        this.attach = function (observer) {
            if (!_this.observers.has(observer))
                _this.observers.add(observer);
        };
        this.dettach = function (observer) {
            if (_this.observers.has(observer))
                _this.observers.delete(observer);
        };
        this.notify = function () {
            var e_1, _a;
            try {
                for (var _b = __values(_this.observers), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var observer = _c.value;
                    // ne znam bas koliko ima smisla
                    if (observer) {
                        try {
                            observer.update();
                        }
                        catch (error) {
                            observer();
                        }
                    }
                    else
                        _this.dettach(observer);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        };
    }
    return Observable;
}());

var PageMap = /** @class */ (function () {
    function PageMap() {
        var _this = this;
        this.map = new Map();
        /**
         * prva implementacija metode nikada ne prazni mapu iako se promeni kriterijum za
         * dobavljanje entiteta
         */
        // has = (key: PageKey): boolean => {
        //   // iteriramo kroz sve elemente
        //   for (const [existingKey, value] of this.map) {
        //     if (
        //       existingKey.pageNumber === key.pageNumber &&
        //       existingKey.pageSize === key.pageSize &&
        //       existingKey.sort.empty === key.sort.empty &&
        //       existingKey.sort.sorted === key.sort.sorted &&
        //       existingKey.sort.unsorted === key.sort.unsorted
        //     )
        //       return true;
        //   }
        //   return false;
        // };
        /**
         * druga implementacija metode prazni mapu ako dodje do promene kriterijuma za dobavljanje entiteta
         */
        this.has = function (key) {
            var e_1, _a;
            if (!_this._hasCriteriaChanged(key)) {
                try {
                    for (var _b = __values(_this.map), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var _d = __read(_c.value, 2), existingKey = _d[0], value = _d[1];
                        if (existingKey.pageNumber === key.pageNumber)
                            return true;
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            else {
                _this.map.clear();
                return false;
            }
        };
        this._hasCriteriaChanged = function (key) {
            var e_2, _a;
            try {
                for (var _b = __values(_this.map), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var _d = __read(_c.value, 2), existingKey = _d[0], value = _d[1];
                    if (existingKey.pageSize === key.pageSize &&
                        existingKey.sort.empty === key.sort.empty &&
                        existingKey.sort.sorted === key.sort.sorted &&
                        existingKey.sort.unsorted === key.sort.unsorted)
                        return false;
                    else
                        return true;
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_2) throw e_2.error; }
            }
        };
        this.getMap = function () {
            return _this.map;
        };
        this.clone = function () {
            // vracamo clone
            return new Map(_this.map);
        };
        this.clear = function () {
            _this.map.clear();
        };
    }
    return PageMap;
}());

var AutocompleteService = /** @class */ (function () {
    function AutocompleteService() {
    }
    AutocompleteService.ɵfac = function AutocompleteService_Factory(t) { return new (t || AutocompleteService)(); };
    AutocompleteService.ɵprov = ɵɵdefineInjectable({ token: AutocompleteService, factory: AutocompleteService.ɵfac, providedIn: 'root' });
    return AutocompleteService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(AutocompleteService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

var PageableReadService = /** @class */ (function (_super) {
    __extends(PageableReadService, _super);
    function PageableReadService(httpClient, schema, host, port, defaultPath, defaultCriteriaPath, factory, pageSize) {
        if (pageSize === void 0) { pageSize = 20; }
        var _this = _super.call(this) || this;
        _this.httpClient = httpClient;
        _this.factory = factory;
        _this.pageSize = pageSize;
        _this.pages = new PageMap();
        // ----------------- interface PageableCrudService -----------------
        //   read pages
        _this.findPageWithPageNumber = function (page) {
            if (page === void 0) { page = 0; }
            var url = _this.url + _this.path;
            _this.httpClient
                .get(url, {
                params: new HttpParams()
                    .set('page', page.toString())
                    .set('size', _this.pageSize.toString()),
            })
                .subscribe(function (page) {
                if (_this._addPage(page))
                    _this.notify();
            }, function (err) {
                _this.showError(err);
            });
        };
        //   read pages
        _this.findPageWithPageNumberAndSize = function (page, pageSize, criteriaValue) {
            if (page === void 0) { page = 0; }
            if (pageSize === void 0) { pageSize = 20; }
            var url = _this.url + _this.path;
            if (criteriaValue &&
                criteriaValue !== '' &&
                criteriaValue.replace(/\s/g, ''))
                url = _this.url + _this.criteriaPath + criteriaValue;
            var observable = _this.httpClient.get(url, {
                params: new HttpParams()
                    .set('page', page.toString())
                    .set('size', pageSize.toString()),
            });
            observable.subscribe(function (page) {
                if (_this._addPage(page))
                    _this.notify();
            }, function (err) {
                _this.showError(err);
            });
            return observable;
        };
        // read pages
        _this.findPageWithPageNumberAndSizeAndSort = function (page, pageSize, sortParams, criteriaValue) {
            if (page === void 0) { page = 0; }
            if (pageSize === void 0) { pageSize = 20; }
            if (sortParams === void 0) { sortParams = []; }
            var url = _this.url + _this.path;
            if (criteriaValue &&
                criteriaValue !== '' &&
                criteriaValue.replace(/\s/g, ''))
                url = _this.url + _this.criteriaPath + criteriaValue;
            var httpParams = new HttpParams()
                .set('page', page.toString())
                .set('size', pageSize.toString());
            sortParams.forEach(function (sortParam) {
                httpParams = httpParams.append('sort', sortParam.field + ',' + sortParam.order);
            });
            var observable = _this.httpClient.get(url, {
                params: httpParams,
            });
            observable.subscribe(function (page) {
                if (_this._addPage(page))
                    _this.notify();
            }, function (err) {
                _this.showError(err);
            });
            return observable;
        };
        _this.showError = function (err) {
            console.log(err);
        };
        _this.getPages = function () {
            // vracamo clone
            return _this.pages.clone();
        };
        _this._addPage = function (page) {
            if (!_this.pages.has({
                pageNumber: page.pageable.pageNumber,
                pageSize: page.pageable.pageSize,
                sort: page.pageable.sort,
            })) {
                _this.pages.getMap().set({
                    pageNumber: page.pageable.pageNumber,
                    pageSize: page.pageable.pageSize,
                    sort: page.pageable.sort,
                }, page);
                return true;
            }
            return false;
        };
        _this.getPageSize = function () {
            return _this.pageSize;
        };
        _this.setPageSize = function (pageSize) {
            _this.pageSize = pageSize;
        };
        _this.setPath = function (path) {
            _this.path = path;
        };
        _this.getPath = function () {
            return _this.path;
        };
        _this.setCriteriaPath = function (criteriaPath) {
            _this.criteriaPath = criteriaPath;
        };
        _this.getCriteriaPath = function () {
            return _this.criteriaPath;
        };
        _this.resetPathAndCriteriaPathToDefault = function () {
            _this.path = _this._defaultPath;
            _this.criteriaPath = _this._defaultCriteriaPath;
        };
        _this.buildEntitet = function (e) {
            return _this.factory.build(e);
        };
        _this.url = schema + '://' + host + ':' + port.toString() + '/';
        _this._defaultPath = defaultPath;
        _this.path = defaultPath;
        _this._defaultCriteriaPath = defaultCriteriaPath;
        _this.criteriaPath = defaultCriteriaPath;
        return _this;
    }
    return PageableReadService;
}(Observable));

function AutocompleteComponent_mat_slide_toggle_8_Template(rf, ctx) { if (rf & 1) {
    var _r6 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-slide-toggle", 8);
    ɵɵlistener("change", function AutocompleteComponent_mat_slide_toggle_8_Template_mat_slide_toggle_change_0_listener($event) { ɵɵrestoreView(_r6); var ctx_r5 = ɵɵnextContext(); return ctx_r5.onSlideToggleChange($event); });
    ɵɵtext(1, "sort?");
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵproperty("appOpacityDuration", 1)("color", "primary")("checked", false)("disabled", false);
} }
function AutocompleteComponent_section_9_div_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r13 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(item_r13);
} }
function AutocompleteComponent_section_9_div_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r14 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(item_r14);
} }
function AutocompleteComponent_section_9_div_18_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r15 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(item_r15);
} }
var _c0 = function (a0, a1) { return [a0, a1]; };
function AutocompleteComponent_section_9_Template(rf, ctx) { if (rf & 1) {
    var _r17 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "section", 9);
    ɵɵelementStart(1, "section", 10);
    ɵɵelementStart(2, "h2");
    ɵɵtext(3, "criteria");
    ɵɵelementEnd();
    ɵɵelementStart(4, "div", 11, 12);
    ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_4_listener($event) { ɵɵrestoreView(_r17); var ctx_r16 = ɵɵnextContext(); return ctx_r16.drop($event); });
    ɵɵtemplate(6, AutocompleteComponent_section_9_div_6_Template, 2, 1, "div", 13);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(7, "section", 10);
    ɵɵelementStart(8, "h2");
    ɵɵtext(9, "asc");
    ɵɵelementEnd();
    ɵɵelementStart(10, "div", 11, 14);
    ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_10_listener($event) { ɵɵrestoreView(_r17); var ctx_r18 = ɵɵnextContext(); return ctx_r18.drop($event); });
    ɵɵtemplate(12, AutocompleteComponent_section_9_div_12_Template, 2, 1, "div", 13);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(13, "section", 10);
    ɵɵelementStart(14, "h2");
    ɵɵtext(15, "desc");
    ɵɵelementEnd();
    ɵɵelementStart(16, "div", 11, 15);
    ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_16_listener($event) { ɵɵrestoreView(_r17); var ctx_r19 = ɵɵnextContext(); return ctx_r19.drop($event); });
    ɵɵtemplate(18, AutocompleteComponent_section_9_div_18_Template, 2, 1, "div", 13);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var _r7 = ɵɵreference(5);
    var _r9 = ɵɵreference(11);
    var _r11 = ɵɵreference(17);
    var ctx_r3 = ɵɵnextContext();
    ɵɵproperty("appOpacityDuration", 1);
    ɵɵadvance(4);
    ɵɵproperty("cdkDropListData", ctx_r3.autocomplete.sortingOptions)("cdkDropListConnectedTo", ɵɵpureFunction2(10, _c0, _r9, _r11));
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r3.autocomplete.sortingOptions);
    ɵɵadvance(4);
    ɵɵproperty("cdkDropListData", ctx_r3.ascList)("cdkDropListConnectedTo", ɵɵpureFunction2(13, _c0, _r7, _r11));
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r3.ascList);
    ɵɵadvance(4);
    ɵɵproperty("cdkDropListData", ctx_r3.descList)("cdkDropListConnectedTo", ɵɵpureFunction2(16, _c0, _r7, _r9));
    ɵɵadvance(2);
    ɵɵproperty("ngForOf", ctx_r3.descList);
} }
function AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-progress-bar", 20);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "h2");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r25 = ɵɵnextContext().$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(item_r25.getContextToShow());
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-spinner", 26);
} if (rf & 2) {
    ɵɵproperty("diameter", 20);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template(rf, ctx) { if (rf & 1) {
    var _r36 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-list-item", 24);
    ɵɵlistener("click", function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template_mat_list_item_click_0_listener() { ɵɵrestoreView(_r36); var item_r25 = ctx.$implicit; var ctx_r35 = ɵɵnextContext(3); return ctx_r35.itemSelected(item_r25); });
    ɵɵtemplate(1, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template, 2, 1, "h2", 7);
    ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template, 1, 1, "mat-spinner", 25);
    ɵɵelementEnd();
} if (rf & 2) {
    var item_r25 = ctx.$implicit;
    var observables_r20 = ɵɵnextContext(2).ngIf;
    ɵɵadvance(1);
    ɵɵproperty("ngIf", !observables_r20.isLoading && item_r25);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", observables_r20.isLoading);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "cdk-virtual-scroll-viewport", 21);
    ɵɵelementStart(1, "mat-list", 22);
    ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template, 3, 2, "mat-list-item", 23);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var observables_r20 = ɵɵnextContext().ngIf;
    var ctx_r22 = ɵɵnextContext();
    ɵɵproperty("appOpacityDuration", 1)("itemSize", observables_r20.totalItems)("height", observables_r20.totalItems * 60 > 200 ? 200 : observables_r20.totalItems * 60);
    ɵɵadvance(2);
    ɵɵproperty("cdkVirtualForOf", ctx_r22.autocomplete.pageDataSource)("cdkVirtualForTemplateCacheSize", 0);
} }
function AutocompleteComponent_ng_container_10_h2_3_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "h2", 27);
    ɵɵtext(1, "No data to display");
    ɵɵelementEnd();
} if (rf & 2) {
    ɵɵproperty("appOpacityDuration", 1);
} }
function AutocompleteComponent_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template, 1, 0, "mat-progress-bar", 17);
    ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template, 3, 5, "cdk-virtual-scroll-viewport", 18);
    ɵɵtemplate(3, AutocompleteComponent_ng_container_10_h2_3_Template, 2, 1, "h2", 19);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    var observables_r20 = ctx.ngIf;
    var ctx_r4 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r4.showScrollView && !observables_r20.hasFetchedData);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && observables_r20.hasDataToShow);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && !observables_r20.hasDataToShow);
} }
var _c1 = function (a0, a1, a2, a3) { return { hasFetchedData: a0, hasDataToShow: a1, isLoading: a2, totalItems: a3 }; };
var AutocompleteComponent = /** @class */ (function () {
    function AutocompleteComponent(elRef) {
        var _this = this;
        this.elRef = elRef;
        this.showScrollView = false;
        this.sort = false;
        this.ascList = [];
        this.descList = [];
        this._previousCriteria = undefined;
        this.onFocus = function (el) {
            // console.log('focus on ', el);
            _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria);
            _this.showScrollView = true;
        };
        this.onModelChange = function (criteria, ngModel) {
            _this.autocomplete.criteria = _this.criteria;
            if (_this._timeoutForFetching)
                clearTimeout(_this._timeoutForFetching);
            _this._timeoutForFetching = setTimeout(function () {
                if (_this._previousCriteria !== _this.criteria) {
                    _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria, _this._getSortingParams());
                }
                _this._previousCriteria = _this.criteria;
            }, 500);
            _this.autocomplete.value = undefined;
            _this.autocomplete.valueSubject.next(_this.autocomplete.value);
            // console.log(this.autocomplete);
        };
        this.itemSelected = function (optionItem) {
            // console.log(optionItem);
            if (optionItem) {
                // console.log(optionItem);
                _this.autocomplete.criteria = optionItem.getContextToShow();
                _this.autocomplete.value = optionItem;
                _this.autocomplete.valueSubject.next(_this.autocomplete.value);
                _this.criteria = _this.autocomplete.criteria;
                _this.showScrollView = false;
                _this.sort = false;
                // console.log(this.autocomplete);
            }
        };
        this.onSlideToggleChange = function (matSlideToggleChange) {
            _this.sort = matSlideToggleChange.checked;
            // console.log(this.sort);
            if (!_this.sort)
                _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria, []);
        };
        this._getSortingParams = function () {
            var sortParams = [];
            _this.ascList.forEach(function (field) {
                sortParams.push({ field: field, order: 'asc' });
            });
            _this.descList.forEach(function (field) {
                sortParams.push({ field: field, order: 'desc' });
            });
            return sortParams;
        };
    }
    AutocompleteComponent.prototype.clickout = function (event) {
        if (this.elRef.nativeElement.contains(event.target)) {
            // console.log('focus on Autocomplete2Component');
        }
        else {
            // console.log('blur on Autocomplete2Component');
            this.showScrollView = false;
            this.sort = false;
        }
    };
    AutocompleteComponent.prototype.drop = function (event) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.autocomplete.pageDataSource.resetAndFetch(this.criteria, this._getSortingParams());
        }
    };
    AutocompleteComponent.prototype.ngOnInit = function () {
        this.criteria = this.autocomplete.criteria;
        this._previousCriteria = this.criteria;
        // console.log('Autocomplete2Component init');
    };
    AutocompleteComponent.prototype.ngOnDestroy = function () {
        if (this._timeoutForFetching)
            clearTimeout(this._timeoutForFetching);
        if (this.autocomplete.pageDataSource)
            this.autocomplete.pageDataSource.myDisconnect();
        // console.log('Autocomplete2Component destroyed');
    };
    AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(ɵɵdirectiveInject(ElementRef)); };
    AutocompleteComponent.ɵcmp = ɵɵdefineComponent({ type: AutocompleteComponent, selectors: [["lib-autocomplete"]], hostBindings: function AutocompleteComponent_HostBindings(rf, ctx) { if (rf & 1) {
            ɵɵlistener("click", function AutocompleteComponent_click_HostBindingHandler($event) { return ctx.clickout($event); }, false, ɵɵresolveDocument);
        } }, inputs: { autocomplete: "autocomplete" }, decls: 15, vars: 22, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 1, "full-width"], ["fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "4vw", 1, "full-width"], ["fxFlex", "", "appOpacityTransition", "", 3, "appearance", "appOpacityDuration"], ["type", "text", "matInput", "", 3, "ngModel", "disabled", "autocomplete", "ngModelChange", "focus"], ["inputCriteria", "ngModel", "inputEl", ""], ["class", "slide-toggle", "appOpacityTransition", "", 3, "appOpacityDuration", "color", "checked", "disabled", "change", 4, "ngIf"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration", 4, "ngIf"], [4, "ngIf"], ["appOpacityTransition", "", 1, "slide-toggle", 3, "appOpacityDuration", "color", "checked", "disabled", "change"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration"], ["fxLayout", "column", "fxLayoutAlign", "start center", 1, "example-container"], ["cdkDropList", "", 1, "example-list", 3, "cdkDropListData", "cdkDropListConnectedTo", "cdkDropListDropped"], ["criteriaDropList", "cdkDropList"], ["class", "example-box", "cdkDrag", "", 4, "ngFor", "ngForOf"], ["ascDropList", "cdkDropList"], ["descDropList", "cdkDropList"], ["cdkDrag", "", 1, "example-box"], ["mode", "indeterminate", 4, "ngIf"], ["appOpacityTransition", "", "class", "my-viewport", "appHeight", "", 3, "appOpacityDuration", "itemSize", "height", 4, "ngIf"], ["appOpacityTransition", "", 3, "appOpacityDuration", 4, "ngIf"], ["mode", "indeterminate"], ["appOpacityTransition", "", "appHeight", "", 1, "my-viewport", 3, "appOpacityDuration", "itemSize", "height"], ["role", "list"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click", 4, "cdkVirtualFor", "cdkVirtualForOf", "cdkVirtualForTemplateCacheSize"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click"], [3, "diameter", 4, "ngIf"], [3, "diameter"], ["appOpacityTransition", "", 3, "appOpacityDuration"]], template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
            var _r39 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "section", 0);
            ɵɵelementStart(1, "section", 1);
            ɵɵelementStart(2, "mat-form-field", 2);
            ɵɵelementStart(3, "mat-label");
            ɵɵtext(4);
            ɵɵelementEnd();
            ɵɵelementStart(5, "input", 3, 4);
            ɵɵlistener("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { return ctx.criteria = $event; })("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { ɵɵrestoreView(_r39); var _r0 = ɵɵreference(6); return ctx.onModelChange($event, _r0); })("focus", function AutocompleteComponent_Template_input_focus_5_listener() { ɵɵrestoreView(_r39); var _r1 = ɵɵreference(7); return ctx.onFocus(_r1); });
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵtemplate(8, AutocompleteComponent_mat_slide_toggle_8_Template, 2, 4, "mat-slide-toggle", 5);
            ɵɵelementEnd();
            ɵɵtemplate(9, AutocompleteComponent_section_9_Template, 19, 19, "section", 6);
            ɵɵtemplate(10, AutocompleteComponent_ng_container_10_Template, 4, 3, "ng-container", 7);
            ɵɵpipe(11, "async");
            ɵɵpipe(12, "async");
            ɵɵpipe(13, "async");
            ɵɵpipe(14, "async");
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(2);
            ɵɵproperty("appearance", ctx.autocomplete.appearance ? ctx.autocomplete.appearance : "fill")("appOpacityDuration", 1);
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.autocomplete.inputPlaceHolder);
            ɵɵadvance(1);
            ɵɵproperty("ngModel", ctx.criteria)("disabled", ctx.autocomplete.disabled)("autocomplete", "off");
            ɵɵadvance(3);
            ɵɵproperty("ngIf", ctx.showScrollView && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.showScrollView && ctx.sort && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ɵɵpureFunction4(17, _c1, ɵɵpipeBind1(11, 9, ctx.autocomplete.pageDataSource.getHasFetchedDataSubject()), ɵɵpipeBind1(12, 11, ctx.autocomplete.pageDataSource.getHasDataToShowSubject()), ɵɵpipeBind1(13, 13, ctx.autocomplete.pageDataSource.isLoading()), ɵɵpipeBind1(14, 15, ctx.autocomplete.pageDataSource.getTotalItemsSubject())));
        } }, directives: [DefaultLayoutDirective, DefaultLayoutAlignDirective, DefaultLayoutGapDirective, MatFormField, DefaultFlexDirective, OpacityTransitionDirective, MatLabel, MatInput, DefaultValueAccessor, NgControlStatus, NgModel, NgIf, MatSlideToggle, CdkDropList, NgForOf, CdkDrag, MatProgressBar, CdkVirtualScrollViewport, CdkFixedSizeVirtualScroll, HeightDirective, MatList, CdkVirtualForOf, MatListItem, MatSpinner], pipes: [AsyncPipe], styles: [".my-viewport[_ngcontent-%COMP%] {\n        height: 150px;\n        width: 100%;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }\n\n    h2[_ngcontent-%COMP%] {\n        font: inherit;\n    }\n\n    mat-list-item[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    cdk-virtual-scroll-viewport[_ngcontent-%COMP%] {\n        box-shadow: 1px 2px 3px grey;\n    }\n\n    .full-width[_ngcontent-%COMP%] {\n        width: 100%;\n    }\n\n    \n\n    .example-container[_ngcontent-%COMP%] {\n        \n        margin: 0 25px 25px 0;\n        display: inline-block;\n        vertical-align: top;\n    }\n\n    .example-list[_ngcontent-%COMP%] {\n        border: solid 1px #ccc;\n        min-height: 60px;\n        min-width: 60px;\n        background: white;\n        border-radius: 4px;\n        overflow: hidden;\n        display: block;\n    }\n\n    .example-box[_ngcontent-%COMP%] {\n        padding: 20px 10px;\n        border-bottom: solid 1px #ccc;\n        color: rgba(0, 0, 0, 0.87);\n        display: flex;\n        flex-direction: row;\n        align-items: center;\n        justify-content: space-between;\n        box-sizing: border-box;\n        cursor: move;\n        background: white;\n        font-size: 14px;\n    }\n\n    .cdk-drag-preview[_ngcontent-%COMP%] {\n        box-sizing: border-box;\n        border-radius: 4px;\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\n    }\n\n    .cdk-drag-placeholder[_ngcontent-%COMP%] {\n        opacity: 0;\n    }\n\n    .cdk-drag-animating[_ngcontent-%COMP%] {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }\n\n    .example-box[_ngcontent-%COMP%]:last-child {\n        border: none;\n    }\n\n    .example-list.cdk-drop-list-dragging[_ngcontent-%COMP%]   .example-box[_ngcontent-%COMP%]:not(.cdk-drag-placeholder) {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }"], changeDetection: 0 });
    return AutocompleteComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(AutocompleteComponent, [{
        type: Component,
        args: [{
                selector: 'lib-autocomplete',
                templateUrl: './autocomplete.component.html',
                styles: [],
                changeDetection: ChangeDetectionStrategy.OnPush,
            }]
    }], function () { return [{ type: ElementRef }]; }, { autocomplete: [{
            type: Input
        }], clickout: [{
            type: HostListener,
            args: ['document:click', ['$event']]
        }] }); })();

var material = [
    MatFormFieldModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
];
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule.ɵmod = ɵɵdefineNgModule({ type: MaterialModule });
    MaterialModule.ɵinj = ɵɵdefineInjector({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [__spread(material), MatFormFieldModule,
            MatAutocompleteModule,
            MatSlideToggleModule,
            MatInputModule,
            MatListModule,
            MatProgressSpinnerModule,
            MatProgressBarModule] });
    return MaterialModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(MaterialModule, { imports: [MatFormFieldModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatInputModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatProgressBarModule], exports: [MatFormFieldModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatInputModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatProgressBarModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaterialModule, [{
        type: NgModule,
        args: [{
                imports: __spread(material),
                exports: __spread(material),
            }]
    }], null, null); })();

var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule.ɵmod = ɵɵdefineNgModule({ type: SharedModule });
    SharedModule.ɵinj = ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MaterialModule,
                FlexLayoutModule,
                ScrollingModule,
                DragDropModule,
            ],
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MaterialModule,
            FlexLayoutModule,
            ScrollingModule,
            DragDropModule] });
    return SharedModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(SharedModule, { declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        ScrollingModule,
        DragDropModule], exports: [HeightDirective,
        WidthDirective,
        OpacityTransitionDirective,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        ScrollingModule,
        DragDropModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(SharedModule, [{
        type: NgModule,
        args: [{
                declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                    ScrollingModule,
                    DragDropModule,
                ],
                exports: [
                    HeightDirective,
                    WidthDirective,
                    OpacityTransitionDirective,
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                    ScrollingModule,
                    DragDropModule,
                ],
                entryComponents: [],
            }]
    }], null, null); })();

var AutocompleteModule = /** @class */ (function () {
    function AutocompleteModule() {
    }
    AutocompleteModule.ɵmod = ɵɵdefineNgModule({ type: AutocompleteModule });
    AutocompleteModule.ɵinj = ɵɵdefineInjector({ factory: function AutocompleteModule_Factory(t) { return new (t || AutocompleteModule)(); }, imports: [[
                BrowserModule,
                BrowserAnimationsModule,
                HttpClientModule,
                SharedModule,
            ]] });
    return AutocompleteModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(AutocompleteModule, { declarations: [AutocompleteComponent], imports: [BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        SharedModule], exports: [AutocompleteComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(AutocompleteModule, [{
        type: NgModule,
        args: [{
                declarations: [AutocompleteComponent],
                imports: [
                    BrowserModule,
                    BrowserAnimationsModule,
                    HttpClientModule,
                    SharedModule,
                ],
                exports: [AutocompleteComponent],
            }]
    }], null, null); })();

/*
 * Public API Surface of autocomplete
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AutocompleteComponent, AutocompleteModule, AutocompleteService, HeightDirective, MaterialModule, Observable, OpacityTransitionDirective, PageDataSource, PageMap, PageableReadService, SharedModule, StanjeModela, WidthDirective };
//# sourceMappingURL=autocomplete.js.map
