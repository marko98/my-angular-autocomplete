import { NgModule } from '@angular/core';
import { AutocompleteComponent } from './autocomplete.component';
import { SharedModule } from './shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import * as i0 from "@angular/core";
var AutocompleteModule = /** @class */ (function () {
    function AutocompleteModule() {
    }
    AutocompleteModule.ɵmod = i0.ɵɵdefineNgModule({ type: AutocompleteModule });
    AutocompleteModule.ɵinj = i0.ɵɵdefineInjector({ factory: function AutocompleteModule_Factory(t) { return new (t || AutocompleteModule)(); }, imports: [[
                BrowserModule,
                BrowserAnimationsModule,
                HttpClientModule,
                SharedModule,
            ]] });
    return AutocompleteModule;
}());
export { AutocompleteModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(AutocompleteModule, { declarations: [AutocompleteComponent], imports: [BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        SharedModule], exports: [AutocompleteComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AutocompleteModule, [{
        type: NgModule,
        args: [{
                declarations: [AutocompleteComponent],
                imports: [
                    BrowserModule,
                    BrowserAnimationsModule,
                    HttpClientModule,
                    SharedModule,
                ],
                exports: [AutocompleteComponent],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9hdXRvY29tcGxldGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDakUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3RELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUMvRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFFeEQ7SUFBQTtLQVVrQzswREFBckIsa0JBQWtCO3VIQUFsQixrQkFBa0Isa0JBUnBCO2dCQUNQLGFBQWE7Z0JBQ2IsdUJBQXVCO2dCQUN2QixnQkFBZ0I7Z0JBQ2hCLFlBQVk7YUFDYjs2QkFkSDtDQWlCa0MsQUFWbEMsSUFVa0M7U0FBckIsa0JBQWtCO3dGQUFsQixrQkFBa0IsbUJBVGQscUJBQXFCLGFBRWxDLGFBQWE7UUFDYix1QkFBdUI7UUFDdkIsZ0JBQWdCO1FBQ2hCLFlBQVksYUFFSixxQkFBcUI7a0RBRXBCLGtCQUFrQjtjQVY5QixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMscUJBQXFCLENBQUM7Z0JBQ3JDLE9BQU8sRUFBRTtvQkFDUCxhQUFhO29CQUNiLHVCQUF1QjtvQkFDdkIsZ0JBQWdCO29CQUNoQixZQUFZO2lCQUNiO2dCQUNELE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO2FBQ2pDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEF1dG9jb21wbGV0ZUNvbXBvbmVudCB9IGZyb20gJy4vYXV0b2NvbXBsZXRlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuL3NoYXJlZC9zaGFyZWQubW9kdWxlJztcbmltcG9ydCB7IEJyb3dzZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbmltcG9ydCB7IEJyb3dzZXJBbmltYXRpb25zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci9hbmltYXRpb25zJztcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0F1dG9jb21wbGV0ZUNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBCcm93c2VyTW9kdWxlLFxuICAgIEJyb3dzZXJBbmltYXRpb25zTW9kdWxlLFxuICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgU2hhcmVkTW9kdWxlLFxuICBdLFxuICBleHBvcnRzOiBbQXV0b2NvbXBsZXRlQ29tcG9uZW50XSxcbn0pXG5leHBvcnQgY2xhc3MgQXV0b2NvbXBsZXRlTW9kdWxlIHt9XG4iXX0=