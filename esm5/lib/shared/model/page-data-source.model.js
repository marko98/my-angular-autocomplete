import { __extends, __read, __spread } from "tslib";
import { DataSource, } from '@angular/cdk/collections';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
var PageDataSource = /** @class */ (function (_super) {
    __extends(PageDataSource, _super);
    /**
     * @param pageableCrudService posto ce nam trebati service(tipa PageableCrudService) za dobavljanje
     * stranica zahtevamo ga kao argument u konstruktoru
     *
     * @param _pageSize koliko item-a zelimo da prikazemo po stranici
     */
    function PageDataSource(pageableCrudService, _pageSize) {
        if (_pageSize === void 0) { _pageSize = 10; }
        var _this = _super.call(this) || this;
        _this.pageableCrudService = pageableCrudService;
        _this._pageSize = _pageSize;
        _this._cachedData = [];
        _this._fetchedPages = new Set(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
        _this._dataStream = new BehaviorSubject(_this._cachedData);
        _this._sentRequests = 0;
        _this._subscription = new Subscription();
        _this._hasDataToShowSubject = new Subject();
        _this._isLoadingSubject = new Subject();
        _this._hasFetchedDataSubject = new Subject();
        _this._totalItemsSubject = new Subject();
        _this._sortParams = [];
        _this.isLoading = function () {
            return _this._isLoadingSubject;
        };
        _this.getHasDataToShowSubject = function () {
            return _this._hasDataToShowSubject;
        };
        _this.getHasFetchedDataSubject = function () {
            return _this._hasFetchedDataSubject;
        };
        _this.getTotalItemsSubject = function () {
            return _this._totalItemsSubject;
        };
        _this.changePath = function (path) {
            _this.pageableCrudService.setPath(path);
        };
        _this.changeCriteriaPath = function (criteriaPath) {
            _this.pageableCrudService.setCriteriaPath(criteriaPath);
        };
        _this.resetAndFetch = function (criteria, sortParams, pageSize) {
            if (pageSize)
                _this._pageSize = pageSize;
            if (sortParams)
                _this._sortParams = sortParams;
            _this._cachedData.splice(0, _this._cachedData.length);
            _this._fetchedPages.clear(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
            _this._dataStream.next(_this._cachedData);
            _this._hasFetchedDataSubject.next(false);
            // console.log(criteria, this._pageSize);
            _this._fetchPage(0, criteria);
        };
        return _this;
        // this._fetchPage(0);
    }
    PageDataSource.prototype.connect = function (collectionViewer) {
        var _this = this;
        this._subscription.add(collectionViewer.viewChange.subscribe(function (range) {
            // console.log('Range: ', range);
            var startPage = _this._getPageForIndex(range.start);
            // console.log('Start page: ', startPage);
            var endPage = _this._getPageForIndex(range.end - 1);
            // console.log('End page: ', endPage);
            for (var pageNumber = startPage; pageNumber <= endPage; pageNumber++) {
                // console.log('pageNumber: ', pageNumber);
                _this._fetchPage(pageNumber);
            }
        }));
        return this._dataStream;
    };
    PageDataSource.prototype.disconnect = function () {
        // console.log(
        //   'disconnected, obrati paznju na *ngIf nad cdk-virtual-scroll-viewport diskonektovaces se, koristi hidden ili napisi svoju metodu za diskonektovanje'
        // );
        // this._subscription.unsubscribe();
    };
    PageDataSource.prototype.myDisconnect = function () {
        this._subscription.unsubscribe();
    };
    PageDataSource.prototype._getPageForIndex = function (index) {
        return Math.floor(index / this._pageSize);
    };
    PageDataSource.prototype._fetchPage = function (pageNumber, criteria) {
        var _this = this;
        if (this._fetchedPages.has(pageNumber)) {
            return;
        }
        // console.log(
        //   'Fetching a next page, page number: ',
        //   pageNumber,
        //   ', criteria: ',
        //   criteria
        // );
        this._fetchedPages.add(pageNumber);
        this._sentRequests += 1;
        // console.log('zahtevi na cekanju: ', this._sentRequests);
        this._isLoadingSubject.next(this._sentRequests > 0);
        this.pageableCrudService
            .findPageWithPageNumberAndSizeAndSort(pageNumber, this._pageSize, this._sortParams, criteria)
            .subscribe(function (page) {
            // console.log(page);
            // trenutno tu da bismo napravili loading
            setTimeout(function () {
                var _a;
                if (!(_this._cachedData.length > 0)) {
                    _this._cachedData = Array.from({
                        length: page.totalElements,
                    });
                    _this._hasFetchedDataSubject.next(true);
                    _this._hasDataToShowSubject.next(page.totalElements > 0);
                    _this._totalItemsSubject.next(page.totalElements);
                }
                if (page.content) {
                    (_a = _this._cachedData).splice.apply(_a, __spread([pageNumber * _this._pageSize,
                        _this._pageSize], page.content.map(function (g) {
                        return _this.pageableCrudService.buildEntitet(g);
                    })));
                }
                _this._dataStream.next(_this._cachedData);
                // console.log(this._cachedData);
                _this._sentRequests -= 1;
                // console.log('zahtevi na cekanju: ', this._sentRequests);
                _this._isLoadingSubject.next(_this._sentRequests > 0);
            }, 200);
        }, function (err) {
            _this._hasFetchedDataSubject.next(true);
            console.log(err);
            _this._sentRequests -= 1;
            // console.log('zahtevi na cekanju: ', this._sentRequests);
            _this._isLoadingSubject.next(_this._sentRequests > 0);
        }, function () {
            // completed subscription
        });
    };
    return PageDataSource;
}(DataSource));
export { PageDataSource };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS1kYXRhLXNvdXJjZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvcGFnZS1kYXRhLXNvdXJjZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUNMLFVBQVUsR0FHWCxNQUFNLDBCQUEwQixDQUFDO0FBQ2xDLE9BQU8sRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQU8xRTtJQUlVLGtDQUFzQjtJQWE5Qjs7Ozs7T0FLRztJQUNILHdCQUNZLG1CQUF3RCxFQUN4RCxTQUFjO1FBQWQsMEJBQUEsRUFBQSxjQUFjO1FBRjFCLFlBSUUsaUJBQU8sU0FFUjtRQUxXLHlCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUM7UUFDeEQsZUFBUyxHQUFULFNBQVMsQ0FBSztRQXBCaEIsaUJBQVcsR0FBaUIsRUFBRSxDQUFDO1FBQy9CLG1CQUFhLEdBQUcsSUFBSSxHQUFHLEVBQVUsQ0FBQyxDQUFDLDhEQUE4RDtRQUNqRyxpQkFBVyxHQUFHLElBQUksZUFBZSxDQUFlLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNsRSxtQkFBYSxHQUFXLENBQUMsQ0FBQztRQUMxQixtQkFBYSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFckMsMkJBQXFCLEdBQXFCLElBQUksT0FBTyxFQUFFLENBQUM7UUFDeEQsdUJBQWlCLEdBQXFCLElBQUksT0FBTyxFQUFFLENBQUM7UUFDcEQsNEJBQXNCLEdBQXFCLElBQUksT0FBTyxFQUFFLENBQUM7UUFDekQsd0JBQWtCLEdBQW9CLElBQUksT0FBTyxFQUFFLENBQUM7UUFDcEQsaUJBQVcsR0FBZ0IsRUFBRSxDQUFDO1FBcUgvQixlQUFTLEdBQUc7WUFDakIsT0FBTyxLQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDaEMsQ0FBQyxDQUFDO1FBRUssNkJBQXVCLEdBQUc7WUFDL0IsT0FBTyxLQUFJLENBQUMscUJBQXFCLENBQUM7UUFDcEMsQ0FBQyxDQUFDO1FBRUssOEJBQXdCLEdBQUc7WUFDaEMsT0FBTyxLQUFJLENBQUMsc0JBQXNCLENBQUM7UUFDckMsQ0FBQyxDQUFDO1FBRUssMEJBQW9CLEdBQUc7WUFDNUIsT0FBTyxLQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDakMsQ0FBQyxDQUFDO1FBRUssZ0JBQVUsR0FBRyxVQUFDLElBQVk7WUFDL0IsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUM7UUFFSyx3QkFBa0IsR0FBRyxVQUFDLFlBQW9CO1lBQy9DLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekQsQ0FBQyxDQUFDO1FBRUssbUJBQWEsR0FBRyxVQUNyQixRQUFnQixFQUNoQixVQUF3QixFQUN4QixRQUFpQjtZQUVqQixJQUFJLFFBQVE7Z0JBQUUsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7WUFFeEMsSUFBSSxVQUFVO2dCQUFFLEtBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDO1lBRTlDLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BELEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyw4REFBOEQ7WUFDMUYsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRXhDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFeEMseUNBQXlDO1lBQ3pDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQzs7UUFqSkEsc0JBQXNCO0lBQ3hCLENBQUM7SUFFRCxnQ0FBTyxHQUFQLFVBQVEsZ0JBQWtDO1FBQTFDLGlCQWVDO1FBZEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQ3BCLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBQyxLQUFnQjtZQUNyRCxpQ0FBaUM7WUFDakMsSUFBTSxTQUFTLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyRCwwQ0FBMEM7WUFDMUMsSUFBTSxPQUFPLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDckQsc0NBQXNDO1lBQ3RDLEtBQUssSUFBSSxVQUFVLEdBQUcsU0FBUyxFQUFFLFVBQVUsSUFBSSxPQUFPLEVBQUUsVUFBVSxFQUFFLEVBQUU7Z0JBQ3BFLDJDQUEyQztnQkFDM0MsS0FBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUM3QjtRQUNILENBQUMsQ0FBQyxDQUNILENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDMUIsQ0FBQztJQUVELG1DQUFVLEdBQVY7UUFDRSxlQUFlO1FBQ2YseUpBQXlKO1FBQ3pKLEtBQUs7UUFDTCxvQ0FBb0M7SUFDdEMsQ0FBQztJQUVELHFDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFFUyx5Q0FBZ0IsR0FBMUIsVUFBMkIsS0FBYTtRQUN0QyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRVMsbUNBQVUsR0FBcEIsVUFBcUIsVUFBa0IsRUFBRSxRQUFpQjtRQUExRCxpQkFtRUM7UUFsRUMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUN0QyxPQUFPO1NBQ1I7UUFDRCxlQUFlO1FBQ2YsMkNBQTJDO1FBQzNDLGdCQUFnQjtRQUNoQixvQkFBb0I7UUFDcEIsYUFBYTtRQUNiLEtBQUs7UUFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUVuQyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsQ0FBQztRQUN4QiwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxtQkFBbUI7YUFDckIsb0NBQW9DLENBQ25DLFVBQVUsRUFDVixJQUFJLENBQUMsU0FBUyxFQUNkLElBQUksQ0FBQyxXQUFXLEVBQ2hCLFFBQVEsQ0FDVDthQUNBLFNBQVMsQ0FDUixVQUFDLElBQWlCO1lBQ2hCLHFCQUFxQjtZQUNyQix5Q0FBeUM7WUFDekMsVUFBVSxDQUFDOztnQkFDVCxJQUFJLENBQUMsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDbEMsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFhO3dCQUN4QyxNQUFNLEVBQUUsSUFBSSxDQUFDLGFBQWE7cUJBQzNCLENBQUMsQ0FBQztvQkFFSCxLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN2QyxLQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3hELEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2lCQUNsRDtnQkFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2hCLENBQUEsS0FBQSxLQUFJLENBQUMsV0FBVyxDQUFBLENBQUMsTUFBTSxxQkFDckIsVUFBVSxHQUFHLEtBQUksQ0FBQyxTQUFTO3dCQUMzQixLQUFJLENBQUMsU0FBUyxHQUNYLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQUMsQ0FBSTt3QkFDdkIsT0FBTyxLQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNsRCxDQUFDLENBQUMsR0FDRjtpQkFDSDtnQkFDRCxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3hDLGlDQUFpQztnQkFFakMsS0FBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLENBQUM7Z0JBQ3hCLDJEQUEyRDtnQkFDM0QsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3RELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNWLENBQUMsRUFDRCxVQUFDLEdBQUc7WUFDRixLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFakIsS0FBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLENBQUM7WUFDeEIsMkRBQTJEO1lBQzNELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN0RCxDQUFDLEVBQ0Q7WUFDRSx5QkFBeUI7UUFDM0IsQ0FBQyxDQUNGLENBQUM7SUFDTixDQUFDO0lBNENILHFCQUFDO0FBQUQsQ0FBQyxBQTlLRCxDQUlVLFVBQVUsR0EwS25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBEYXRhU291cmNlLFxyXG4gIENvbGxlY3Rpb25WaWV3ZXIsXHJcbiAgTGlzdFJhbmdlLFxyXG59IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgU3Vic2NyaXB0aW9uLCBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFBhZ2UsIFNvcnRQYXJhbSB9IGZyb20gJy4uL3NlcnZpY2UvcGFnZWFibGUtcmVhZC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT3B0aW9uSXRlbSB9IGZyb20gJy4vaW50ZXJmYWNlL29wdGlvbi1pdGVtLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IEVudGl0ZXQgfSBmcm9tICcuL2ludGVyZmFjZS9lbnRpdGV0LmludGVyZmFjZSc7XHJcbmltcG9ydCB7IEVOVElURVRfTU9ERUxfSU5URVJGQUNFIH0gZnJvbSAnLi9pbnRlcmZhY2UvZW50aXRldC5tb2RlbC1pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBQYWdlYWJsZVJlYWRTZXJ2aWNlSW50ZXJmYWNlIH0gZnJvbSAnLi4vc2VydmljZS9pbnRlcmZhY2UvcGFnZWFibGUtcmVhZC1zZXJ2aWNlLmludGVyZmFjZSc7XHJcblxyXG5leHBvcnQgY2xhc3MgUGFnZURhdGFTb3VyY2U8XHJcbiAgVCBleHRlbmRzIEVudGl0ZXQ8VCwgSUQ+LFxyXG4gIElELFxyXG4gIEcgZXh0ZW5kcyBFTlRJVEVUX01PREVMX0lOVEVSRkFDRVxyXG4+IGV4dGVuZHMgRGF0YVNvdXJjZTxPcHRpb25JdGVtPiB7XHJcbiAgcHJvdGVjdGVkIF9jYWNoZWREYXRhOiBPcHRpb25JdGVtW10gPSBbXTtcclxuICBwcm90ZWN0ZWQgX2ZldGNoZWRQYWdlcyA9IG5ldyBTZXQ8bnVtYmVyPigpOyAvLyBza3VwIGJyb2pldmEga29qaSBwcmVkc3RhdmxqYWp1IGJyb2pldmUgZG9odmFjZW5paCBzdHJhbmljYVxyXG4gIHByb3RlY3RlZCBfZGF0YVN0cmVhbSA9IG5ldyBCZWhhdmlvclN1YmplY3Q8T3B0aW9uSXRlbVtdPih0aGlzLl9jYWNoZWREYXRhKTtcclxuICBwcm90ZWN0ZWQgX3NlbnRSZXF1ZXN0czogbnVtYmVyID0gMDtcclxuICBwcm90ZWN0ZWQgX3N1YnNjcmlwdGlvbiA9IG5ldyBTdWJzY3JpcHRpb24oKTtcclxuXHJcbiAgcHJpdmF0ZSBfaGFzRGF0YVRvU2hvd1N1YmplY3Q6IFN1YmplY3Q8Ym9vbGVhbj4gPSBuZXcgU3ViamVjdCgpO1xyXG4gIHByaXZhdGUgX2lzTG9hZGluZ1N1YmplY3Q6IFN1YmplY3Q8Ym9vbGVhbj4gPSBuZXcgU3ViamVjdCgpO1xyXG4gIHByaXZhdGUgX2hhc0ZldGNoZWREYXRhU3ViamVjdDogU3ViamVjdDxib29sZWFuPiA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgcHJpdmF0ZSBfdG90YWxJdGVtc1N1YmplY3Q6IFN1YmplY3Q8bnVtYmVyPiA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgcHJpdmF0ZSBfc29ydFBhcmFtczogU29ydFBhcmFtW10gPSBbXTtcclxuXHJcbiAgLyoqXHJcbiAgICogQHBhcmFtIHBhZ2VhYmxlQ3J1ZFNlcnZpY2UgcG9zdG8gY2UgbmFtIHRyZWJhdGkgc2VydmljZSh0aXBhIFBhZ2VhYmxlQ3J1ZFNlcnZpY2UpIHphIGRvYmF2bGphbmplXHJcbiAgICogc3RyYW5pY2EgemFodGV2YW1vIGdhIGthbyBhcmd1bWVudCB1IGtvbnN0cnVrdG9ydVxyXG4gICAqXHJcbiAgICogQHBhcmFtIF9wYWdlU2l6ZSBrb2xpa28gaXRlbS1hIHplbGltbyBkYSBwcmlrYXplbW8gcG8gc3RyYW5pY2lcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByb3RlY3RlZCBwYWdlYWJsZUNydWRTZXJ2aWNlOiBQYWdlYWJsZVJlYWRTZXJ2aWNlSW50ZXJmYWNlPFQsIElEPixcclxuICAgIHByb3RlY3RlZCBfcGFnZVNpemUgPSAxMFxyXG4gICkge1xyXG4gICAgc3VwZXIoKTtcclxuICAgIC8vIHRoaXMuX2ZldGNoUGFnZSgwKTtcclxuICB9XHJcblxyXG4gIGNvbm5lY3QoY29sbGVjdGlvblZpZXdlcjogQ29sbGVjdGlvblZpZXdlcik6IE9ic2VydmFibGU8T3B0aW9uSXRlbVtdPiB7XHJcbiAgICB0aGlzLl9zdWJzY3JpcHRpb24uYWRkKFxyXG4gICAgICBjb2xsZWN0aW9uVmlld2VyLnZpZXdDaGFuZ2Uuc3Vic2NyaWJlKChyYW5nZTogTGlzdFJhbmdlKSA9PiB7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coJ1JhbmdlOiAnLCByYW5nZSk7XHJcbiAgICAgICAgY29uc3Qgc3RhcnRQYWdlID0gdGhpcy5fZ2V0UGFnZUZvckluZGV4KHJhbmdlLnN0YXJ0KTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZygnU3RhcnQgcGFnZTogJywgc3RhcnRQYWdlKTtcclxuICAgICAgICBjb25zdCBlbmRQYWdlID0gdGhpcy5fZ2V0UGFnZUZvckluZGV4KHJhbmdlLmVuZCAtIDEpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdFbmQgcGFnZTogJywgZW5kUGFnZSk7XHJcbiAgICAgICAgZm9yIChsZXQgcGFnZU51bWJlciA9IHN0YXJ0UGFnZTsgcGFnZU51bWJlciA8PSBlbmRQYWdlOyBwYWdlTnVtYmVyKyspIHtcclxuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdwYWdlTnVtYmVyOiAnLCBwYWdlTnVtYmVyKTtcclxuICAgICAgICAgIHRoaXMuX2ZldGNoUGFnZShwYWdlTnVtYmVyKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gICAgcmV0dXJuIHRoaXMuX2RhdGFTdHJlYW07XHJcbiAgfVxyXG5cclxuICBkaXNjb25uZWN0KCk6IHZvaWQge1xyXG4gICAgLy8gY29uc29sZS5sb2coXHJcbiAgICAvLyAgICdkaXNjb25uZWN0ZWQsIG9icmF0aSBwYXpuanUgbmEgKm5nSWYgbmFkIGNkay12aXJ0dWFsLXNjcm9sbC12aWV3cG9ydCBkaXNrb25la3RvdmFjZXMgc2UsIGtvcmlzdGkgaGlkZGVuIGlsaSBuYXBpc2kgc3ZvanUgbWV0b2R1IHphIGRpc2tvbmVrdG92YW5qZSdcclxuICAgIC8vICk7XHJcbiAgICAvLyB0aGlzLl9zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICB9XHJcblxyXG4gIG15RGlzY29ubmVjdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuX3N1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIF9nZXRQYWdlRm9ySW5kZXgoaW5kZXg6IG51bWJlcik6IG51bWJlciB7XHJcbiAgICByZXR1cm4gTWF0aC5mbG9vcihpbmRleCAvIHRoaXMuX3BhZ2VTaXplKTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBfZmV0Y2hQYWdlKHBhZ2VOdW1iZXI6IG51bWJlciwgY3JpdGVyaWE/OiBzdHJpbmcpIHtcclxuICAgIGlmICh0aGlzLl9mZXRjaGVkUGFnZXMuaGFzKHBhZ2VOdW1iZXIpKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIC8vIGNvbnNvbGUubG9nKFxyXG4gICAgLy8gICAnRmV0Y2hpbmcgYSBuZXh0IHBhZ2UsIHBhZ2UgbnVtYmVyOiAnLFxyXG4gICAgLy8gICBwYWdlTnVtYmVyLFxyXG4gICAgLy8gICAnLCBjcml0ZXJpYTogJyxcclxuICAgIC8vICAgY3JpdGVyaWFcclxuICAgIC8vICk7XHJcbiAgICB0aGlzLl9mZXRjaGVkUGFnZXMuYWRkKHBhZ2VOdW1iZXIpO1xyXG5cclxuICAgIHRoaXMuX3NlbnRSZXF1ZXN0cyArPSAxO1xyXG4gICAgLy8gY29uc29sZS5sb2coJ3phaHRldmkgbmEgY2VrYW5qdTogJywgdGhpcy5fc2VudFJlcXVlc3RzKTtcclxuICAgIHRoaXMuX2lzTG9hZGluZ1N1YmplY3QubmV4dCh0aGlzLl9zZW50UmVxdWVzdHMgPiAwKTtcclxuICAgIHRoaXMucGFnZWFibGVDcnVkU2VydmljZVxyXG4gICAgICAuZmluZFBhZ2VXaXRoUGFnZU51bWJlckFuZFNpemVBbmRTb3J0KFxyXG4gICAgICAgIHBhZ2VOdW1iZXIsXHJcbiAgICAgICAgdGhpcy5fcGFnZVNpemUsXHJcbiAgICAgICAgdGhpcy5fc29ydFBhcmFtcyxcclxuICAgICAgICBjcml0ZXJpYVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgKHBhZ2U6IFBhZ2U8VCwgSUQ+KSA9PiB7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyhwYWdlKTtcclxuICAgICAgICAgIC8vIHRyZW51dG5vIHR1IGRhIGJpc21vIG5hcHJhdmlsaSBsb2FkaW5nXHJcbiAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKCEodGhpcy5fY2FjaGVkRGF0YS5sZW5ndGggPiAwKSkge1xyXG4gICAgICAgICAgICAgIHRoaXMuX2NhY2hlZERhdGEgPSBBcnJheS5mcm9tPE9wdGlvbkl0ZW0+KHtcclxuICAgICAgICAgICAgICAgIGxlbmd0aDogcGFnZS50b3RhbEVsZW1lbnRzLFxyXG4gICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICB0aGlzLl9oYXNGZXRjaGVkRGF0YVN1YmplY3QubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICB0aGlzLl9oYXNEYXRhVG9TaG93U3ViamVjdC5uZXh0KHBhZ2UudG90YWxFbGVtZW50cyA+IDApO1xyXG4gICAgICAgICAgICAgIHRoaXMuX3RvdGFsSXRlbXNTdWJqZWN0Lm5leHQocGFnZS50b3RhbEVsZW1lbnRzKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHBhZ2UuY29udGVudCkge1xyXG4gICAgICAgICAgICAgIHRoaXMuX2NhY2hlZERhdGEuc3BsaWNlKFxyXG4gICAgICAgICAgICAgICAgcGFnZU51bWJlciAqIHRoaXMuX3BhZ2VTaXplLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fcGFnZVNpemUsXHJcbiAgICAgICAgICAgICAgICAuLi5wYWdlLmNvbnRlbnQubWFwKChnOiBHKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnBhZ2VhYmxlQ3J1ZFNlcnZpY2UuYnVpbGRFbnRpdGV0KGcpO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuX2RhdGFTdHJlYW0ubmV4dCh0aGlzLl9jYWNoZWREYXRhKTtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5fY2FjaGVkRGF0YSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9zZW50UmVxdWVzdHMgLT0gMTtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ3phaHRldmkgbmEgY2VrYW5qdTogJywgdGhpcy5fc2VudFJlcXVlc3RzKTtcclxuICAgICAgICAgICAgdGhpcy5faXNMb2FkaW5nU3ViamVjdC5uZXh0KHRoaXMuX3NlbnRSZXF1ZXN0cyA+IDApO1xyXG4gICAgICAgICAgfSwgMjAwKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgIHRoaXMuX2hhc0ZldGNoZWREYXRhU3ViamVjdC5uZXh0KHRydWUpO1xyXG5cclxuICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XHJcblxyXG4gICAgICAgICAgdGhpcy5fc2VudFJlcXVlc3RzIC09IDE7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZygnemFodGV2aSBuYSBjZWthbmp1OiAnLCB0aGlzLl9zZW50UmVxdWVzdHMpO1xyXG4gICAgICAgICAgdGhpcy5faXNMb2FkaW5nU3ViamVjdC5uZXh0KHRoaXMuX3NlbnRSZXF1ZXN0cyA+IDApO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgLy8gY29tcGxldGVkIHN1YnNjcmlwdGlvblxyXG4gICAgICAgIH1cclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBpc0xvYWRpbmcgPSAoKTogU3ViamVjdDxib29sZWFuPiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5faXNMb2FkaW5nU3ViamVjdDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0SGFzRGF0YVRvU2hvd1N1YmplY3QgPSAoKTogU3ViamVjdDxib29sZWFuPiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5faGFzRGF0YVRvU2hvd1N1YmplY3Q7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldEhhc0ZldGNoZWREYXRhU3ViamVjdCA9ICgpOiBTdWJqZWN0PGJvb2xlYW4+ID0+IHtcclxuICAgIHJldHVybiB0aGlzLl9oYXNGZXRjaGVkRGF0YVN1YmplY3Q7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGdldFRvdGFsSXRlbXNTdWJqZWN0ID0gKCk6IFN1YmplY3Q8bnVtYmVyPiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fdG90YWxJdGVtc1N1YmplY3Q7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGNoYW5nZVBhdGggPSAocGF0aDogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnBhZ2VhYmxlQ3J1ZFNlcnZpY2Uuc2V0UGF0aChwYXRoKTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgY2hhbmdlQ3JpdGVyaWFQYXRoID0gKGNyaXRlcmlhUGF0aDogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnBhZ2VhYmxlQ3J1ZFNlcnZpY2Uuc2V0Q3JpdGVyaWFQYXRoKGNyaXRlcmlhUGF0aCk7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHJlc2V0QW5kRmV0Y2ggPSAoXHJcbiAgICBjcml0ZXJpYTogc3RyaW5nLFxyXG4gICAgc29ydFBhcmFtcz86IFNvcnRQYXJhbVtdLFxyXG4gICAgcGFnZVNpemU/OiBudW1iZXJcclxuICApOiB2b2lkID0+IHtcclxuICAgIGlmIChwYWdlU2l6ZSkgdGhpcy5fcGFnZVNpemUgPSBwYWdlU2l6ZTtcclxuXHJcbiAgICBpZiAoc29ydFBhcmFtcykgdGhpcy5fc29ydFBhcmFtcyA9IHNvcnRQYXJhbXM7XHJcblxyXG4gICAgdGhpcy5fY2FjaGVkRGF0YS5zcGxpY2UoMCwgdGhpcy5fY2FjaGVkRGF0YS5sZW5ndGgpO1xyXG4gICAgdGhpcy5fZmV0Y2hlZFBhZ2VzLmNsZWFyKCk7IC8vIHNrdXAgYnJvamV2YSBrb2ppIHByZWRzdGF2bGphanUgYnJvamV2ZSBkb2h2YWNlbmloIHN0cmFuaWNhXHJcbiAgICB0aGlzLl9kYXRhU3RyZWFtLm5leHQodGhpcy5fY2FjaGVkRGF0YSk7XHJcblxyXG4gICAgdGhpcy5faGFzRmV0Y2hlZERhdGFTdWJqZWN0Lm5leHQoZmFsc2UpO1xyXG5cclxuICAgIC8vIGNvbnNvbGUubG9nKGNyaXRlcmlhLCB0aGlzLl9wYWdlU2l6ZSk7XHJcbiAgICB0aGlzLl9mZXRjaFBhZ2UoMCwgY3JpdGVyaWEpO1xyXG4gIH07XHJcbn1cclxuIl19