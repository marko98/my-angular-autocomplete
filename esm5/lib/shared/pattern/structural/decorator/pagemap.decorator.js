import { __read, __values } from "tslib";
var PageMap = /** @class */ (function () {
    function PageMap() {
        var _this = this;
        this.map = new Map();
        /**
         * prva implementacija metode nikada ne prazni mapu iako se promeni kriterijum za
         * dobavljanje entiteta
         */
        // has = (key: PageKey): boolean => {
        //   // iteriramo kroz sve elemente
        //   for (const [existingKey, value] of this.map) {
        //     if (
        //       existingKey.pageNumber === key.pageNumber &&
        //       existingKey.pageSize === key.pageSize &&
        //       existingKey.sort.empty === key.sort.empty &&
        //       existingKey.sort.sorted === key.sort.sorted &&
        //       existingKey.sort.unsorted === key.sort.unsorted
        //     )
        //       return true;
        //   }
        //   return false;
        // };
        /**
         * druga implementacija metode prazni mapu ako dodje do promene kriterijuma za dobavljanje entiteta
         */
        this.has = function (key) {
            var e_1, _a;
            if (!_this._hasCriteriaChanged(key)) {
                try {
                    for (var _b = __values(_this.map), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var _d = __read(_c.value, 2), existingKey = _d[0], value = _d[1];
                        if (existingKey.pageNumber === key.pageNumber)
                            return true;
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            else {
                _this.map.clear();
                return false;
            }
        };
        this._hasCriteriaChanged = function (key) {
            var e_2, _a;
            try {
                for (var _b = __values(_this.map), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var _d = __read(_c.value, 2), existingKey = _d[0], value = _d[1];
                    if (existingKey.pageSize === key.pageSize &&
                        existingKey.sort.empty === key.sort.empty &&
                        existingKey.sort.sorted === key.sort.sorted &&
                        existingKey.sort.unsorted === key.sort.unsorted)
                        return false;
                    else
                        return true;
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_2) throw e_2.error; }
            }
        };
        this.getMap = function () {
            return _this.map;
        };
        this.clone = function () {
            // vracamo clone
            return new Map(_this.map);
        };
        this.clear = function () {
            _this.map.clear();
        };
    }
    return PageMap;
}());
export { PageMap };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZW1hcC5kZWNvcmF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hdXRvY29tcGxldGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3BhdHRlcm4vc3RydWN0dXJhbC9kZWNvcmF0b3IvcGFnZW1hcC5kZWNvcmF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUlBO0lBQUE7UUFBQSxpQkE2REM7UUE1RFMsUUFBRyxHQUE4QixJQUFJLEdBQUcsRUFBRSxDQUFDO1FBRW5EOzs7V0FHRztRQUNILHFDQUFxQztRQUNyQyxtQ0FBbUM7UUFDbkMsbURBQW1EO1FBQ25ELFdBQVc7UUFDWCxxREFBcUQ7UUFDckQsaURBQWlEO1FBQ2pELHFEQUFxRDtRQUNyRCx1REFBdUQ7UUFDdkQsd0RBQXdEO1FBQ3hELFFBQVE7UUFDUixxQkFBcUI7UUFDckIsTUFBTTtRQUNOLGtCQUFrQjtRQUNsQixLQUFLO1FBRUw7O1dBRUc7UUFDSCxRQUFHLEdBQUcsVUFBQyxHQUFZOztZQUNqQixJQUFJLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxFQUFFOztvQkFDbEMsS0FBbUMsSUFBQSxLQUFBLFNBQUEsS0FBSSxDQUFDLEdBQUcsQ0FBQSxnQkFBQSw0QkFBRTt3QkFBbEMsSUFBQSx3QkFBb0IsRUFBbkIsbUJBQVcsRUFBRSxhQUFLO3dCQUM1QixJQUFJLFdBQVcsQ0FBQyxVQUFVLEtBQUssR0FBRyxDQUFDLFVBQVU7NEJBQUUsT0FBTyxJQUFJLENBQUM7cUJBQzVEOzs7Ozs7Ozs7YUFDRjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixPQUFPLEtBQUssQ0FBQzthQUNkO1FBQ0gsQ0FBQyxDQUFDO1FBRU0sd0JBQW1CLEdBQUcsVUFBQyxHQUFZOzs7Z0JBQ3pDLEtBQW1DLElBQUEsS0FBQSxTQUFBLEtBQUksQ0FBQyxHQUFHLENBQUEsZ0JBQUEsNEJBQUU7b0JBQWxDLElBQUEsd0JBQW9CLEVBQW5CLG1CQUFXLEVBQUUsYUFBSztvQkFDNUIsSUFDRSxXQUFXLENBQUMsUUFBUSxLQUFLLEdBQUcsQ0FBQyxRQUFRO3dCQUNyQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUs7d0JBQ3pDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTTt3QkFDM0MsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRO3dCQUUvQyxPQUFPLEtBQUssQ0FBQzs7d0JBQ1YsT0FBTyxJQUFJLENBQUM7aUJBQ2xCOzs7Ozs7Ozs7UUFDSCxDQUFDLENBQUM7UUFFRixXQUFNLEdBQUc7WUFDUCxPQUFPLEtBQUksQ0FBQyxHQUFHLENBQUM7UUFDbEIsQ0FBQyxDQUFDO1FBRUYsVUFBSyxHQUFHO1lBQ04sZ0JBQWdCO1lBQ2hCLE9BQU8sSUFBSSxHQUFHLENBQUMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNCLENBQUMsQ0FBQztRQUVGLFVBQUssR0FBRztZQUNOLEtBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUFELGNBQUM7QUFBRCxDQUFDLEFBN0RELElBNkRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRW50aXRldCB9IGZyb20gJy4uLy4uLy4uL21vZGVsL2ludGVyZmFjZS9lbnRpdGV0LmludGVyZmFjZSc7XHJcbmltcG9ydCB7IFByb3RvdHlwZSB9IGZyb20gJy4uLy4uL2NyZWF0aW9uYWwvcHJvdG90eXBlL3Byb3RvdHlwZS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBQYWdlS2V5LCBQYWdlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZS9wYWdlYWJsZS1yZWFkLnNlcnZpY2UnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBhZ2VNYXA8VCBleHRlbmRzIEVudGl0ZXQ8VCwgSUQ+LCBJRD4gaW1wbGVtZW50cyBQcm90b3R5cGUge1xyXG4gIHByaXZhdGUgbWFwOiBNYXA8UGFnZUtleSwgUGFnZTxULCBJRD4+ID0gbmV3IE1hcCgpO1xyXG5cclxuICAvKipcclxuICAgKiBwcnZhIGltcGxlbWVudGFjaWphIG1ldG9kZSBuaWthZGEgbmUgcHJhem5pIG1hcHUgaWFrbyBzZSBwcm9tZW5pIGtyaXRlcmlqdW0gemFcclxuICAgKiBkb2JhdmxqYW5qZSBlbnRpdGV0YVxyXG4gICAqL1xyXG4gIC8vIGhhcyA9IChrZXk6IFBhZ2VLZXkpOiBib29sZWFuID0+IHtcclxuICAvLyAgIC8vIGl0ZXJpcmFtbyBrcm96IHN2ZSBlbGVtZW50ZVxyXG4gIC8vICAgZm9yIChjb25zdCBbZXhpc3RpbmdLZXksIHZhbHVlXSBvZiB0aGlzLm1hcCkge1xyXG4gIC8vICAgICBpZiAoXHJcbiAgLy8gICAgICAgZXhpc3RpbmdLZXkucGFnZU51bWJlciA9PT0ga2V5LnBhZ2VOdW1iZXIgJiZcclxuICAvLyAgICAgICBleGlzdGluZ0tleS5wYWdlU2l6ZSA9PT0ga2V5LnBhZ2VTaXplICYmXHJcbiAgLy8gICAgICAgZXhpc3RpbmdLZXkuc29ydC5lbXB0eSA9PT0ga2V5LnNvcnQuZW1wdHkgJiZcclxuICAvLyAgICAgICBleGlzdGluZ0tleS5zb3J0LnNvcnRlZCA9PT0ga2V5LnNvcnQuc29ydGVkICYmXHJcbiAgLy8gICAgICAgZXhpc3RpbmdLZXkuc29ydC51bnNvcnRlZCA9PT0ga2V5LnNvcnQudW5zb3J0ZWRcclxuICAvLyAgICAgKVxyXG4gIC8vICAgICAgIHJldHVybiB0cnVlO1xyXG4gIC8vICAgfVxyXG4gIC8vICAgcmV0dXJuIGZhbHNlO1xyXG4gIC8vIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIGRydWdhIGltcGxlbWVudGFjaWphIG1ldG9kZSBwcmF6bmkgbWFwdSBha28gZG9kamUgZG8gcHJvbWVuZSBrcml0ZXJpanVtYSB6YSBkb2JhdmxqYW5qZSBlbnRpdGV0YVxyXG4gICAqL1xyXG4gIGhhcyA9IChrZXk6IFBhZ2VLZXkpOiBib29sZWFuID0+IHtcclxuICAgIGlmICghdGhpcy5faGFzQ3JpdGVyaWFDaGFuZ2VkKGtleSkpIHtcclxuICAgICAgZm9yIChjb25zdCBbZXhpc3RpbmdLZXksIHZhbHVlXSBvZiB0aGlzLm1hcCkge1xyXG4gICAgICAgIGlmIChleGlzdGluZ0tleS5wYWdlTnVtYmVyID09PSBrZXkucGFnZU51bWJlcikgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubWFwLmNsZWFyKCk7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBwcml2YXRlIF9oYXNDcml0ZXJpYUNoYW5nZWQgPSAoa2V5OiBQYWdlS2V5KTogYm9vbGVhbiA9PiB7XHJcbiAgICBmb3IgKGNvbnN0IFtleGlzdGluZ0tleSwgdmFsdWVdIG9mIHRoaXMubWFwKSB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICBleGlzdGluZ0tleS5wYWdlU2l6ZSA9PT0ga2V5LnBhZ2VTaXplICYmXHJcbiAgICAgICAgZXhpc3RpbmdLZXkuc29ydC5lbXB0eSA9PT0ga2V5LnNvcnQuZW1wdHkgJiZcclxuICAgICAgICBleGlzdGluZ0tleS5zb3J0LnNvcnRlZCA9PT0ga2V5LnNvcnQuc29ydGVkICYmXHJcbiAgICAgICAgZXhpc3RpbmdLZXkuc29ydC51bnNvcnRlZCA9PT0ga2V5LnNvcnQudW5zb3J0ZWRcclxuICAgICAgKVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgZWxzZSByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBnZXRNYXAgPSAoKTogTWFwPFBhZ2VLZXksIFBhZ2U8VCwgSUQ+PiA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5tYXA7XHJcbiAgfTtcclxuXHJcbiAgY2xvbmUgPSAoKTogTWFwPFBhZ2VLZXksIFBhZ2U8VCwgSUQ+PiA9PiB7XHJcbiAgICAvLyB2cmFjYW1vIGNsb25lXHJcbiAgICByZXR1cm4gbmV3IE1hcCh0aGlzLm1hcCk7XHJcbiAgfTtcclxuXHJcbiAgY2xlYXIgPSAoKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLm1hcC5jbGVhcigpO1xyXG4gIH07XHJcbn1cclxuIl19