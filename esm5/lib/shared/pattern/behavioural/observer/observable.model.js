/*
    observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
    objektom tipa Observable na koju smo subscribe-ovani dodje do promena

    primer observer-a:

export class AppComponent implements OnInit, OnDestroy {

    articleObserver = (observable: ArticleRepository): void => {
        console.log(observable);
        this.articles = observable.getArticles();
    }

    // interfaces
    ngOnInit() {
        this.articleRepository.attach(this.articleObserver);

        this.articles = this.articleRepository.getArticles();
        console.log("AppComponent init");
    }

    ngOnDestroy() {
        if (this.articleRepository)
            this.articleRepository.dettach(this.articleObserver);
        console.log("AppComponent destroyed");
    }
}
*/
import { __values } from "tslib";
var Observable = /** @class */ (function () {
    function Observable() {
        var _this = this;
        this.observers = new Set();
        this.attach = function (observer) {
            if (!_this.observers.has(observer))
                _this.observers.add(observer);
        };
        this.dettach = function (observer) {
            if (_this.observers.has(observer))
                _this.observers.delete(observer);
        };
        this.notify = function () {
            var e_1, _a;
            try {
                for (var _b = __values(_this.observers), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var observer = _c.value;
                    // ne znam bas koliko ima smisla
                    if (observer) {
                        try {
                            observer.update();
                        }
                        catch (error) {
                            observer();
                        }
                    }
                    else
                        _this.dettach(observer);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        };
    }
    return Observable;
}());
export { Observable };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JzZXJ2YWJsZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvcGF0dGVybi9iZWhhdmlvdXJhbC9vYnNlcnZlci9vYnNlcnZhYmxlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUEyQkU7O0FBSUY7SUFBQTtRQUFBLGlCQXVCQztRQXRCVyxjQUFTLEdBQTZCLElBQUksR0FBRyxFQUFFLENBQUM7UUFFMUQsV0FBTSxHQUFHLFVBQUMsUUFBNkI7WUFDckMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQztnQkFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNsRSxDQUFDLENBQUM7UUFFRixZQUFPLEdBQUcsVUFBQyxRQUE2QjtZQUN0QyxJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQztnQkFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwRSxDQUFDLENBQUM7UUFFUSxXQUFNLEdBQUc7OztnQkFDakIsS0FBcUIsSUFBQSxLQUFBLFNBQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBaEMsSUFBSSxRQUFRLFdBQUE7b0JBQ2YsZ0NBQWdDO29CQUNoQyxJQUFJLFFBQVEsRUFBRTt3QkFDWixJQUFJOzRCQUNTLFFBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt5QkFDL0I7d0JBQUMsT0FBTyxLQUFLLEVBQUU7NEJBQ0gsUUFBUyxFQUFFLENBQUM7eUJBQ3hCO3FCQUNGOzt3QkFBTSxLQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUMvQjs7Ozs7Ozs7O1FBQ0gsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUFELGlCQUFDO0FBQUQsQ0FBQyxBQXZCRCxJQXVCQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiAgICBvYnNlcnZlcnMgbmlzdSBuaXN0YSBkcnVnbyBkbyBzYW1lIGZ1bmtjaWplIGtvamEgdHJlYmEgZGEgc2UgcG96b3ZlIGthZGEgbmFkXHJcbiAgICBvYmpla3RvbSB0aXBhIE9ic2VydmFibGUgbmEga29qdSBzbW8gc3Vic2NyaWJlLW92YW5pIGRvZGplIGRvIHByb21lbmFcclxuXHJcbiAgICBwcmltZXIgb2JzZXJ2ZXItYTpcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG4gICAgYXJ0aWNsZU9ic2VydmVyID0gKG9ic2VydmFibGU6IEFydGljbGVSZXBvc2l0b3J5KTogdm9pZCA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2cob2JzZXJ2YWJsZSk7XHJcbiAgICAgICAgdGhpcy5hcnRpY2xlcyA9IG9ic2VydmFibGUuZ2V0QXJ0aWNsZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBpbnRlcmZhY2VzXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmFydGljbGVSZXBvc2l0b3J5LmF0dGFjaCh0aGlzLmFydGljbGVPYnNlcnZlcik7XHJcblxyXG4gICAgICAgIHRoaXMuYXJ0aWNsZXMgPSB0aGlzLmFydGljbGVSZXBvc2l0b3J5LmdldEFydGljbGVzKCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJBcHBDb21wb25lbnQgaW5pdFwiKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICBpZiAodGhpcy5hcnRpY2xlUmVwb3NpdG9yeSlcclxuICAgICAgICAgICAgdGhpcy5hcnRpY2xlUmVwb3NpdG9yeS5kZXR0YWNoKHRoaXMuYXJ0aWNsZU9ic2VydmVyKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIkFwcENvbXBvbmVudCBkZXN0cm95ZWRcIik7XHJcbiAgICB9XHJcbn1cclxuKi9cclxuXHJcbmltcG9ydCB7IE9ic2VydmVyIH0gZnJvbSAnLi9vYnNlcnZlci5pbnRlcmZhY2UnO1xyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIE9ic2VydmFibGUge1xyXG4gIHByb3RlY3RlZCBvYnNlcnZlcnM6IFNldDxPYnNlcnZlciB8IEZ1bmN0aW9uPiA9IG5ldyBTZXQoKTtcclxuXHJcbiAgYXR0YWNoID0gKG9ic2VydmVyOiBPYnNlcnZlciB8IEZ1bmN0aW9uKTogdm9pZCA9PiB7XHJcbiAgICBpZiAoIXRoaXMub2JzZXJ2ZXJzLmhhcyhvYnNlcnZlcikpIHRoaXMub2JzZXJ2ZXJzLmFkZChvYnNlcnZlcik7XHJcbiAgfTtcclxuXHJcbiAgZGV0dGFjaCA9IChvYnNlcnZlcjogT2JzZXJ2ZXIgfCBGdW5jdGlvbik6IHZvaWQgPT4ge1xyXG4gICAgaWYgKHRoaXMub2JzZXJ2ZXJzLmhhcyhvYnNlcnZlcikpIHRoaXMub2JzZXJ2ZXJzLmRlbGV0ZShvYnNlcnZlcik7XHJcbiAgfTtcclxuXHJcbiAgcHJvdGVjdGVkIG5vdGlmeSA9ICgpOiB2b2lkID0+IHtcclxuICAgIGZvciAobGV0IG9ic2VydmVyIG9mIHRoaXMub2JzZXJ2ZXJzKSB7XHJcbiAgICAgIC8vIG5lIHpuYW0gYmFzIGtvbGlrbyBpbWEgc21pc2xhXHJcbiAgICAgIGlmIChvYnNlcnZlcikge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAoPE9ic2VydmVyPm9ic2VydmVyKS51cGRhdGUoKTtcclxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgKDxGdW5jdGlvbj5vYnNlcnZlcikoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB0aGlzLmRldHRhY2gob2JzZXJ2ZXIpO1xyXG4gICAgfVxyXG4gIH07XHJcbn1cclxuIl19