import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { HeightDirective } from './directive/height.directive';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { WidthDirective } from './directive/width.directive';
import { OpacityTransitionDirective } from './directive/opacity-transition.directive';
import * as i0 from "@angular/core";
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule.ɵmod = i0.ɵɵdefineNgModule({ type: SharedModule });
    SharedModule.ɵinj = i0.ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MaterialModule,
                FlexLayoutModule,
                ScrollingModule,
                DragDropModule,
            ],
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MaterialModule,
            FlexLayoutModule,
            ScrollingModule,
            DragDropModule] });
    return SharedModule;
}());
export { SharedModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(SharedModule, { declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective], imports: [CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        ScrollingModule,
        DragDropModule], exports: [HeightDirective,
        WidthDirective,
        OpacityTransitionDirective,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        ScrollingModule,
        DragDropModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(SharedModule, [{
        type: NgModule,
        args: [{
                declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                    ScrollingModule,
                    DragDropModule,
                ],
                exports: [
                    HeightDirective,
                    WidthDirective,
                    OpacityTransitionDirective,
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                    ScrollingModule,
                    DragDropModule,
                ],
                entryComponents: [],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2hhcmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRXhELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQzs7QUFFdEY7SUFBQTtLQTBCNEI7b0RBQWYsWUFBWTsyR0FBWixZQUFZLGtCQXhCZDtnQkFDUCxZQUFZO2dCQUNaLFdBQVc7Z0JBQ1gsbUJBQW1CO2dCQUNuQixjQUFjO2dCQUNkLGdCQUFnQjtnQkFDaEIsZUFBZTtnQkFDZixjQUFjO2FBQ2Y7WUFNQyxZQUFZO1lBQ1osV0FBVztZQUNYLG1CQUFtQjtZQUNuQixjQUFjO1lBQ2QsZ0JBQWdCO1lBQ2hCLGVBQWU7WUFDZixjQUFjO3VCQWxDbEI7Q0FzQzRCLEFBMUI1QixJQTBCNEI7U0FBZixZQUFZO3dGQUFaLFlBQVksbUJBekJSLGVBQWUsRUFBRSxjQUFjLEVBQUUsMEJBQTBCLGFBRXhFLFlBQVk7UUFDWixXQUFXO1FBQ1gsbUJBQW1CO1FBQ25CLGNBQWM7UUFDZCxnQkFBZ0I7UUFDaEIsZUFBZTtRQUNmLGNBQWMsYUFHZCxlQUFlO1FBQ2YsY0FBYztRQUNkLDBCQUEwQjtRQUUxQixZQUFZO1FBQ1osV0FBVztRQUNYLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsZ0JBQWdCO1FBQ2hCLGVBQWU7UUFDZixjQUFjO2tEQUlMLFlBQVk7Y0ExQnhCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxlQUFlLEVBQUUsY0FBYyxFQUFFLDBCQUEwQixDQUFDO2dCQUMzRSxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsY0FBYztvQkFDZCxnQkFBZ0I7b0JBQ2hCLGVBQWU7b0JBQ2YsY0FBYztpQkFDZjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsZUFBZTtvQkFDZixjQUFjO29CQUNkLDBCQUEwQjtvQkFFMUIsWUFBWTtvQkFDWixXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsY0FBYztvQkFDZCxnQkFBZ0I7b0JBQ2hCLGVBQWU7b0JBQ2YsY0FBYztpQkFDZjtnQkFDRCxlQUFlLEVBQUUsRUFBRTthQUNwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBGbGV4TGF5b3V0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZmxleC1sYXlvdXQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgSGVpZ2h0RGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmUvaGVpZ2h0LmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFNjcm9sbGluZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9zY3JvbGxpbmcnO1xyXG5pbXBvcnQgeyBEcmFnRHJvcE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xyXG5cclxuaW1wb3J0IHsgV2lkdGhEaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZS93aWR0aC5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBPcGFjaXR5VHJhbnNpdGlvbkRpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlL29wYWNpdHktdHJhbnNpdGlvbi5kaXJlY3RpdmUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtIZWlnaHREaXJlY3RpdmUsIFdpZHRoRGlyZWN0aXZlLCBPcGFjaXR5VHJhbnNpdGlvbkRpcmVjdGl2ZV0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgRm9ybXNNb2R1bGUsXHJcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBGbGV4TGF5b3V0TW9kdWxlLFxyXG4gICAgU2Nyb2xsaW5nTW9kdWxlLFxyXG4gICAgRHJhZ0Ryb3BNb2R1bGUsXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBIZWlnaHREaXJlY3RpdmUsXHJcbiAgICBXaWR0aERpcmVjdGl2ZSxcclxuICAgIE9wYWNpdHlUcmFuc2l0aW9uRGlyZWN0aXZlLFxyXG5cclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgRmxleExheW91dE1vZHVsZSxcclxuICAgIFNjcm9sbGluZ01vZHVsZSxcclxuICAgIERyYWdEcm9wTW9kdWxlLFxyXG4gIF0sXHJcbiAgZW50cnlDb21wb25lbnRzOiBbXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIFNoYXJlZE1vZHVsZSB7fVxyXG4iXX0=