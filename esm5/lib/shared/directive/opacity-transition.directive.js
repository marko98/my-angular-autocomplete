import { Directive, Input } from '@angular/core';
import * as gsap from 'gsap';
import * as i0 from "@angular/core";
var OpacityTransitionDirective = /** @class */ (function () {
    function OpacityTransitionDirective(_el) {
        this._el = _el;
        this.appOpacityDuration = 1;
        this._timeline = new gsap.TimelineMax();
    }
    OpacityTransitionDirective.prototype.ngAfterViewInit = function () {
        this._timeline.fromTo(this._el.nativeElement, this.appOpacityDuration, { opacity: 0 }, {
            opacity: 1,
            onComplete: function () {
                // console.log('animacija gotova :)');
            },
        });
    };
    OpacityTransitionDirective.ɵfac = function OpacityTransitionDirective_Factory(t) { return new (t || OpacityTransitionDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
    OpacityTransitionDirective.ɵdir = i0.ɵɵdefineDirective({ type: OpacityTransitionDirective, selectors: [["", "appOpacityTransition", ""]], inputs: { appOpacityDuration: "appOpacityDuration" } });
    return OpacityTransitionDirective;
}());
export { OpacityTransitionDirective };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(OpacityTransitionDirective, [{
        type: Directive,
        args: [{
                selector: '[appOpacityTransition]',
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { appOpacityDuration: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3BhY2l0eS10cmFuc2l0aW9uLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL29wYWNpdHktdHJhbnNpdGlvbi5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBNkIsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sS0FBSyxJQUFJLE1BQU0sTUFBTSxDQUFDOztBQUU3QjtJQU9FLG9DQUFvQixHQUFlO1FBQWYsUUFBRyxHQUFILEdBQUcsQ0FBWTtRQUgxQix1QkFBa0IsR0FBVyxDQUFDLENBQUM7UUFDaEMsY0FBUyxHQUFHLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBRUwsQ0FBQztJQUV2QyxvREFBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUN0QixJQUFJLENBQUMsa0JBQWtCLEVBQ3ZCLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUNkO1lBQ0UsT0FBTyxFQUFFLENBQUM7WUFDVixVQUFVLEVBQUU7Z0JBQ1Ysc0NBQXNDO1lBQ3hDLENBQUM7U0FDRixDQUNGLENBQUM7SUFDSixDQUFDO3dHQWxCVSwwQkFBMEI7bUVBQTFCLDBCQUEwQjtxQ0FOdkM7Q0F5QkMsQUF0QkQsSUFzQkM7U0FuQlksMEJBQTBCO2tEQUExQiwwQkFBMEI7Y0FIdEMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSx3QkFBd0I7YUFDbkM7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEFmdGVyVmlld0luaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgKiBhcyBnc2FwIGZyb20gJ2dzYXAnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbYXBwT3BhY2l0eVRyYW5zaXRpb25dJyxcbn0pXG5leHBvcnQgY2xhc3MgT3BhY2l0eVRyYW5zaXRpb25EaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcbiAgQElucHV0KCkgYXBwT3BhY2l0eUR1cmF0aW9uOiBudW1iZXIgPSAxO1xuICBwcml2YXRlIF90aW1lbGluZSA9IG5ldyBnc2FwLlRpbWVsaW5lTWF4KCk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfZWw6IEVsZW1lbnRSZWYpIHt9XG5cbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3RpbWVsaW5lLmZyb21UbyhcbiAgICAgIHRoaXMuX2VsLm5hdGl2ZUVsZW1lbnQsXG4gICAgICB0aGlzLmFwcE9wYWNpdHlEdXJhdGlvbixcbiAgICAgIHsgb3BhY2l0eTogMCB9LFxuICAgICAge1xuICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICBvbkNvbXBsZXRlOiAoKSA9PiB7XG4gICAgICAgICAgLy8gY29uc29sZS5sb2coJ2FuaW1hY2lqYSBnb3RvdmEgOiknKTtcbiAgICAgICAgfSxcbiAgICAgIH1cbiAgICApO1xuICB9XG59XG4iXX0=