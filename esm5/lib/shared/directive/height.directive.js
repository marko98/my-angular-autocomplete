import { Directive, Input } from '@angular/core';
import * as i0 from "@angular/core";
var HeightDirective = /** @class */ (function () {
    function HeightDirective(elRef) {
        this.elRef = elRef;
    }
    HeightDirective.prototype.ngAfterViewInit = function () {
        this.elRef.nativeElement.style.height = this.height + 'px';
    };
    HeightDirective.ɵfac = function HeightDirective_Factory(t) { return new (t || HeightDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
    HeightDirective.ɵdir = i0.ɵɵdefineDirective({ type: HeightDirective, selectors: [["", "appHeight", ""]], inputs: { height: "height" } });
    return HeightDirective;
}());
export { HeightDirective };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(HeightDirective, [{
        type: Directive,
        args: [{
                selector: '[appHeight]',
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { height: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVpZ2h0LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL2hlaWdodC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYyxLQUFLLEVBQWlCLE1BQU0sZUFBZSxDQUFDOztBQUU1RTtJQVNFLHlCQUFvQixLQUFpQjtRQUFqQixVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQUcsQ0FBQztJQUV6Qyx5Q0FBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUM3RCxDQUFDO2tGQVZVLGVBQWU7d0RBQWYsZUFBZTswQkFMNUI7Q0FnQkMsQUFkRCxJQWNDO1NBWFksZUFBZTtrREFBZixlQUFlO2NBSDNCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsYUFBYTthQUN4Qjs7a0JBS0UsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSW5wdXQsIEFmdGVyVmlld0luaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2FwcEhlaWdodF0nLFxufSlcbmV4cG9ydCBjbGFzcyBIZWlnaHREaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcbiAgLyoqXG4gICAqIEBoZWlnaHQgLSB1IHB4XG4gICAqL1xuICBASW5wdXQoKSBoZWlnaHQ6IG51bWJlcjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmKSB7fVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQuc3R5bGUuaGVpZ2h0ID0gdGhpcy5oZWlnaHQgKyAncHgnO1xuICB9XG59XG4iXX0=