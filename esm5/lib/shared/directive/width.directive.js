import { Directive, Input, } from '@angular/core';
import * as i0 from "@angular/core";
var WidthDirective = /** @class */ (function () {
    function WidthDirective(elRef) {
        this.elRef = elRef;
    }
    WidthDirective.prototype.ngAfterViewInit = function () {
        this.elRef.nativeElement.style.width = this.width + 'vw';
    };
    WidthDirective.ɵfac = function WidthDirective_Factory(t) { return new (t || WidthDirective)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
    WidthDirective.ɵdir = i0.ɵɵdefineDirective({ type: WidthDirective, selectors: [["", "appWidth", ""]], inputs: { width: "width" } });
    return WidthDirective;
}());
export { WidthDirective };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(WidthDirective, [{
        type: Directive,
        args: [{
                selector: '[appWidth]',
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { width: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkdGguZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYXV0b2NvbXBsZXRlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmUvd2lkdGguZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsS0FBSyxHQUlOLE1BQU0sZUFBZSxDQUFDOztBQUV2QjtJQVNFLHdCQUFvQixLQUFpQjtRQUFqQixVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQUcsQ0FBQztJQUV6Qyx3Q0FBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUMzRCxDQUFDO2dGQVZVLGNBQWM7dURBQWQsY0FBYzt5QkFYM0I7Q0FzQkMsQUFkRCxJQWNDO1NBWFksY0FBYztrREFBZCxjQUFjO2NBSDFCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsWUFBWTthQUN2Qjs7a0JBS0UsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIERpcmVjdGl2ZSxcbiAgSW5wdXQsXG4gIE9uSW5pdCxcbiAgQWZ0ZXJWaWV3SW5pdCxcbiAgRWxlbWVudFJlZixcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1thcHBXaWR0aF0nLFxufSlcbmV4cG9ydCBjbGFzcyBXaWR0aERpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuICAvKipcbiAgICogQHdpZHRoIC0gdSB2d1xuICAgKi9cbiAgQElucHV0KCkgd2lkdGg6IG51bWJlcjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmKSB7fVxuXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQuc3R5bGUud2lkdGggPSB0aGlzLndpZHRoICsgJ3Z3JztcbiAgfVxufVxuIl19