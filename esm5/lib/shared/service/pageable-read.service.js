import { __extends } from "tslib";
import { HttpParams } from '@angular/common/http';
// decorator
import { PageMap } from '../pattern/structural/decorator/pagemap.decorator';
// model
import { Observable as CustomObservebale } from '../pattern/behavioural/observer/observable.model';
var PageableReadService = /** @class */ (function (_super) {
    __extends(PageableReadService, _super);
    function PageableReadService(httpClient, schema, host, port, defaultPath, defaultCriteriaPath, factory, pageSize) {
        if (pageSize === void 0) { pageSize = 20; }
        var _this = _super.call(this) || this;
        _this.httpClient = httpClient;
        _this.factory = factory;
        _this.pageSize = pageSize;
        _this.pages = new PageMap();
        // ----------------- interface PageableCrudService -----------------
        //   read pages
        _this.findPageWithPageNumber = function (page) {
            if (page === void 0) { page = 0; }
            var url = _this.url + _this.path;
            _this.httpClient
                .get(url, {
                params: new HttpParams()
                    .set('page', page.toString())
                    .set('size', _this.pageSize.toString()),
            })
                .subscribe(function (page) {
                if (_this._addPage(page))
                    _this.notify();
            }, function (err) {
                _this.showError(err);
            });
        };
        //   read pages
        _this.findPageWithPageNumberAndSize = function (page, pageSize, criteriaValue) {
            if (page === void 0) { page = 0; }
            if (pageSize === void 0) { pageSize = 20; }
            var url = _this.url + _this.path;
            if (criteriaValue &&
                criteriaValue !== '' &&
                criteriaValue.replace(/\s/g, ''))
                url = _this.url + _this.criteriaPath + criteriaValue;
            var observable = _this.httpClient.get(url, {
                params: new HttpParams()
                    .set('page', page.toString())
                    .set('size', pageSize.toString()),
            });
            observable.subscribe(function (page) {
                if (_this._addPage(page))
                    _this.notify();
            }, function (err) {
                _this.showError(err);
            });
            return observable;
        };
        // read pages
        _this.findPageWithPageNumberAndSizeAndSort = function (page, pageSize, sortParams, criteriaValue) {
            if (page === void 0) { page = 0; }
            if (pageSize === void 0) { pageSize = 20; }
            if (sortParams === void 0) { sortParams = []; }
            var url = _this.url + _this.path;
            if (criteriaValue &&
                criteriaValue !== '' &&
                criteriaValue.replace(/\s/g, ''))
                url = _this.url + _this.criteriaPath + criteriaValue;
            var httpParams = new HttpParams()
                .set('page', page.toString())
                .set('size', pageSize.toString());
            sortParams.forEach(function (sortParam) {
                httpParams = httpParams.append('sort', sortParam.field + ',' + sortParam.order);
            });
            var observable = _this.httpClient.get(url, {
                params: httpParams,
            });
            observable.subscribe(function (page) {
                if (_this._addPage(page))
                    _this.notify();
            }, function (err) {
                _this.showError(err);
            });
            return observable;
        };
        _this.showError = function (err) {
            console.log(err);
        };
        _this.getPages = function () {
            // vracamo clone
            return _this.pages.clone();
        };
        _this._addPage = function (page) {
            if (!_this.pages.has({
                pageNumber: page.pageable.pageNumber,
                pageSize: page.pageable.pageSize,
                sort: page.pageable.sort,
            })) {
                _this.pages.getMap().set({
                    pageNumber: page.pageable.pageNumber,
                    pageSize: page.pageable.pageSize,
                    sort: page.pageable.sort,
                }, page);
                return true;
            }
            return false;
        };
        _this.getPageSize = function () {
            return _this.pageSize;
        };
        _this.setPageSize = function (pageSize) {
            _this.pageSize = pageSize;
        };
        _this.setPath = function (path) {
            _this.path = path;
        };
        _this.getPath = function () {
            return _this.path;
        };
        _this.setCriteriaPath = function (criteriaPath) {
            _this.criteriaPath = criteriaPath;
        };
        _this.getCriteriaPath = function () {
            return _this.criteriaPath;
        };
        _this.resetPathAndCriteriaPathToDefault = function () {
            _this.path = _this._defaultPath;
            _this.criteriaPath = _this._defaultCriteriaPath;
        };
        _this.buildEntitet = function (e) {
            return _this.factory.build(e);
        };
        _this.url = schema + '://' + host + ':' + port.toString() + '/';
        _this._defaultPath = defaultPath;
        _this.path = defaultPath;
        _this._defaultCriteriaPath = defaultCriteriaPath;
        _this.criteriaPath = defaultCriteriaPath;
        return _this;
    }
    return PageableReadService;
}(CustomObservebale));
export { PageableReadService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZWFibGUtcmVhZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYXV0b2NvbXBsZXRlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlL3BhZ2VhYmxlLXJlYWQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFjLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBVzNFLFlBQVk7QUFDWixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFFNUUsUUFBUTtBQUNSLE9BQU8sRUFBRSxVQUFVLElBQUksaUJBQWlCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQTBDbkc7SUFDVSx1Q0FBaUI7SUFTekIsNkJBQ1ksVUFBc0IsRUFDaEMsTUFBYyxFQUNkLElBQVksRUFDWixJQUFxQixFQUNyQixXQUFtQixFQUNuQixtQkFBMkIsRUFDakIsT0FBOEIsRUFDOUIsUUFBcUI7UUFBckIseUJBQUEsRUFBQSxhQUFxQjtRQVJqQyxZQVVFLGlCQUFPLFNBUVI7UUFqQlcsZ0JBQVUsR0FBVixVQUFVLENBQVk7UUFNdEIsYUFBTyxHQUFQLE9BQU8sQ0FBdUI7UUFDOUIsY0FBUSxHQUFSLFFBQVEsQ0FBYTtRQVZ2QixXQUFLLEdBQW1CLElBQUksT0FBTyxFQUFFLENBQUM7UUFzQmhELG9FQUFvRTtRQUVwRSxlQUFlO1FBQ1IsNEJBQXNCLEdBQUcsVUFBQyxJQUFnQjtZQUFoQixxQkFBQSxFQUFBLFFBQWdCO1lBQy9DLElBQUksR0FBRyxHQUFHLEtBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQztZQUUvQixLQUFJLENBQUMsVUFBVTtpQkFDWixHQUFHLENBQWMsR0FBRyxFQUFFO2dCQUNyQixNQUFNLEVBQUUsSUFBSSxVQUFVLEVBQUU7cUJBQ3JCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUM1QixHQUFHLENBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDekMsQ0FBQztpQkFDRCxTQUFTLENBQ1IsVUFBQyxJQUFJO2dCQUNILElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQUUsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3pDLENBQUMsRUFDRCxVQUFDLEdBQUc7Z0JBQ0YsS0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN0QixDQUFDLENBQ0YsQ0FBQztRQUNOLENBQUMsQ0FBQztRQUVGLGVBQWU7UUFDUixtQ0FBNkIsR0FBRyxVQUNyQyxJQUFnQixFQUNoQixRQUFxQixFQUNyQixhQUFzQjtZQUZ0QixxQkFBQSxFQUFBLFFBQWdCO1lBQ2hCLHlCQUFBLEVBQUEsYUFBcUI7WUFHckIsSUFBSSxHQUFHLEdBQUcsS0FBSSxDQUFDLEdBQUcsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDO1lBQy9CLElBQ0UsYUFBYTtnQkFDYixhQUFhLEtBQUssRUFBRTtnQkFDcEIsYUFBYSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDO2dCQUVoQyxHQUFHLEdBQUcsS0FBSSxDQUFDLEdBQUcsR0FBRyxLQUFJLENBQUMsWUFBWSxHQUFHLGFBQWEsQ0FBQztZQUVyRCxJQUFJLFVBQVUsR0FBNEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQzNELEdBQUcsRUFDSDtnQkFDRSxNQUFNLEVBQUUsSUFBSSxVQUFVLEVBQUU7cUJBQ3JCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUM1QixHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNwQyxDQUNGLENBQUM7WUFFRixVQUFVLENBQUMsU0FBUyxDQUNsQixVQUFDLElBQUk7Z0JBQ0gsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFBRSxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDekMsQ0FBQyxFQUNELFVBQUMsR0FBRztnQkFDRixLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLENBQUMsQ0FDRixDQUFDO1lBRUYsT0FBTyxVQUFVLENBQUM7UUFDcEIsQ0FBQyxDQUFDO1FBRUYsYUFBYTtRQUNOLDBDQUFvQyxHQUFHLFVBQzVDLElBQWdCLEVBQ2hCLFFBQXFCLEVBQ3JCLFVBQTRCLEVBQzVCLGFBQXNCO1lBSHRCLHFCQUFBLEVBQUEsUUFBZ0I7WUFDaEIseUJBQUEsRUFBQSxhQUFxQjtZQUNyQiwyQkFBQSxFQUFBLGVBQTRCO1lBRzVCLElBQUksR0FBRyxHQUFHLEtBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQztZQUMvQixJQUNFLGFBQWE7Z0JBQ2IsYUFBYSxLQUFLLEVBQUU7Z0JBQ3BCLGFBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQztnQkFFaEMsR0FBRyxHQUFHLEtBQUksQ0FBQyxHQUFHLEdBQUcsS0FBSSxDQUFDLFlBQVksR0FBRyxhQUFhLENBQUM7WUFFckQsSUFBSSxVQUFVLEdBQWUsSUFBSSxVQUFVLEVBQUU7aUJBQzFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUM1QixHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ3BDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxTQUFvQjtnQkFDdEMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQzVCLE1BQU0sRUFDTixTQUFTLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUN4QyxDQUFDO1lBQ0osQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLFVBQVUsR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBYyxHQUFHLEVBQUU7Z0JBQ3JELE1BQU0sRUFBRSxVQUFVO2FBQ25CLENBQUMsQ0FBQztZQUVILFVBQVUsQ0FBQyxTQUFTLENBQ2xCLFVBQUMsSUFBSTtnQkFDSCxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUFFLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUN6QyxDQUFDLEVBQ0QsVUFBQyxHQUFHO2dCQUNGLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdEIsQ0FBQyxDQUNGLENBQUM7WUFFRixPQUFPLFVBQVUsQ0FBQztRQUNwQixDQUFDLENBQUM7UUFFUSxlQUFTLEdBQUcsVUFBQyxHQUFRO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsQ0FBQyxDQUFDO1FBRUssY0FBUSxHQUFHO1lBQ2hCLGdCQUFnQjtZQUNoQixPQUFPLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUIsQ0FBQyxDQUFDO1FBRU0sY0FBUSxHQUFHLFVBQUMsSUFBaUI7WUFDbkMsSUFDRSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO2dCQUNkLFVBQVUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVU7Z0JBQ3BDLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVE7Z0JBQ2hDLElBQUksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUk7YUFDekIsQ0FBQyxFQUNGO2dCQUNBLEtBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUNyQjtvQkFDRSxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVO29CQUNwQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRO29CQUNoQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJO2lCQUN6QixFQUNELElBQUksQ0FDTCxDQUFDO2dCQUNGLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsQ0FBQztRQUVLLGlCQUFXLEdBQUc7WUFDbkIsT0FBTyxLQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3ZCLENBQUMsQ0FBQztRQUVLLGlCQUFXLEdBQUcsVUFBQyxRQUFnQjtZQUNwQyxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUMzQixDQUFDLENBQUM7UUFFSyxhQUFPLEdBQUcsVUFBQyxJQUFZO1lBQzVCLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUMsQ0FBQztRQUVLLGFBQU8sR0FBRztZQUNmLE9BQU8sS0FBSSxDQUFDLElBQUksQ0FBQztRQUNuQixDQUFDLENBQUM7UUFFSyxxQkFBZSxHQUFHLFVBQUMsWUFBb0I7WUFDNUMsS0FBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7UUFDbkMsQ0FBQyxDQUFDO1FBRUsscUJBQWUsR0FBRztZQUN2QixPQUFPLEtBQUksQ0FBQyxZQUFZLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBRUssdUNBQWlDLEdBQUc7WUFDekMsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDO1lBQzlCLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLG9CQUFvQixDQUFDO1FBQ2hELENBQUMsQ0FBQztRQUVLLGtCQUFZLEdBQUcsVUFBQyxDQUEwQjtZQUMvQyxPQUFPLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQztRQXhLQSxLQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sR0FBRyxLQUFLLEdBQUcsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRyxDQUFDO1FBRS9ELEtBQUksQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDO1FBQ2hDLEtBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDO1FBRXhCLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxtQkFBbUIsQ0FBQztRQUNoRCxLQUFJLENBQUMsWUFBWSxHQUFHLG1CQUFtQixDQUFDOztJQUMxQyxDQUFDO0lBa0tILDBCQUFDO0FBQUQsQ0FBQyxBQTlMRCxDQUNVLGlCQUFpQixHQTZMMUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zLCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuLy8gaW50ZXJmYWNlXHJcbmltcG9ydCB7IFBhZ2VhYmxlUmVhZFNlcnZpY2VJbnRlcmZhY2UgfSBmcm9tICcuL2ludGVyZmFjZS9wYWdlYWJsZS1yZWFkLXNlcnZpY2UuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgRW50aXRldEZhY3RvcnkgfSBmcm9tICcuLi9wYXR0ZXJuL2NyZWF0aW9uYWwvZmFjdG9yeS9lbnRpdGV0LWZhY3RvcnkuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgRW50aXRldCB9IGZyb20gJy4uL21vZGVsL2ludGVyZmFjZS9lbnRpdGV0LmludGVyZmFjZSc7XHJcblxyXG4vLyBtb2RlbCBpbnRlcmZhY2VcclxuaW1wb3J0IHsgRU5USVRFVF9NT0RFTF9JTlRFUkZBQ0UgfSBmcm9tICcuLi9tb2RlbC9pbnRlcmZhY2UvZW50aXRldC5tb2RlbC1pbnRlcmZhY2UnO1xyXG5cclxuLy8gZGVjb3JhdG9yXHJcbmltcG9ydCB7IFBhZ2VNYXAgfSBmcm9tICcuLi9wYXR0ZXJuL3N0cnVjdHVyYWwvZGVjb3JhdG9yL3BhZ2VtYXAuZGVjb3JhdG9yJztcclxuXHJcbi8vIG1vZGVsXHJcbmltcG9ydCB7IE9ic2VydmFibGUgYXMgQ3VzdG9tT2JzZXJ2ZWJhbGUgfSBmcm9tICcuLi9wYXR0ZXJuL2JlaGF2aW91cmFsL29ic2VydmVyL29ic2VydmFibGUubW9kZWwnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFBhZ2U8VCBleHRlbmRzIEVudGl0ZXQ8VCwgSUQ+LCBJRD4ge1xyXG4gIGNvbnRlbnQ6IEVOVElURVRfTU9ERUxfSU5URVJGQUNFW107XHJcbiAgZW1wdHk6IGJvb2xlYW47XHJcbiAgZmlyc3Q6IGJvb2xlYW47XHJcbiAgbGFzdDogYm9vbGVhbjtcclxuICBudW1iZXI6IG51bWJlcjtcclxuICBudW1iZXJPZkVsZW1lbnRzOiBudW1iZXI7XHJcbiAgcGFnZWFibGU6IFBhZ2VhYmxlO1xyXG4gIHNpemU6IG51bWJlcjtcclxuICBzb3J0OiBTb3J0O1xyXG4gIHRvdGFsRWxlbWVudHM6IG51bWJlcjtcclxuICB0b3RhbFBhZ2VzOiBudW1iZXI7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBQYWdlYWJsZSB7XHJcbiAgb2Zmc2V0OiBudW1iZXI7XHJcbiAgcGFnZU51bWJlcjogbnVtYmVyO1xyXG4gIHBhZ2VTaXplOiBudW1iZXI7XHJcbiAgcGFnZWQ6IGJvb2xlYW47XHJcbiAgc29ydDogU29ydDtcclxuICB1bnBhZ2VkOiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgU29ydCB7XHJcbiAgZW1wdHk6IGJvb2xlYW47XHJcbiAgc29ydGVkOiBib29sZWFuO1xyXG4gIHVuc29ydGVkOiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgU29ydFBhcmFtIHtcclxuICBmaWVsZDogc3RyaW5nO1xyXG4gIG9yZGVyOiAnYXNjJyB8ICdkZXNjJztcclxufVxyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFBhZ2VLZXkge1xyXG4gIHBhZ2VOdW1iZXI6IG51bWJlcjtcclxuICBwYWdlU2l6ZTogbnVtYmVyO1xyXG4gIHNvcnQ6IFNvcnQ7XHJcbn1cclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBQYWdlYWJsZVJlYWRTZXJ2aWNlPFQgZXh0ZW5kcyBFbnRpdGV0PFQsIElEPiwgSUQ+XHJcbiAgZXh0ZW5kcyBDdXN0b21PYnNlcnZlYmFsZVxyXG4gIGltcGxlbWVudHMgUGFnZWFibGVSZWFkU2VydmljZUludGVyZmFjZTxULCBJRD4ge1xyXG4gIHByb3RlY3RlZCB1cmw6IHN0cmluZztcclxuICBwcm90ZWN0ZWQgcGF0aDogc3RyaW5nO1xyXG4gIHByaXZhdGUgX2RlZmF1bHRQYXRoOiBzdHJpbmc7XHJcbiAgcHJvdGVjdGVkIGNyaXRlcmlhUGF0aDogc3RyaW5nO1xyXG4gIHByaXZhdGUgX2RlZmF1bHRDcml0ZXJpYVBhdGg6IHN0cmluZztcclxuICBwcm90ZWN0ZWQgcGFnZXM6IFBhZ2VNYXA8VCwgSUQ+ID0gbmV3IFBhZ2VNYXAoKTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcm90ZWN0ZWQgaHR0cENsaWVudDogSHR0cENsaWVudCxcclxuICAgIHNjaGVtYTogc3RyaW5nLFxyXG4gICAgaG9zdDogc3RyaW5nLFxyXG4gICAgcG9ydDogc3RyaW5nIHwgbnVtYmVyLFxyXG4gICAgZGVmYXVsdFBhdGg6IHN0cmluZyxcclxuICAgIGRlZmF1bHRDcml0ZXJpYVBhdGg6IHN0cmluZyxcclxuICAgIHByb3RlY3RlZCBmYWN0b3J5OiBFbnRpdGV0RmFjdG9yeTxULCBJRD4sXHJcbiAgICBwcm90ZWN0ZWQgcGFnZVNpemU6IG51bWJlciA9IDIwXHJcbiAgKSB7XHJcbiAgICBzdXBlcigpO1xyXG4gICAgdGhpcy51cmwgPSBzY2hlbWEgKyAnOi8vJyArIGhvc3QgKyAnOicgKyBwb3J0LnRvU3RyaW5nKCkgKyAnLyc7XHJcblxyXG4gICAgdGhpcy5fZGVmYXVsdFBhdGggPSBkZWZhdWx0UGF0aDtcclxuICAgIHRoaXMucGF0aCA9IGRlZmF1bHRQYXRoO1xyXG5cclxuICAgIHRoaXMuX2RlZmF1bHRDcml0ZXJpYVBhdGggPSBkZWZhdWx0Q3JpdGVyaWFQYXRoO1xyXG4gICAgdGhpcy5jcml0ZXJpYVBhdGggPSBkZWZhdWx0Q3JpdGVyaWFQYXRoO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0gaW50ZXJmYWNlIFBhZ2VhYmxlQ3J1ZFNlcnZpY2UgLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLy8gICByZWFkIHBhZ2VzXHJcbiAgcHVibGljIGZpbmRQYWdlV2l0aFBhZ2VOdW1iZXIgPSAocGFnZTogbnVtYmVyID0gMCk6IHZvaWQgPT4ge1xyXG4gICAgbGV0IHVybCA9IHRoaXMudXJsICsgdGhpcy5wYXRoO1xyXG5cclxuICAgIHRoaXMuaHR0cENsaWVudFxyXG4gICAgICAuZ2V0PFBhZ2U8VCwgSUQ+Pih1cmwsIHtcclxuICAgICAgICBwYXJhbXM6IG5ldyBIdHRwUGFyYW1zKClcclxuICAgICAgICAgIC5zZXQoJ3BhZ2UnLCBwYWdlLnRvU3RyaW5nKCkpXHJcbiAgICAgICAgICAuc2V0KCdzaXplJywgdGhpcy5wYWdlU2l6ZS50b1N0cmluZygpKSxcclxuICAgICAgfSlcclxuICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAocGFnZSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMuX2FkZFBhZ2UocGFnZSkpIHRoaXMubm90aWZ5KCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnNob3dFcnJvcihlcnIpO1xyXG4gICAgICAgIH1cclxuICAgICAgKTtcclxuICB9O1xyXG5cclxuICAvLyAgIHJlYWQgcGFnZXNcclxuICBwdWJsaWMgZmluZFBhZ2VXaXRoUGFnZU51bWJlckFuZFNpemUgPSAoXHJcbiAgICBwYWdlOiBudW1iZXIgPSAwLFxyXG4gICAgcGFnZVNpemU6IG51bWJlciA9IDIwLFxyXG4gICAgY3JpdGVyaWFWYWx1ZT86IHN0cmluZ1xyXG4gICk6IE9ic2VydmFibGU8UGFnZTxULCBJRD4+ID0+IHtcclxuICAgIGxldCB1cmwgPSB0aGlzLnVybCArIHRoaXMucGF0aDtcclxuICAgIGlmIChcclxuICAgICAgY3JpdGVyaWFWYWx1ZSAmJlxyXG4gICAgICBjcml0ZXJpYVZhbHVlICE9PSAnJyAmJlxyXG4gICAgICBjcml0ZXJpYVZhbHVlLnJlcGxhY2UoL1xccy9nLCAnJylcclxuICAgIClcclxuICAgICAgdXJsID0gdGhpcy51cmwgKyB0aGlzLmNyaXRlcmlhUGF0aCArIGNyaXRlcmlhVmFsdWU7XHJcblxyXG4gICAgbGV0IG9ic2VydmFibGU6IE9ic2VydmFibGU8UGFnZTxULCBJRD4+ID0gdGhpcy5odHRwQ2xpZW50LmdldDxQYWdlPFQsIElEPj4oXHJcbiAgICAgIHVybCxcclxuICAgICAge1xyXG4gICAgICAgIHBhcmFtczogbmV3IEh0dHBQYXJhbXMoKVxyXG4gICAgICAgICAgLnNldCgncGFnZScsIHBhZ2UudG9TdHJpbmcoKSlcclxuICAgICAgICAgIC5zZXQoJ3NpemUnLCBwYWdlU2l6ZS50b1N0cmluZygpKSxcclxuICAgICAgfVxyXG4gICAgKTtcclxuXHJcbiAgICBvYnNlcnZhYmxlLnN1YnNjcmliZShcclxuICAgICAgKHBhZ2UpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5fYWRkUGFnZShwYWdlKSkgdGhpcy5ub3RpZnkoKTtcclxuICAgICAgfSxcclxuICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgIHRoaXMuc2hvd0Vycm9yKGVycik7XHJcbiAgICAgIH1cclxuICAgICk7XHJcblxyXG4gICAgcmV0dXJuIG9ic2VydmFibGU7XHJcbiAgfTtcclxuXHJcbiAgLy8gcmVhZCBwYWdlc1xyXG4gIHB1YmxpYyBmaW5kUGFnZVdpdGhQYWdlTnVtYmVyQW5kU2l6ZUFuZFNvcnQgPSAoXHJcbiAgICBwYWdlOiBudW1iZXIgPSAwLFxyXG4gICAgcGFnZVNpemU6IG51bWJlciA9IDIwLFxyXG4gICAgc29ydFBhcmFtczogU29ydFBhcmFtW10gPSBbXSxcclxuICAgIGNyaXRlcmlhVmFsdWU/OiBzdHJpbmdcclxuICApOiBPYnNlcnZhYmxlPFBhZ2U8VCwgSUQ+PiA9PiB7XHJcbiAgICBsZXQgdXJsID0gdGhpcy51cmwgKyB0aGlzLnBhdGg7XHJcbiAgICBpZiAoXHJcbiAgICAgIGNyaXRlcmlhVmFsdWUgJiZcclxuICAgICAgY3JpdGVyaWFWYWx1ZSAhPT0gJycgJiZcclxuICAgICAgY3JpdGVyaWFWYWx1ZS5yZXBsYWNlKC9cXHMvZywgJycpXHJcbiAgICApXHJcbiAgICAgIHVybCA9IHRoaXMudXJsICsgdGhpcy5jcml0ZXJpYVBhdGggKyBjcml0ZXJpYVZhbHVlO1xyXG5cclxuICAgIGxldCBodHRwUGFyYW1zOiBIdHRwUGFyYW1zID0gbmV3IEh0dHBQYXJhbXMoKVxyXG4gICAgICAuc2V0KCdwYWdlJywgcGFnZS50b1N0cmluZygpKVxyXG4gICAgICAuc2V0KCdzaXplJywgcGFnZVNpemUudG9TdHJpbmcoKSk7XHJcbiAgICBzb3J0UGFyYW1zLmZvckVhY2goKHNvcnRQYXJhbTogU29ydFBhcmFtKSA9PiB7XHJcbiAgICAgIGh0dHBQYXJhbXMgPSBodHRwUGFyYW1zLmFwcGVuZChcclxuICAgICAgICAnc29ydCcsXHJcbiAgICAgICAgc29ydFBhcmFtLmZpZWxkICsgJywnICsgc29ydFBhcmFtLm9yZGVyXHJcbiAgICAgICk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBsZXQgb2JzZXJ2YWJsZSA9IHRoaXMuaHR0cENsaWVudC5nZXQ8UGFnZTxULCBJRD4+KHVybCwge1xyXG4gICAgICBwYXJhbXM6IGh0dHBQYXJhbXMsXHJcbiAgICB9KTtcclxuXHJcbiAgICBvYnNlcnZhYmxlLnN1YnNjcmliZShcclxuICAgICAgKHBhZ2UpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5fYWRkUGFnZShwYWdlKSkgdGhpcy5ub3RpZnkoKTtcclxuICAgICAgfSxcclxuICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgIHRoaXMuc2hvd0Vycm9yKGVycik7XHJcbiAgICAgIH1cclxuICAgICk7XHJcblxyXG4gICAgcmV0dXJuIG9ic2VydmFibGU7XHJcbiAgfTtcclxuXHJcbiAgcHJvdGVjdGVkIHNob3dFcnJvciA9IChlcnI6IGFueSk6IHZvaWQgPT4ge1xyXG4gICAgY29uc29sZS5sb2coZXJyKTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0UGFnZXMgPSAoKTogTWFwPFBhZ2VLZXksIFBhZ2U8VCwgSUQ+PiA9PiB7XHJcbiAgICAvLyB2cmFjYW1vIGNsb25lXHJcbiAgICByZXR1cm4gdGhpcy5wYWdlcy5jbG9uZSgpO1xyXG4gIH07XHJcblxyXG4gIHByaXZhdGUgX2FkZFBhZ2UgPSAocGFnZTogUGFnZTxULCBJRD4pOiBib29sZWFuID0+IHtcclxuICAgIGlmIChcclxuICAgICAgIXRoaXMucGFnZXMuaGFzKHtcclxuICAgICAgICBwYWdlTnVtYmVyOiBwYWdlLnBhZ2VhYmxlLnBhZ2VOdW1iZXIsXHJcbiAgICAgICAgcGFnZVNpemU6IHBhZ2UucGFnZWFibGUucGFnZVNpemUsXHJcbiAgICAgICAgc29ydDogcGFnZS5wYWdlYWJsZS5zb3J0LFxyXG4gICAgICB9KVxyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMucGFnZXMuZ2V0TWFwKCkuc2V0KFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHBhZ2VOdW1iZXI6IHBhZ2UucGFnZWFibGUucGFnZU51bWJlcixcclxuICAgICAgICAgIHBhZ2VTaXplOiBwYWdlLnBhZ2VhYmxlLnBhZ2VTaXplLFxyXG4gICAgICAgICAgc29ydDogcGFnZS5wYWdlYWJsZS5zb3J0LFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcGFnZVxyXG4gICAgICApO1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgZ2V0UGFnZVNpemUgPSAoKTogbnVtYmVyID0+IHtcclxuICAgIHJldHVybiB0aGlzLnBhZ2VTaXplO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRQYWdlU2l6ZSA9IChwYWdlU2l6ZTogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnBhZ2VTaXplID0gcGFnZVNpemU7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIHNldFBhdGggPSAocGF0aDogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLnBhdGggPSBwYXRoO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRQYXRoID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5wYXRoO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBzZXRDcml0ZXJpYVBhdGggPSAoY3JpdGVyaWFQYXRoOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuY3JpdGVyaWFQYXRoID0gY3JpdGVyaWFQYXRoO1xyXG4gIH07XHJcblxyXG4gIHB1YmxpYyBnZXRDcml0ZXJpYVBhdGggPSAoKTogc3RyaW5nID0+IHtcclxuICAgIHJldHVybiB0aGlzLmNyaXRlcmlhUGF0aDtcclxuICB9O1xyXG5cclxuICBwdWJsaWMgcmVzZXRQYXRoQW5kQ3JpdGVyaWFQYXRoVG9EZWZhdWx0ID0gKCk6IHZvaWQgPT4ge1xyXG4gICAgdGhpcy5wYXRoID0gdGhpcy5fZGVmYXVsdFBhdGg7XHJcbiAgICB0aGlzLmNyaXRlcmlhUGF0aCA9IHRoaXMuX2RlZmF1bHRDcml0ZXJpYVBhdGg7XHJcbiAgfTtcclxuXHJcbiAgcHVibGljIGJ1aWxkRW50aXRldCA9IChlOiBFTlRJVEVUX01PREVMX0lOVEVSRkFDRSk6IEVudGl0ZXQ8VCwgSUQ+ID0+IHtcclxuICAgIHJldHVybiB0aGlzLmZhY3RvcnkuYnVpbGQoZSk7XHJcbiAgfTtcclxufVxyXG4iXX0=