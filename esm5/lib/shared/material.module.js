import { __read, __spread } from "tslib";
import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import * as i0 from "@angular/core";
var material = [
    MatFormFieldModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
];
var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule.ɵmod = i0.ɵɵdefineNgModule({ type: MaterialModule });
    MaterialModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [__spread(material), MatFormFieldModule,
            MatAutocompleteModule,
            MatSlideToggleModule,
            MatInputModule,
            MatListModule,
            MatProgressSpinnerModule,
            MatProgressBarModule] });
    return MaterialModule;
}());
export { MaterialModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MaterialModule, { imports: [MatFormFieldModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatInputModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatProgressBarModule], exports: [MatFormFieldModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatInputModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatProgressBarModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaterialModule, [{
        type: NgModule,
        args: [{
                imports: __spread(material),
                exports: __spread(material),
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYXV0b2NvbXBsZXRlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tYXRlcmlhbC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDbEUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDdkUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDdEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUM5RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQzs7QUFFdEUsSUFBTSxRQUFRLEdBQUc7SUFDZixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLG9CQUFvQjtJQUNwQixjQUFjO0lBQ2QsYUFBYTtJQUNiLHdCQUF3QjtJQUN4QixvQkFBb0I7Q0FDckIsQ0FBQztBQUVGO0lBQUE7S0FJOEI7c0RBQWpCLGNBQWM7K0dBQWQsY0FBYywyQkFIWixRQUFRLEdBVnJCLGtCQUFrQjtZQUNsQixxQkFBcUI7WUFDckIsb0JBQW9CO1lBQ3BCLGNBQWM7WUFDZCxhQUFhO1lBQ2Isd0JBQXdCO1lBQ3hCLG9CQUFvQjt5QkFoQnRCO0NBdUI4QixBQUo5QixJQUk4QjtTQUFqQixjQUFjO3dGQUFkLGNBQWMsY0FiekIsa0JBQWtCO1FBQ2xCLHFCQUFxQjtRQUNyQixvQkFBb0I7UUFDcEIsY0FBYztRQUNkLGFBQWE7UUFDYix3QkFBd0I7UUFDeEIsb0JBQW9CLGFBTnBCLGtCQUFrQjtRQUNsQixxQkFBcUI7UUFDckIsb0JBQW9CO1FBQ3BCLGNBQWM7UUFDZCxhQUFhO1FBQ2Isd0JBQXdCO1FBQ3hCLG9CQUFvQjtrREFPVCxjQUFjO2NBSjFCLFFBQVE7ZUFBQztnQkFDUixPQUFPLFdBQU0sUUFBUSxDQUFDO2dCQUN0QixPQUFPLFdBQU0sUUFBUSxDQUFDO2FBQ3ZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0Rm9ybUZpZWxkTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZm9ybS1maWVsZCc7XHJcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7IE1hdFNsaWRlVG9nZ2xlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2xpZGUtdG9nZ2xlJztcclxuaW1wb3J0IHsgTWF0SW5wdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pbnB1dCc7XHJcbmltcG9ydCB7IE1hdExpc3RNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9saXN0JztcclxuaW1wb3J0IHsgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3Mtc3Bpbm5lcic7XHJcbmltcG9ydCB7IE1hdFByb2dyZXNzQmFyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3MtYmFyJztcclxuXHJcbmNvbnN0IG1hdGVyaWFsID0gW1xyXG4gIE1hdEZvcm1GaWVsZE1vZHVsZSxcclxuICBNYXRBdXRvY29tcGxldGVNb2R1bGUsXHJcbiAgTWF0U2xpZGVUb2dnbGVNb2R1bGUsXHJcbiAgTWF0SW5wdXRNb2R1bGUsXHJcbiAgTWF0TGlzdE1vZHVsZSxcclxuICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXHJcbiAgTWF0UHJvZ3Jlc3NCYXJNb2R1bGUsXHJcbl07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFsuLi5tYXRlcmlhbF0sXHJcbiAgZXhwb3J0czogWy4uLm1hdGVyaWFsXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsTW9kdWxlIHt9XHJcbiJdfQ==