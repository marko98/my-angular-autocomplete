import { Component, ChangeDetectionStrategy, Input, HostListener, } from '@angular/core';
import { transferArrayItem, moveItemInArray, } from '@angular/cdk/drag-drop';
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout/flex";
import * as i2 from "@angular/material/form-field";
import * as i3 from "./shared/directive/opacity-transition.directive";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/forms";
import * as i6 from "@angular/common";
import * as i7 from "@angular/material/slide-toggle";
import * as i8 from "@angular/cdk/drag-drop";
import * as i9 from "@angular/material/progress-bar";
import * as i10 from "@angular/cdk/scrolling";
import * as i11 from "./shared/directive/height.directive";
import * as i12 from "@angular/material/list";
import * as i13 from "@angular/material/progress-spinner";
function AutocompleteComponent_mat_slide_toggle_8_Template(rf, ctx) { if (rf & 1) {
    var _r6 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-slide-toggle", 8);
    i0.ɵɵlistener("change", function AutocompleteComponent_mat_slide_toggle_8_Template_mat_slide_toggle_change_0_listener($event) { i0.ɵɵrestoreView(_r6); var ctx_r5 = i0.ɵɵnextContext(); return ctx_r5.onSlideToggleChange($event); });
    i0.ɵɵtext(1, "sort?");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("appOpacityDuration", 1)("color", "primary")("checked", false)("disabled", false);
} }
function AutocompleteComponent_section_9_div_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r13 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r13);
} }
function AutocompleteComponent_section_9_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r14 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r14);
} }
function AutocompleteComponent_section_9_div_18_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r15 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r15);
} }
var _c0 = function (a0, a1) { return [a0, a1]; };
function AutocompleteComponent_section_9_Template(rf, ctx) { if (rf & 1) {
    var _r17 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "section", 9);
    i0.ɵɵelementStart(1, "section", 10);
    i0.ɵɵelementStart(2, "h2");
    i0.ɵɵtext(3, "criteria");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 11, 12);
    i0.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_4_listener($event) { i0.ɵɵrestoreView(_r17); var ctx_r16 = i0.ɵɵnextContext(); return ctx_r16.drop($event); });
    i0.ɵɵtemplate(6, AutocompleteComponent_section_9_div_6_Template, 2, 1, "div", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "section", 10);
    i0.ɵɵelementStart(8, "h2");
    i0.ɵɵtext(9, "asc");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 11, 14);
    i0.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_10_listener($event) { i0.ɵɵrestoreView(_r17); var ctx_r18 = i0.ɵɵnextContext(); return ctx_r18.drop($event); });
    i0.ɵɵtemplate(12, AutocompleteComponent_section_9_div_12_Template, 2, 1, "div", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "section", 10);
    i0.ɵɵelementStart(14, "h2");
    i0.ɵɵtext(15, "desc");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(16, "div", 11, 15);
    i0.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_16_listener($event) { i0.ɵɵrestoreView(_r17); var ctx_r19 = i0.ɵɵnextContext(); return ctx_r19.drop($event); });
    i0.ɵɵtemplate(18, AutocompleteComponent_section_9_div_18_Template, 2, 1, "div", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var _r7 = i0.ɵɵreference(5);
    var _r9 = i0.ɵɵreference(11);
    var _r11 = i0.ɵɵreference(17);
    var ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵproperty("appOpacityDuration", 1);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("cdkDropListData", ctx_r3.autocomplete.sortingOptions)("cdkDropListConnectedTo", i0.ɵɵpureFunction2(10, _c0, _r9, _r11));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r3.autocomplete.sortingOptions);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("cdkDropListData", ctx_r3.ascList)("cdkDropListConnectedTo", i0.ɵɵpureFunction2(13, _c0, _r7, _r11));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r3.ascList);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("cdkDropListData", ctx_r3.descList)("cdkDropListConnectedTo", i0.ɵɵpureFunction2(16, _c0, _r7, _r9));
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngForOf", ctx_r3.descList);
} }
function AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-progress-bar", 20);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h2");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r25 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(item_r25.getContextToShow());
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-spinner", 26);
} if (rf & 2) {
    i0.ɵɵproperty("diameter", 20);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template(rf, ctx) { if (rf & 1) {
    var _r36 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-list-item", 24);
    i0.ɵɵlistener("click", function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template_mat_list_item_click_0_listener() { i0.ɵɵrestoreView(_r36); var item_r25 = ctx.$implicit; var ctx_r35 = i0.ɵɵnextContext(3); return ctx_r35.itemSelected(item_r25); });
    i0.ɵɵtemplate(1, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template, 2, 1, "h2", 7);
    i0.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template, 1, 1, "mat-spinner", 25);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var item_r25 = ctx.$implicit;
    var observables_r20 = i0.ɵɵnextContext(2).ngIf;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", !observables_r20.isLoading && item_r25);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", observables_r20.isLoading);
} }
function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "cdk-virtual-scroll-viewport", 21);
    i0.ɵɵelementStart(1, "mat-list", 22);
    i0.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template, 3, 2, "mat-list-item", 23);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var observables_r20 = i0.ɵɵnextContext().ngIf;
    var ctx_r22 = i0.ɵɵnextContext();
    i0.ɵɵproperty("appOpacityDuration", 1)("itemSize", observables_r20.totalItems)("height", observables_r20.totalItems * 60 > 200 ? 200 : observables_r20.totalItems * 60);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("cdkVirtualForOf", ctx_r22.autocomplete.pageDataSource)("cdkVirtualForTemplateCacheSize", 0);
} }
function AutocompleteComponent_ng_container_10_h2_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "h2", 27);
    i0.ɵɵtext(1, "No data to display");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    i0.ɵɵproperty("appOpacityDuration", 1);
} }
function AutocompleteComponent_ng_container_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template, 1, 0, "mat-progress-bar", 17);
    i0.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template, 3, 5, "cdk-virtual-scroll-viewport", 18);
    i0.ɵɵtemplate(3, AutocompleteComponent_ng_container_10_h2_3_Template, 2, 1, "h2", 19);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    var observables_r20 = ctx.ngIf;
    var ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.showScrollView && !observables_r20.hasFetchedData);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && observables_r20.hasDataToShow);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && !observables_r20.hasDataToShow);
} }
var _c1 = function (a0, a1, a2, a3) { return { hasFetchedData: a0, hasDataToShow: a1, isLoading: a2, totalItems: a3 }; };
var AutocompleteComponent = /** @class */ (function () {
    function AutocompleteComponent(elRef) {
        var _this = this;
        this.elRef = elRef;
        this.showScrollView = false;
        this.sort = false;
        this.ascList = [];
        this.descList = [];
        this._previousCriteria = undefined;
        this.onFocus = function (el) {
            // console.log('focus on ', el);
            _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria);
            _this.showScrollView = true;
        };
        this.onModelChange = function (criteria, ngModel) {
            _this.autocomplete.criteria = _this.criteria;
            if (_this._timeoutForFetching)
                clearTimeout(_this._timeoutForFetching);
            _this._timeoutForFetching = setTimeout(function () {
                if (_this._previousCriteria !== _this.criteria) {
                    _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria, _this._getSortingParams());
                }
                _this._previousCriteria = _this.criteria;
            }, 500);
            _this.autocomplete.value = undefined;
            _this.autocomplete.valueSubject.next(_this.autocomplete.value);
            // console.log(this.autocomplete);
        };
        this.itemSelected = function (optionItem) {
            // console.log(optionItem);
            if (optionItem) {
                // console.log(optionItem);
                _this.autocomplete.criteria = optionItem.getContextToShow();
                _this.autocomplete.value = optionItem;
                _this.autocomplete.valueSubject.next(_this.autocomplete.value);
                _this.criteria = _this.autocomplete.criteria;
                _this.showScrollView = false;
                _this.sort = false;
                // console.log(this.autocomplete);
            }
        };
        this.onSlideToggleChange = function (matSlideToggleChange) {
            _this.sort = matSlideToggleChange.checked;
            // console.log(this.sort);
            if (!_this.sort)
                _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria, []);
        };
        this._getSortingParams = function () {
            var sortParams = [];
            _this.ascList.forEach(function (field) {
                sortParams.push({ field: field, order: 'asc' });
            });
            _this.descList.forEach(function (field) {
                sortParams.push({ field: field, order: 'desc' });
            });
            return sortParams;
        };
    }
    AutocompleteComponent.prototype.clickout = function (event) {
        if (this.elRef.nativeElement.contains(event.target)) {
            // console.log('focus on Autocomplete2Component');
        }
        else {
            // console.log('blur on Autocomplete2Component');
            this.showScrollView = false;
            this.sort = false;
        }
    };
    AutocompleteComponent.prototype.drop = function (event) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.autocomplete.pageDataSource.resetAndFetch(this.criteria, this._getSortingParams());
        }
    };
    AutocompleteComponent.prototype.ngOnInit = function () {
        this.criteria = this.autocomplete.criteria;
        this._previousCriteria = this.criteria;
        // console.log('Autocomplete2Component init');
    };
    AutocompleteComponent.prototype.ngOnDestroy = function () {
        if (this._timeoutForFetching)
            clearTimeout(this._timeoutForFetching);
        if (this.autocomplete.pageDataSource)
            this.autocomplete.pageDataSource.myDisconnect();
        // console.log('Autocomplete2Component destroyed');
    };
    AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
    AutocompleteComponent.ɵcmp = i0.ɵɵdefineComponent({ type: AutocompleteComponent, selectors: [["lib-autocomplete"]], hostBindings: function AutocompleteComponent_HostBindings(rf, ctx) { if (rf & 1) {
            i0.ɵɵlistener("click", function AutocompleteComponent_click_HostBindingHandler($event) { return ctx.clickout($event); }, false, i0.ɵɵresolveDocument);
        } }, inputs: { autocomplete: "autocomplete" }, decls: 15, vars: 22, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 1, "full-width"], ["fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "4vw", 1, "full-width"], ["fxFlex", "", "appOpacityTransition", "", 3, "appearance", "appOpacityDuration"], ["type", "text", "matInput", "", 3, "ngModel", "disabled", "autocomplete", "ngModelChange", "focus"], ["inputCriteria", "ngModel", "inputEl", ""], ["class", "slide-toggle", "appOpacityTransition", "", 3, "appOpacityDuration", "color", "checked", "disabled", "change", 4, "ngIf"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration", 4, "ngIf"], [4, "ngIf"], ["appOpacityTransition", "", 1, "slide-toggle", 3, "appOpacityDuration", "color", "checked", "disabled", "change"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration"], ["fxLayout", "column", "fxLayoutAlign", "start center", 1, "example-container"], ["cdkDropList", "", 1, "example-list", 3, "cdkDropListData", "cdkDropListConnectedTo", "cdkDropListDropped"], ["criteriaDropList", "cdkDropList"], ["class", "example-box", "cdkDrag", "", 4, "ngFor", "ngForOf"], ["ascDropList", "cdkDropList"], ["descDropList", "cdkDropList"], ["cdkDrag", "", 1, "example-box"], ["mode", "indeterminate", 4, "ngIf"], ["appOpacityTransition", "", "class", "my-viewport", "appHeight", "", 3, "appOpacityDuration", "itemSize", "height", 4, "ngIf"], ["appOpacityTransition", "", 3, "appOpacityDuration", 4, "ngIf"], ["mode", "indeterminate"], ["appOpacityTransition", "", "appHeight", "", 1, "my-viewport", 3, "appOpacityDuration", "itemSize", "height"], ["role", "list"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click", 4, "cdkVirtualFor", "cdkVirtualForOf", "cdkVirtualForTemplateCacheSize"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click"], [3, "diameter", 4, "ngIf"], [3, "diameter"], ["appOpacityTransition", "", 3, "appOpacityDuration"]], template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
            var _r39 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "section", 0);
            i0.ɵɵelementStart(1, "section", 1);
            i0.ɵɵelementStart(2, "mat-form-field", 2);
            i0.ɵɵelementStart(3, "mat-label");
            i0.ɵɵtext(4);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "input", 3, 4);
            i0.ɵɵlistener("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { return ctx.criteria = $event; })("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { i0.ɵɵrestoreView(_r39); var _r0 = i0.ɵɵreference(6); return ctx.onModelChange($event, _r0); })("focus", function AutocompleteComponent_Template_input_focus_5_listener() { i0.ɵɵrestoreView(_r39); var _r1 = i0.ɵɵreference(7); return ctx.onFocus(_r1); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(8, AutocompleteComponent_mat_slide_toggle_8_Template, 2, 4, "mat-slide-toggle", 5);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(9, AutocompleteComponent_section_9_Template, 19, 19, "section", 6);
            i0.ɵɵtemplate(10, AutocompleteComponent_ng_container_10_Template, 4, 3, "ng-container", 7);
            i0.ɵɵpipe(11, "async");
            i0.ɵɵpipe(12, "async");
            i0.ɵɵpipe(13, "async");
            i0.ɵɵpipe(14, "async");
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("appearance", ctx.autocomplete.appearance ? ctx.autocomplete.appearance : "fill")("appOpacityDuration", 1);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.autocomplete.inputPlaceHolder);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngModel", ctx.criteria)("disabled", ctx.autocomplete.disabled)("autocomplete", "off");
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("ngIf", ctx.showScrollView && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.showScrollView && ctx.sort && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", i0.ɵɵpureFunction4(17, _c1, i0.ɵɵpipeBind1(11, 9, ctx.autocomplete.pageDataSource.getHasFetchedDataSubject()), i0.ɵɵpipeBind1(12, 11, ctx.autocomplete.pageDataSource.getHasDataToShowSubject()), i0.ɵɵpipeBind1(13, 13, ctx.autocomplete.pageDataSource.isLoading()), i0.ɵɵpipeBind1(14, 15, ctx.autocomplete.pageDataSource.getTotalItemsSubject())));
        } }, directives: [i1.DefaultLayoutDirective, i1.DefaultLayoutAlignDirective, i1.DefaultLayoutGapDirective, i2.MatFormField, i1.DefaultFlexDirective, i3.OpacityTransitionDirective, i2.MatLabel, i4.MatInput, i5.DefaultValueAccessor, i5.NgControlStatus, i5.NgModel, i6.NgIf, i7.MatSlideToggle, i8.CdkDropList, i6.NgForOf, i8.CdkDrag, i9.MatProgressBar, i10.CdkVirtualScrollViewport, i10.CdkFixedSizeVirtualScroll, i11.HeightDirective, i12.MatList, i10.CdkVirtualForOf, i12.MatListItem, i13.MatSpinner], pipes: [i6.AsyncPipe], styles: [".my-viewport[_ngcontent-%COMP%] {\n        height: 150px;\n        width: 100%;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }\n\n    h2[_ngcontent-%COMP%] {\n        font: inherit;\n    }\n\n    mat-list-item[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    cdk-virtual-scroll-viewport[_ngcontent-%COMP%] {\n        box-shadow: 1px 2px 3px grey;\n    }\n\n    .full-width[_ngcontent-%COMP%] {\n        width: 100%;\n    }\n\n    \n\n    .example-container[_ngcontent-%COMP%] {\n        \n        margin: 0 25px 25px 0;\n        display: inline-block;\n        vertical-align: top;\n    }\n\n    .example-list[_ngcontent-%COMP%] {\n        border: solid 1px #ccc;\n        min-height: 60px;\n        min-width: 60px;\n        background: white;\n        border-radius: 4px;\n        overflow: hidden;\n        display: block;\n    }\n\n    .example-box[_ngcontent-%COMP%] {\n        padding: 20px 10px;\n        border-bottom: solid 1px #ccc;\n        color: rgba(0, 0, 0, 0.87);\n        display: flex;\n        flex-direction: row;\n        align-items: center;\n        justify-content: space-between;\n        box-sizing: border-box;\n        cursor: move;\n        background: white;\n        font-size: 14px;\n    }\n\n    .cdk-drag-preview[_ngcontent-%COMP%] {\n        box-sizing: border-box;\n        border-radius: 4px;\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\n    }\n\n    .cdk-drag-placeholder[_ngcontent-%COMP%] {\n        opacity: 0;\n    }\n\n    .cdk-drag-animating[_ngcontent-%COMP%] {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }\n\n    .example-box[_ngcontent-%COMP%]:last-child {\n        border: none;\n    }\n\n    .example-list.cdk-drop-list-dragging[_ngcontent-%COMP%]   .example-box[_ngcontent-%COMP%]:not(.cdk-drag-placeholder) {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }"], changeDetection: 0 });
    return AutocompleteComponent;
}());
export { AutocompleteComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AutocompleteComponent, [{
        type: Component,
        args: [{
                selector: 'lib-autocomplete',
                templateUrl: './autocomplete.component.html',
                styles: [],
                changeDetection: ChangeDetectionStrategy.OnPush,
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { autocomplete: [{
            type: Input
        }], clickout: [{
            type: HostListener,
            args: ['document:click', ['$event']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2F1dG9jb21wbGV0ZS8iLCJzb3VyY2VzIjpbImxpYi9hdXRvY29tcGxldGUuY29tcG9uZW50LnRzIiwibGliL2F1dG9jb21wbGV0ZS5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULHVCQUF1QixFQUN2QixLQUFLLEVBR0wsWUFBWSxHQUNiLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFDTCxpQkFBaUIsRUFDakIsZUFBZSxHQUVoQixNQUFNLHdCQUF3QixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztJQ3NHcEIsMkNBUWdEO0lBQTVDLHFPQUEyQztJQUFDLHFCQUFLO0lBQUEsaUJBQW1COztJQUpwRSxzQ0FBd0Isb0JBQUEsa0JBQUEsbUJBQUE7OztJQTRCaEIsK0JBQXVGO0lBQUEsWUFBUTtJQUFBLGlCQUFNOzs7SUFBZCxlQUFRO0lBQVIsOEJBQVE7OztJQWdCL0YsK0JBQW1FO0lBQUEsWUFBUTtJQUFBLGlCQUFNOzs7SUFBZCxlQUFRO0lBQVIsOEJBQVE7OztJQWdCM0UsK0JBQW9FO0lBQUEsWUFBUTtJQUFBLGlCQUFNOzs7SUFBZCxlQUFRO0lBQVIsOEJBQVE7Ozs7O0lBcERoRyxrQ0FRUTtJQUNBLG1DQUdRO0lBQUEsMEJBQUk7SUFBQSx3QkFBUTtJQUFBLGlCQUFLO0lBQ2pCLG1DQU9JO0lBREEsMk5BQW1DO0lBQ25DLGlGQUF1RjtJQUMzRixpQkFBTTtJQUNkLGlCQUFVO0lBR1YsbUNBR1E7SUFBQSwwQkFBSTtJQUFBLG1CQUFHO0lBQUEsaUJBQUs7SUFDWixvQ0FPSTtJQURBLDROQUFtQztJQUNuQyxtRkFBbUU7SUFDdkUsaUJBQU07SUFDZCxpQkFBVTtJQUdWLG9DQUdRO0lBQUEsMkJBQUk7SUFBQSxxQkFBSTtJQUFBLGlCQUFLO0lBQ2Isb0NBT0k7SUFEQSw0TkFBbUM7SUFDbkMsbUZBQW9FO0lBQ3hFLGlCQUFNO0lBQ2QsaUJBQVU7SUFFbEIsaUJBQVU7Ozs7OztJQXJETixzQ0FBd0I7SUFhUixlQUFvRDtJQUFwRCxvRUFBb0Qsa0VBQUE7SUFJM0IsZUFBcUQ7SUFBckQsNERBQXFEO0lBWTlFLGVBQWdDO0lBQWhDLGdEQUFnQyxrRUFBQTtJQUlQLGVBQWlDO0lBQWpDLHdDQUFpQztJQVkxRCxlQUFpQztJQUFqQyxpREFBaUMsaUVBQUE7SUFJUixlQUFrQztJQUFsQyx5Q0FBa0M7OztJQWF2RSx1Q0FFa0Y7OztJQTBCMUQsMEJBQTJDO0lBQUEsWUFBMkI7SUFBQSxpQkFBSzs7O0lBQWhDLGVBQTJCO0lBQTNCLGlEQUEyQjs7O0lBQ3RFLGtDQUF5RTs7SUFBOUIsNkJBQWU7Ozs7SUFkbEUseUNBYVE7SUFESixrU0FBaUM7SUFDN0Isa0lBQTJDO0lBQzNDLHFKQUEyRDtJQUNuRSxpQkFBZ0I7Ozs7SUFGSixlQUFzQztJQUF0Qyw2REFBc0M7SUFDN0IsZUFBNkI7SUFBN0IsZ0RBQTZCOzs7SUF4QmxFLHVEQVFRO0lBQUEsb0NBRVE7SUFBQSx5SUFhUTtJQUdoQixpQkFBVztJQUNuQixpQkFBOEI7Ozs7SUF4QjFCLHNDQUF3Qix3Q0FBQSx5RkFBQTtJQVNSLGVBT3FDO0lBUHJDLHFFQU9xQyxxQ0FBQTs7O0lBVXpELDhCQUc2QjtJQUFBLGtDQUFrQjtJQUFBLGlCQUFLOztJQUFoRCxzQ0FBd0I7OztJQTVDcEMsNkJBT1E7SUFBQSxpSEFFK0Q7SUFHL0QsdUlBUVE7SUFxQlIscUZBRzZCO0lBRXJDLDBCQUFlOzs7O0lBckNILGVBQTBEO0lBQTFELCtFQUEwRDtJQUkxRCxlQUFzRjtJQUF0RiwrR0FBc0Y7SUE2QnRGLGVBQXVGO0lBQXZGLGdIQUF1Rjs7O0FEaE52RztJQWtCRSwrQkFBb0IsS0FBaUI7UUFBckMsaUJBQXlDO1FBQXJCLFVBQUssR0FBTCxLQUFLLENBQVk7UUFUOUIsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsU0FBSSxHQUFZLEtBQUssQ0FBQztRQUN0QixZQUFPLEdBQWEsRUFBRSxDQUFDO1FBQ3ZCLGFBQVEsR0FBYSxFQUFFLENBQUM7UUFJdkIsc0JBQWlCLEdBQVcsU0FBUyxDQUFDO1FBZXZDLFlBQU8sR0FBRyxVQUFDLEVBQW9CO1lBQ3BDLGdDQUFnQztZQUVoQyxLQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTlELEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzdCLENBQUMsQ0FBQztRQUVLLGtCQUFhLEdBQUcsVUFBQyxRQUFhLEVBQUUsT0FBZ0I7WUFDckQsS0FBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQztZQUUzQyxJQUFJLEtBQUksQ0FBQyxtQkFBbUI7Z0JBQUUsWUFBWSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQ3JFLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxVQUFVLENBQUM7Z0JBQ3BDLElBQUksS0FBSSxDQUFDLGlCQUFpQixLQUFLLEtBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQzVDLEtBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FDNUMsS0FBSSxDQUFDLFFBQVEsRUFDYixLQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FDekIsQ0FBQztpQkFDSDtnQkFFRCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQztZQUN6QyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFFUixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7WUFDcEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0Qsa0NBQWtDO1FBQ3BDLENBQUMsQ0FBQztRQUVLLGlCQUFZLEdBQUcsVUFBQyxVQUFzQjtZQUMzQywyQkFBMkI7WUFDM0IsSUFBSSxVQUFVLEVBQUU7Z0JBQ2QsMkJBQTJCO2dCQUMzQixLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDM0QsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDO2dCQUNyQyxLQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDN0QsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztnQkFFM0MsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO2dCQUVsQixrQ0FBa0M7YUFDbkM7UUFDSCxDQUFDLENBQUM7UUFFSyx3QkFBbUIsR0FBRyxVQUMzQixvQkFBMEM7WUFFMUMsS0FBSSxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQyxPQUFPLENBQUM7WUFDekMsMEJBQTBCO1lBRTFCLElBQUksQ0FBQyxLQUFJLENBQUMsSUFBSTtnQkFDWixLQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN0RSxDQUFDLENBQUM7UUF1Qk0sc0JBQWlCLEdBQUc7WUFDMUIsSUFBSSxVQUFVLEdBQWdCLEVBQUUsQ0FBQztZQUNqQyxLQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUs7Z0JBQ3pCLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQ2xELENBQUMsQ0FBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO2dCQUMxQixVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztZQUNuRCxDQUFDLENBQUMsQ0FBQztZQUVILE9BQU8sVUFBVSxDQUFDO1FBQ3BCLENBQUMsQ0FBQztJQW5Hc0MsQ0FBQztJQUd6Qyx3Q0FBUSxHQURSLFVBQ1MsS0FBSztRQUNaLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNuRCxrREFBa0Q7U0FDbkQ7YUFBTTtZQUNMLGlEQUFpRDtZQUNqRCxJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztZQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztTQUNuQjtJQUNILENBQUM7SUF3REQsb0NBQUksR0FBSixVQUFLLEtBQTRCO1FBQy9CLElBQUksS0FBSyxDQUFDLGlCQUFpQixLQUFLLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDL0MsZUFBZSxDQUNiLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUNwQixLQUFLLENBQUMsYUFBYSxFQUNuQixLQUFLLENBQUMsWUFBWSxDQUNuQixDQUFDO1NBQ0g7YUFBTTtZQUNMLGlCQUFpQixDQUNmLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQzVCLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUNwQixLQUFLLENBQUMsYUFBYSxFQUNuQixLQUFLLENBQUMsWUFBWSxDQUNuQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUM1QyxJQUFJLENBQUMsUUFBUSxFQUNiLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUN6QixDQUFDO1NBQ0g7SUFDSCxDQUFDO0lBZUQsd0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7UUFDM0MsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFFdkMsOENBQThDO0lBQ2hELENBQUM7SUFFRCwyQ0FBVyxHQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsbUJBQW1CO1lBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3JFLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjO1lBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRWxELG1EQUFtRDtJQUNyRCxDQUFDOzhGQTlIVSxxQkFBcUI7OERBQXJCLHFCQUFxQjs0R0FBckIsb0JBQWdCOzs7WUM0RDdCLGtDQU1JO1lBQUEsa0NBTVE7WUFBQSx5Q0FLUTtZQUFBLGlDQUFXO1lBQUEsWUFBc0M7WUFBQSxpQkFBWTtZQUM3RCxtQ0FVUjtZQVBZLGdKQUEyQiwrSkFFViw4QkFBeUMsSUFGL0IseUlBSWxCLGdCQUFxQixJQUpIO1lBSC9CLGlCQVVSO1lBQUEsaUJBQWlCO1lBRWpCLGdHQVFnRDtZQUV4RCxpQkFBVTtZQUVWLGdGQVFRO1lBa0RSLDBGQU9ROzs7OztZQXlDWixpQkFBVTs7WUF2SU0sZUFBbUY7WUFBbkYsK0ZBQW1GLHlCQUFBO1lBSXBFLGVBQXNDO1lBQXRDLHVEQUFzQztZQUk3QyxlQUEyQjtZQUEzQixzQ0FBMkIsdUNBQUEsdUJBQUE7WUFXbkMsZUFBbUk7WUFBbkksOElBQW1JO1lBVzNJLGVBQWdKO1lBQWhKLDBKQUFnSjtZQXlEdEksZUFLTztZQUxQLDZXQUtPOztnQ0QvTHpCO0NBeUpDLEFBcklELElBcUlDO1NBL0hZLHFCQUFxQjtrREFBckIscUJBQXFCO2NBTmpDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixXQUFXLEVBQUUsK0JBQStCO2dCQUM1QyxNQUFNLEVBQUUsRUFBRTtnQkFDVixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTthQUNoRDs7a0JBRUUsS0FBSzs7a0JBYUwsWUFBWTttQkFBQyxnQkFBZ0IsRUFBRSxDQUFDLFFBQVEsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcbiAgSW5wdXQsXG4gIE9uRGVzdHJveSxcbiAgRWxlbWVudFJlZixcbiAgSG9zdExpc3RlbmVyLFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFNvcnRQYXJhbSB9IGZyb20gJy4vc2hhcmVkL3NlcnZpY2UvcGFnZWFibGUtcmVhZC5zZXJ2aWNlJztcbmltcG9ydCB7XG4gIHRyYW5zZmVyQXJyYXlJdGVtLFxuICBtb3ZlSXRlbUluQXJyYXksXG4gIENka0RyYWdEcm9wLFxufSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcbmltcG9ydCB7IE1hdFNsaWRlVG9nZ2xlQ2hhbmdlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2xpZGUtdG9nZ2xlJztcbmltcG9ydCB7IE9wdGlvbkl0ZW0gfSBmcm9tICcuL3NoYXJlZC9tb2RlbC9pbnRlcmZhY2Uvb3B0aW9uLWl0ZW0uaW50ZXJmYWNlJztcbmltcG9ydCB7IEF1dG9jb21wbGV0ZUludGVyZmFjZSB9IGZyb20gJy4vc2hhcmVkL21vZGVsL2ludGVyZmFjZS9hdXRvY29tcGxldGUuaW50ZXJmYWNlJztcbmltcG9ydCB7IE5nTW9kZWwgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1hdXRvY29tcGxldGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVzOiBbXSxcbiAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXG59KVxuZXhwb3J0IGNsYXNzIEF1dG9jb21wbGV0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgQElucHV0KCkgYXV0b2NvbXBsZXRlOiBBdXRvY29tcGxldGVJbnRlcmZhY2U7XG5cbiAgcHVibGljIHNob3dTY3JvbGxWaWV3OiBib29sZWFuID0gZmFsc2U7XG4gIHB1YmxpYyBzb3J0OiBib29sZWFuID0gZmFsc2U7XG4gIHB1YmxpYyBhc2NMaXN0OiBzdHJpbmdbXSA9IFtdO1xuICBwdWJsaWMgZGVzY0xpc3Q6IHN0cmluZ1tdID0gW107XG4gIHB1YmxpYyBjcml0ZXJpYTogc3RyaW5nO1xuXG4gIHByaXZhdGUgX3RpbWVvdXRGb3JGZXRjaGluZztcbiAgcHJpdmF0ZSBfcHJldmlvdXNDcml0ZXJpYTogc3RyaW5nID0gdW5kZWZpbmVkO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYpIHt9XG5cbiAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6Y2xpY2snLCBbJyRldmVudCddKVxuICBjbGlja291dChldmVudCkge1xuICAgIGlmICh0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQuY29udGFpbnMoZXZlbnQudGFyZ2V0KSkge1xuICAgICAgLy8gY29uc29sZS5sb2coJ2ZvY3VzIG9uIEF1dG9jb21wbGV0ZTJDb21wb25lbnQnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gY29uc29sZS5sb2coJ2JsdXIgb24gQXV0b2NvbXBsZXRlMkNvbXBvbmVudCcpO1xuICAgICAgdGhpcy5zaG93U2Nyb2xsVmlldyA9IGZhbHNlO1xuICAgICAgdGhpcy5zb3J0ID0gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG9uRm9jdXMgPSAoZWw6IEhUTUxJbnB1dEVsZW1lbnQpOiB2b2lkID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZygnZm9jdXMgb24gJywgZWwpO1xuXG4gICAgdGhpcy5hdXRvY29tcGxldGUucGFnZURhdGFTb3VyY2UucmVzZXRBbmRGZXRjaCh0aGlzLmNyaXRlcmlhKTtcblxuICAgIHRoaXMuc2hvd1Njcm9sbFZpZXcgPSB0cnVlO1xuICB9O1xuXG4gIHB1YmxpYyBvbk1vZGVsQ2hhbmdlID0gKGNyaXRlcmlhOiBhbnksIG5nTW9kZWw6IE5nTW9kZWwpOiB2b2lkID0+IHtcbiAgICB0aGlzLmF1dG9jb21wbGV0ZS5jcml0ZXJpYSA9IHRoaXMuY3JpdGVyaWE7XG5cbiAgICBpZiAodGhpcy5fdGltZW91dEZvckZldGNoaW5nKSBjbGVhclRpbWVvdXQodGhpcy5fdGltZW91dEZvckZldGNoaW5nKTtcbiAgICB0aGlzLl90aW1lb3V0Rm9yRmV0Y2hpbmcgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGlmICh0aGlzLl9wcmV2aW91c0NyaXRlcmlhICE9PSB0aGlzLmNyaXRlcmlhKSB7XG4gICAgICAgIHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLnJlc2V0QW5kRmV0Y2goXG4gICAgICAgICAgdGhpcy5jcml0ZXJpYSxcbiAgICAgICAgICB0aGlzLl9nZXRTb3J0aW5nUGFyYW1zKClcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5fcHJldmlvdXNDcml0ZXJpYSA9IHRoaXMuY3JpdGVyaWE7XG4gICAgfSwgNTAwKTtcblxuICAgIHRoaXMuYXV0b2NvbXBsZXRlLnZhbHVlID0gdW5kZWZpbmVkO1xuICAgIHRoaXMuYXV0b2NvbXBsZXRlLnZhbHVlU3ViamVjdC5uZXh0KHRoaXMuYXV0b2NvbXBsZXRlLnZhbHVlKTtcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmF1dG9jb21wbGV0ZSk7XG4gIH07XG5cbiAgcHVibGljIGl0ZW1TZWxlY3RlZCA9IChvcHRpb25JdGVtOiBPcHRpb25JdGVtKTogdm9pZCA9PiB7XG4gICAgLy8gY29uc29sZS5sb2cob3B0aW9uSXRlbSk7XG4gICAgaWYgKG9wdGlvbkl0ZW0pIHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKG9wdGlvbkl0ZW0pO1xuICAgICAgdGhpcy5hdXRvY29tcGxldGUuY3JpdGVyaWEgPSBvcHRpb25JdGVtLmdldENvbnRleHRUb1Nob3coKTtcbiAgICAgIHRoaXMuYXV0b2NvbXBsZXRlLnZhbHVlID0gb3B0aW9uSXRlbTtcbiAgICAgIHRoaXMuYXV0b2NvbXBsZXRlLnZhbHVlU3ViamVjdC5uZXh0KHRoaXMuYXV0b2NvbXBsZXRlLnZhbHVlKTtcbiAgICAgIHRoaXMuY3JpdGVyaWEgPSB0aGlzLmF1dG9jb21wbGV0ZS5jcml0ZXJpYTtcblxuICAgICAgdGhpcy5zaG93U2Nyb2xsVmlldyA9IGZhbHNlO1xuICAgICAgdGhpcy5zb3J0ID0gZmFsc2U7XG5cbiAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuYXV0b2NvbXBsZXRlKTtcbiAgICB9XG4gIH07XG5cbiAgcHVibGljIG9uU2xpZGVUb2dnbGVDaGFuZ2UgPSAoXG4gICAgbWF0U2xpZGVUb2dnbGVDaGFuZ2U6IE1hdFNsaWRlVG9nZ2xlQ2hhbmdlXG4gICk6IHZvaWQgPT4ge1xuICAgIHRoaXMuc29ydCA9IG1hdFNsaWRlVG9nZ2xlQ2hhbmdlLmNoZWNrZWQ7XG4gICAgLy8gY29uc29sZS5sb2codGhpcy5zb3J0KTtcblxuICAgIGlmICghdGhpcy5zb3J0KVxuICAgICAgdGhpcy5hdXRvY29tcGxldGUucGFnZURhdGFTb3VyY2UucmVzZXRBbmRGZXRjaCh0aGlzLmNyaXRlcmlhLCBbXSk7XG4gIH07XG5cbiAgZHJvcChldmVudDogQ2RrRHJhZ0Ryb3A8c3RyaW5nW10+KSB7XG4gICAgaWYgKGV2ZW50LnByZXZpb3VzQ29udGFpbmVyID09PSBldmVudC5jb250YWluZXIpIHtcbiAgICAgIG1vdmVJdGVtSW5BcnJheShcbiAgICAgICAgZXZlbnQuY29udGFpbmVyLmRhdGEsXG4gICAgICAgIGV2ZW50LnByZXZpb3VzSW5kZXgsXG4gICAgICAgIGV2ZW50LmN1cnJlbnRJbmRleFxuICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdHJhbnNmZXJBcnJheUl0ZW0oXG4gICAgICAgIGV2ZW50LnByZXZpb3VzQ29udGFpbmVyLmRhdGEsXG4gICAgICAgIGV2ZW50LmNvbnRhaW5lci5kYXRhLFxuICAgICAgICBldmVudC5wcmV2aW91c0luZGV4LFxuICAgICAgICBldmVudC5jdXJyZW50SW5kZXhcbiAgICAgICk7XG4gICAgICB0aGlzLmF1dG9jb21wbGV0ZS5wYWdlRGF0YVNvdXJjZS5yZXNldEFuZEZldGNoKFxuICAgICAgICB0aGlzLmNyaXRlcmlhLFxuICAgICAgICB0aGlzLl9nZXRTb3J0aW5nUGFyYW1zKClcbiAgICAgICk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBfZ2V0U29ydGluZ1BhcmFtcyA9ICgpOiBTb3J0UGFyYW1bXSA9PiB7XG4gICAgbGV0IHNvcnRQYXJhbXM6IFNvcnRQYXJhbVtdID0gW107XG4gICAgdGhpcy5hc2NMaXN0LmZvckVhY2goKGZpZWxkKSA9PiB7XG4gICAgICBzb3J0UGFyYW1zLnB1c2goeyBmaWVsZDogZmllbGQsIG9yZGVyOiAnYXNjJyB9KTtcbiAgICB9KTtcblxuICAgIHRoaXMuZGVzY0xpc3QuZm9yRWFjaCgoZmllbGQpID0+IHtcbiAgICAgIHNvcnRQYXJhbXMucHVzaCh7IGZpZWxkOiBmaWVsZCwgb3JkZXI6ICdkZXNjJyB9KTtcbiAgICB9KTtcblxuICAgIHJldHVybiBzb3J0UGFyYW1zO1xuICB9O1xuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuY3JpdGVyaWEgPSB0aGlzLmF1dG9jb21wbGV0ZS5jcml0ZXJpYTtcbiAgICB0aGlzLl9wcmV2aW91c0NyaXRlcmlhID0gdGhpcy5jcml0ZXJpYTtcblxuICAgIC8vIGNvbnNvbGUubG9nKCdBdXRvY29tcGxldGUyQ29tcG9uZW50IGluaXQnKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIGlmICh0aGlzLl90aW1lb3V0Rm9yRmV0Y2hpbmcpIGNsZWFyVGltZW91dCh0aGlzLl90aW1lb3V0Rm9yRmV0Y2hpbmcpO1xuICAgIGlmICh0aGlzLmF1dG9jb21wbGV0ZS5wYWdlRGF0YVNvdXJjZSlcbiAgICAgIHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLm15RGlzY29ubmVjdCgpO1xuXG4gICAgLy8gY29uc29sZS5sb2coJ0F1dG9jb21wbGV0ZTJDb21wb25lbnQgZGVzdHJveWVkJyk7XG4gIH1cbn1cbiIsIjxzdHlsZT5cclxuICAgIC5teS12aWV3cG9ydCB7XHJcbiAgICAgICAgaGVpZ2h0OiAxNTBweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICBtYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcclxuICAgIH1cclxuXHJcbiAgICBoMiB7XHJcbiAgICAgICAgZm9udDogaW5oZXJpdDtcclxuICAgIH1cclxuXHJcbiAgICBtYXQtbGlzdC1pdGVtIHtcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgY2RrLXZpcnR1YWwtc2Nyb2xsLXZpZXdwb3J0IHtcclxuICAgICAgICBib3gtc2hhZG93OiAxcHggMnB4IDNweCBncmV5O1xyXG4gICAgfVxyXG5cclxuICAgIC5mdWxsLXdpZHRoIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICAvKiB6YSBkcmFnIGFuZCBkcm9wICovXHJcblxyXG4gICAgLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICAgICAgICAvKiB3aWR0aDogNDAwcHg7XHJcbiAgICAgICAgbWF4LXdpZHRoOiAxMDAlOyAqL1xyXG4gICAgICAgIG1hcmdpbjogMCAyNXB4IDI1cHggMDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICAgIH1cclxuXHJcbiAgICAuZXhhbXBsZS1saXN0IHtcclxuICAgICAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgbWluLXdpZHRoOiA2MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgfVxyXG5cclxuICAgIC5leGFtcGxlLWJveCB7XHJcbiAgICAgICAgcGFkZGluZzogMjBweCAxMHB4O1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjY2NjO1xyXG4gICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODcpO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgIGN1cnNvcjogbW92ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNkay1kcmFnLXByZXZpZXcge1xyXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgNXB4IDVweCAtM3B4IHJnYmEoMCwgMCwgMCwgMC4yKSxcclxuICAgICAgICAgICAgICAgICAgICAwIDhweCAxMHB4IDFweCByZ2JhKDAsIDAsIDAsIDAuMTQpLFxyXG4gICAgICAgICAgICAgICAgICAgIDAgM3B4IDE0cHggMnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XHJcbiAgICB9XHJcblxyXG4gICAgLmNkay1kcmFnLXBsYWNlaG9sZGVyIHtcclxuICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgfVxyXG5cclxuICAgIC5jZGstZHJhZy1hbmltYXRpbmcge1xyXG4gICAgICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcclxuICAgIH1cclxuXHJcbiAgICAuZXhhbXBsZS1ib3g6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5leGFtcGxlLWxpc3QuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZyAuZXhhbXBsZS1ib3g6bm90KC5jZGstZHJhZy1wbGFjZWhvbGRlcikge1xyXG4gICAgICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAyNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcclxuICAgIH1cclxuXHJcbjwvc3R5bGU+XHJcblxyXG48c2VjdGlvblxyXG4gICAgY2xhc3M9XCJmdWxsLXdpZHRoXCJcclxuICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcclxuICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCJcclxuICAgIGZ4TGF5b3V0R2FwPVwiMnZ3XCI+XHJcblxyXG4gICAgPHNlY3Rpb25cclxuICAgICAgICBjbGFzcz1cImZ1bGwtd2lkdGhcIlxyXG4gICAgICAgIGZ4TGF5b3V0XHJcbiAgICAgICAgZnhMYXlvdXRBbGlnbj1cImNlbnRlciBjZW50ZXJcIlxyXG4gICAgICAgIGZ4TGF5b3V0R2FwPVwiNHZ3XCI+XHJcblxyXG4gICAgICAgICAgICA8bWF0LWZvcm0tZmllbGQgXHJcbiAgICAgICAgICAgICAgICBbYXBwZWFyYW5jZV09XCJ0aGlzLmF1dG9jb21wbGV0ZS5hcHBlYXJhbmNlID8gdGhpcy5hdXRvY29tcGxldGUuYXBwZWFyYW5jZSA6ICdmaWxsJ1wiIFxyXG4gICAgICAgICAgICAgICAgZnhGbGV4XHJcbiAgICAgICAgICAgICAgICBhcHBPcGFjaXR5VHJhbnNpdGlvblxyXG4gICAgICAgICAgICAgICAgW2FwcE9wYWNpdHlEdXJhdGlvbl09XCIxXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG1hdC1sYWJlbD57e3RoaXMuYXV0b2NvbXBsZXRlLmlucHV0UGxhY2VIb2xkZXJ9fTwvbWF0LWxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF0SW5wdXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgWyhuZ01vZGVsKV09XCJ0aGlzLmNyaXRlcmlhXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgI2lucHV0Q3JpdGVyaWE9XCJuZ01vZGVsXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgKG5nTW9kZWxDaGFuZ2UpPVwidGhpcy5vbk1vZGVsQ2hhbmdlKCRldmVudCwgaW5wdXRDcml0ZXJpYSlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwidGhpcy5hdXRvY29tcGxldGUuZGlzYWJsZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoZm9jdXMpPVwidGhpcy5vbkZvY3VzKGlucHV0RWwpXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgW2F1dG9jb21wbGV0ZV09XCInb2ZmJ1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICNpbnB1dEVsPlxyXG4gICAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG4gICAgICAgIFxyXG4gICAgICAgICAgICA8bWF0LXNsaWRlLXRvZ2dsZVxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJzbGlkZS10b2dnbGVcIlxyXG4gICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNob3dTY3JvbGxWaWV3ICYmICh0aGlzLmF1dG9jb21wbGV0ZS5zb3J0aW5nT3B0aW9ucy5sZW5ndGggPiAwIHx8IHRoaXMuYXNjTGlzdC5sZW5ndGggPiAwIHx8IHRoaXMuZGVzY0xpc3QubGVuZ3RoID4gMClcIlxyXG4gICAgICAgICAgICAgICAgYXBwT3BhY2l0eVRyYW5zaXRpb25cclxuICAgICAgICAgICAgICAgIFthcHBPcGFjaXR5RHVyYXRpb25dPVwiMVwiXHJcbiAgICAgICAgICAgICAgICBbY29sb3JdPVwiJ3ByaW1hcnknXCJcclxuICAgICAgICAgICAgICAgIFtjaGVja2VkXT0nZmFsc2UnXHJcbiAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPSdmYWxzZSdcclxuICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwidGhpcy5vblNsaWRlVG9nZ2xlQ2hhbmdlKCRldmVudClcIj5zb3J0PzwvbWF0LXNsaWRlLXRvZ2dsZT5cclxuXHJcbiAgICA8L3NlY3Rpb24+XHJcblxyXG4gICAgPHNlY3Rpb25cclxuICAgICAgICAqbmdJZj1cInRoaXMuc2hvd1Njcm9sbFZpZXcgJiYgdGhpcy5zb3J0ICYmICh0aGlzLmF1dG9jb21wbGV0ZS5zb3J0aW5nT3B0aW9ucy5sZW5ndGggPiAwIHx8IHRoaXMuYXNjTGlzdC5sZW5ndGggPiAwIHx8IHRoaXMuZGVzY0xpc3QubGVuZ3RoID4gMClcIlxyXG4gICAgICAgIGFwcE9wYWNpdHlUcmFuc2l0aW9uXHJcbiAgICAgICAgW2FwcE9wYWNpdHlEdXJhdGlvbl09XCIxXCJcclxuICAgICAgICBmeExheW91dFxyXG4gICAgICAgIGZ4TGF5b3V0QWxpZ249XCJjZW50ZXIgY2VudGVyXCJcclxuICAgICAgICBmeExheW91dEdhcD1cIjJ2d1wiPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBjcml0ZXJpYSBzZWN0aW9uIC0tPlxyXG4gICAgICAgICAgICA8c2VjdGlvbiBjbGFzcz1cImV4YW1wbGUtY29udGFpbmVyXCJcclxuICAgICAgICAgICAgICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcclxuICAgICAgICAgICAgICAgIGZ4TGF5b3V0QWxpZ249XCJzdGFydCBjZW50ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDI+Y3JpdGVyaWE8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2RrRHJvcExpc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgI2NyaXRlcmlhRHJvcExpc3Q9XCJjZGtEcm9wTGlzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtjZGtEcm9wTGlzdERhdGFdPVwidGhpcy5hdXRvY29tcGxldGUuc29ydGluZ09wdGlvbnNcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbY2RrRHJvcExpc3RDb25uZWN0ZWRUb109XCJbYXNjRHJvcExpc3QsIGRlc2NEcm9wTGlzdF1cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImV4YW1wbGUtbGlzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjZGtEcm9wTGlzdERyb3BwZWQpPVwiZHJvcCgkZXZlbnQpXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJleGFtcGxlLWJveFwiICpuZ0Zvcj1cImxldCBpdGVtIG9mIHRoaXMuYXV0b2NvbXBsZXRlLnNvcnRpbmdPcHRpb25zXCIgY2RrRHJhZz57e2l0ZW19fTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L3NlY3Rpb24+XHJcblxyXG4gICAgICAgICAgICA8IS0tIGFzYyBzZWN0aW9uIC0tPlxyXG4gICAgICAgICAgICA8c2VjdGlvbiBjbGFzcz1cImV4YW1wbGUtY29udGFpbmVyXCJcclxuICAgICAgICAgICAgICAgIGZ4TGF5b3V0PVwiY29sdW1uXCJcclxuICAgICAgICAgICAgICAgIGZ4TGF5b3V0QWxpZ249XCJzdGFydCBjZW50ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDI+YXNjPC9oMj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNka0Ryb3BMaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICNhc2NEcm9wTGlzdD1cImNka0Ryb3BMaXN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgW2Nka0Ryb3BMaXN0RGF0YV09XCJ0aGlzLmFzY0xpc3RcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbY2RrRHJvcExpc3RDb25uZWN0ZWRUb109XCJbY3JpdGVyaWFEcm9wTGlzdCwgZGVzY0Ryb3BMaXN0XVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZXhhbXBsZS1saXN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGNka0Ryb3BMaXN0RHJvcHBlZCk9XCJkcm9wKCRldmVudClcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImV4YW1wbGUtYm94XCIgKm5nRm9yPVwibGV0IGl0ZW0gb2YgdGhpcy5hc2NMaXN0XCIgY2RrRHJhZz57e2l0ZW19fTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L3NlY3Rpb24+XHJcblxyXG4gICAgICAgICAgICA8IS0tIGRlc2Mgc2VjdGlvbiAtLT5cclxuICAgICAgICAgICAgPHNlY3Rpb24gY2xhc3M9XCJleGFtcGxlLWNvbnRhaW5lclwiXHJcbiAgICAgICAgICAgICAgICBmeExheW91dD1cImNvbHVtblwiXHJcbiAgICAgICAgICAgICAgICBmeExheW91dEFsaWduPVwic3RhcnQgY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgyPmRlc2M8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2RrRHJvcExpc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgI2Rlc2NEcm9wTGlzdD1cImNka0Ryb3BMaXN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgW2Nka0Ryb3BMaXN0RGF0YV09XCJ0aGlzLmRlc2NMaXN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgW2Nka0Ryb3BMaXN0Q29ubmVjdGVkVG9dPVwiW2NyaXRlcmlhRHJvcExpc3QsIGFzY0Ryb3BMaXN0XVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZXhhbXBsZS1saXN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGNka0Ryb3BMaXN0RHJvcHBlZCk9XCJkcm9wKCRldmVudClcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImV4YW1wbGUtYm94XCIgKm5nRm9yPVwibGV0IGl0ZW0gb2YgdGhpcy5kZXNjTGlzdFwiIGNka0RyYWc+e3tpdGVtfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9zZWN0aW9uPlxyXG4gICAgICAgICAgICBcclxuICAgIDwvc2VjdGlvbj5cclxuXHJcbiAgICA8bmctY29udGFpbmVyICpuZ0lmPVwie1xyXG4gICAgICAgICAgICBoYXNGZXRjaGVkRGF0YTogdGhpcy5hdXRvY29tcGxldGUucGFnZURhdGFTb3VyY2UuZ2V0SGFzRmV0Y2hlZERhdGFTdWJqZWN0KCkgfCBhc3luYyxcclxuICAgICAgICAgICAgaGFzRGF0YVRvU2hvdzogdGhpcy5hdXRvY29tcGxldGUucGFnZURhdGFTb3VyY2UuZ2V0SGFzRGF0YVRvU2hvd1N1YmplY3QoKSB8IGFzeW5jLFxyXG4gICAgICAgICAgICBpc0xvYWRpbmc6IHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLmlzTG9hZGluZygpIHwgYXN5bmMsXHJcbiAgICAgICAgICAgIHRvdGFsSXRlbXM6IHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlLmdldFRvdGFsSXRlbXNTdWJqZWN0KCkgfCBhc3luY1xyXG4gICAgICAgIH0gYXMgb2JzZXJ2YWJsZXNcIj5cclxuXHJcbiAgICAgICAgICAgIDxtYXQtcHJvZ3Jlc3MtYmFyIFxyXG4gICAgICAgICAgICAgICAgbW9kZT1cImluZGV0ZXJtaW5hdGVcIlxyXG4gICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNob3dTY3JvbGxWaWV3ICYmICFvYnNlcnZhYmxlcy5oYXNGZXRjaGVkRGF0YVwiPjwvbWF0LXByb2dyZXNzLWJhcj5cclxuXHJcbiAgICAgICAgICAgIDwhLS0gW2l0ZW1TaXplXSAtPiBUaGUgc2l6ZSBvZiB0aGUgaXRlbXMgaW4gdGhlIGxpc3QgKGluIHBpeGVscykuIC0tPlxyXG4gICAgICAgICAgICA8Y2RrLXZpcnR1YWwtc2Nyb2xsLXZpZXdwb3J0IFxyXG4gICAgICAgICAgICAgICAgKm5nSWY9XCJ0aGlzLnNob3dTY3JvbGxWaWV3ICYmIG9ic2VydmFibGVzLmhhc0ZldGNoZWREYXRhICYmIG9ic2VydmFibGVzLmhhc0RhdGFUb1Nob3dcIlxyXG4gICAgICAgICAgICAgICAgYXBwT3BhY2l0eVRyYW5zaXRpb25cclxuICAgICAgICAgICAgICAgIFthcHBPcGFjaXR5RHVyYXRpb25dPVwiMVwiXHJcbiAgICAgICAgICAgICAgICBbaXRlbVNpemVdPVwib2JzZXJ2YWJsZXMudG90YWxJdGVtc1wiIFxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJteS12aWV3cG9ydFwiXHJcbiAgICAgICAgICAgICAgICBhcHBIZWlnaHRcclxuICAgICAgICAgICAgICAgIFtoZWlnaHRdPVwib2JzZXJ2YWJsZXMudG90YWxJdGVtcyAqIDYwID4gMjAwID8gMjAwIDogb2JzZXJ2YWJsZXMudG90YWxJdGVtcyAqIDYwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG1hdC1saXN0IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb2xlPVwibGlzdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPG1hdC1saXN0LWl0ZW0gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9sZT1cImxpc3RpdGVtXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqY2RrVmlydHVhbEZvcj1cImxldCBpdGVtIG9mIHRoaXMuYXV0b2NvbXBsZXRlLnBhZ2VEYXRhU291cmNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgaW5kZXggPSBpbmRleDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvdW50ID0gY291bnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBmaXJzdCA9IGZpcnN0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgbGFzdCA9IGxhc3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBldmVuID0gZXZlbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG9kZCA9IG9kZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVDYWNoZVNpemU6IDBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZ4TGF5b3V0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnhMYXlvdXRBbGlnbj1cInN0YXJ0IGNlbnRlclwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cInRoaXMuaXRlbVNlbGVjdGVkKGl0ZW0pXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxoMiAqbmdJZj1cIiFvYnNlcnZhYmxlcy5pc0xvYWRpbmcgJiYgaXRlbVwiPnt7aXRlbS5nZXRDb250ZXh0VG9TaG93KCl9fTwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxtYXQtc3Bpbm5lciAqbmdJZj1cIm9ic2VydmFibGVzLmlzTG9hZGluZ1wiIFtkaWFtZXRlcl09XCIyMFwiPjwvbWF0LXNwaW5uZXI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L21hdC1saXN0LWl0ZW0+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9tYXQtbGlzdD5cclxuICAgICAgICAgICAgPC9jZGstdmlydHVhbC1zY3JvbGwtdmlld3BvcnQ+XHJcblxyXG4gICAgICAgICAgICA8aDIgXHJcbiAgICAgICAgICAgICAgICAqbmdJZj1cInRoaXMuc2hvd1Njcm9sbFZpZXcgJiYgb2JzZXJ2YWJsZXMuaGFzRmV0Y2hlZERhdGEgJiYgIW9ic2VydmFibGVzLmhhc0RhdGFUb1Nob3dcIlxyXG4gICAgICAgICAgICAgICAgYXBwT3BhY2l0eVRyYW5zaXRpb25cclxuICAgICAgICAgICAgICAgIFthcHBPcGFjaXR5RHVyYXRpb25dPVwiMVwiPk5vIGRhdGEgdG8gZGlzcGxheTwvaDI+XHJcblxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcbiAgICBcclxuPC9zZWN0aW9uPiJdfQ==