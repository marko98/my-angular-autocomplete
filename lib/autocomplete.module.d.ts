import * as i0 from "@angular/core";
import * as i1 from "./autocomplete.component";
import * as i2 from "@angular/platform-browser";
import * as i3 from "@angular/platform-browser/animations";
import * as i4 from "@angular/common/http";
import * as i5 from "./shared/shared.module";
export declare class AutocompleteModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<AutocompleteModule, [typeof i1.AutocompleteComponent], [typeof i2.BrowserModule, typeof i3.BrowserAnimationsModule, typeof i4.HttpClientModule, typeof i5.SharedModule], [typeof i1.AutocompleteComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<AutocompleteModule>;
}
