import { SortParam, PageKey, Page } from '../pageable-read.service';
import { Observable as CustomObservable } from '../../pattern/behavioural/observer/observable.model';
import { Observable } from 'rxjs';
import { Entitet } from '../../model/interface/entitet.interface';
import { ENTITET_MODEL_INTERFACE } from '../../model/interface/entitet.model-interface';
export declare interface PageableReadServiceInterface<T extends Entitet<T, ID>, ID> extends CustomObservable {
    findPageWithPageNumber(page: number): void;
    findPageWithPageNumberAndSize(page: number, pageSize: number): Observable<Page<T, ID>>;
    findPageWithPageNumberAndSizeAndSort(page: number, pageSize: number, sortParams: SortParam[], criteriaValue?: string): Observable<Page<T, ID>>;
    getPages(): Map<PageKey, Page<T, ID>>;
    getPageSize(): number;
    setPageSize(pageSize: number): any;
    buildEntitet(e: ENTITET_MODEL_INTERFACE): Entitet<T, ID>;
    setPath(path: string): void;
    getPath(): string;
    setCriteriaPath(criteriaPath: string): void;
    getCriteriaPath(): string;
    resetPathAndCriteriaPathToDefault(): void;
}
