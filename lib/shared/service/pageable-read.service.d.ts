import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PageableReadServiceInterface } from './interface/pageable-read-service.interface';
import { EntitetFactory } from '../pattern/creational/factory/entitet-factory.interface';
import { Entitet } from '../model/interface/entitet.interface';
import { ENTITET_MODEL_INTERFACE } from '../model/interface/entitet.model-interface';
import { PageMap } from '../pattern/structural/decorator/pagemap.decorator';
import { Observable as CustomObservebale } from '../pattern/behavioural/observer/observable.model';
export declare interface Page<T extends Entitet<T, ID>, ID> {
    content: ENTITET_MODEL_INTERFACE[];
    empty: boolean;
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort;
    totalElements: number;
    totalPages: number;
}
export declare interface Pageable {
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    sort: Sort;
    unpaged: boolean;
}
export declare interface Sort {
    empty: boolean;
    sorted: boolean;
    unsorted: boolean;
}
export declare interface SortParam {
    field: string;
    order: 'asc' | 'desc';
}
export declare interface PageKey {
    pageNumber: number;
    pageSize: number;
    sort: Sort;
}
export declare abstract class PageableReadService<T extends Entitet<T, ID>, ID> extends CustomObservebale implements PageableReadServiceInterface<T, ID> {
    protected httpClient: HttpClient;
    protected factory: EntitetFactory<T, ID>;
    protected pageSize: number;
    protected url: string;
    protected path: string;
    private _defaultPath;
    protected criteriaPath: string;
    private _defaultCriteriaPath;
    protected pages: PageMap<T, ID>;
    constructor(httpClient: HttpClient, schema: string, host: string, port: string | number, defaultPath: string, defaultCriteriaPath: string, factory: EntitetFactory<T, ID>, pageSize?: number);
    findPageWithPageNumber: (page?: number) => void;
    findPageWithPageNumberAndSize: (page?: number, pageSize?: number, criteriaValue?: string) => Observable<Page<T, ID>>;
    findPageWithPageNumberAndSizeAndSort: (page?: number, pageSize?: number, sortParams?: SortParam[], criteriaValue?: string) => Observable<Page<T, ID>>;
    protected showError: (err: any) => void;
    getPages: () => Map<PageKey, Page<T, ID>>;
    private _addPage;
    getPageSize: () => number;
    setPageSize: (pageSize: number) => void;
    setPath: (path: string) => void;
    getPath: () => string;
    setCriteriaPath: (criteriaPath: string) => void;
    getCriteriaPath: () => string;
    resetPathAndCriteriaPathToDefault: () => void;
    buildEntitet: (e: ENTITET_MODEL_INTERFACE) => Entitet<T, ID>;
}
