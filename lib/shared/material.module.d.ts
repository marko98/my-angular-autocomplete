import * as i0 from "@angular/core";
import * as i1 from "@angular/material/form-field";
import * as i2 from "@angular/material/autocomplete";
import * as i3 from "@angular/material/slide-toggle";
import * as i4 from "@angular/material/input";
import * as i5 from "@angular/material/list";
import * as i6 from "@angular/material/progress-spinner";
import * as i7 from "@angular/material/progress-bar";
export declare class MaterialModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MaterialModule, never, [typeof i1.MatFormFieldModule, typeof i2.MatAutocompleteModule, typeof i3.MatSlideToggleModule, typeof i4.MatInputModule, typeof i5.MatListModule, typeof i6.MatProgressSpinnerModule, typeof i7.MatProgressBarModule], [typeof i1.MatFormFieldModule, typeof i2.MatAutocompleteModule, typeof i3.MatSlideToggleModule, typeof i4.MatInputModule, typeof i5.MatListModule, typeof i6.MatProgressSpinnerModule, typeof i7.MatProgressBarModule]>;
    static ɵinj: i0.ɵɵInjectorDef<MaterialModule>;
}
