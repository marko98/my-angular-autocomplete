import { ElementRef, AfterViewInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class HeightDirective implements AfterViewInit {
    private elRef;
    /**
     * @height - u px
     */
    height: number;
    constructor(elRef: ElementRef);
    ngAfterViewInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<HeightDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDefWithMeta<HeightDirective, "[appHeight]", never, { "height": "height"; }, {}, never>;
}
