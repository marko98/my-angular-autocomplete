import { ElementRef, AfterViewInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class OpacityTransitionDirective implements AfterViewInit {
    private _el;
    appOpacityDuration: number;
    private _timeline;
    constructor(_el: ElementRef);
    ngAfterViewInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<OpacityTransitionDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDefWithMeta<OpacityTransitionDirective, "[appOpacityTransition]", never, { "appOpacityDuration": "appOpacityDuration"; }, {}, never>;
}
