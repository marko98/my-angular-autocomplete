import { AfterViewInit, ElementRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class WidthDirective implements AfterViewInit {
    private elRef;
    /**
     * @width - u vw
     */
    width: number;
    constructor(elRef: ElementRef);
    ngAfterViewInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<WidthDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDefWithMeta<WidthDirective, "[appWidth]", never, { "width": "width"; }, {}, never>;
}
