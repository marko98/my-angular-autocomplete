import { ENTITET_MODEL_INTERFACE } from '../../../model/interface/entitet.model-interface';
import { Entitet } from '../../../model/interface/entitet.interface';
export declare interface EntitetFactory<T, ID> {
    build(entitetModelInterface?: ENTITET_MODEL_INTERFACE): Entitet<T, ID>;
}
