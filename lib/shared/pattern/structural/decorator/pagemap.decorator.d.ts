import { Entitet } from '../../../model/interface/entitet.interface';
import { Prototype } from '../../creational/prototype/prototype.interface';
import { PageKey, Page } from '../../../service/pageable-read.service';
export declare class PageMap<T extends Entitet<T, ID>, ID> implements Prototype {
    private map;
    /**
     * prva implementacija metode nikada ne prazni mapu iako se promeni kriterijum za
     * dobavljanje entiteta
     */
    /**
     * druga implementacija metode prazni mapu ako dodje do promene kriterijuma za dobavljanje entiteta
     */
    has: (key: PageKey) => boolean;
    private _hasCriteriaChanged;
    getMap: () => Map<PageKey, Page<T, ID>>;
    clone: () => Map<PageKey, Page<T, ID>>;
    clear: () => void;
}
