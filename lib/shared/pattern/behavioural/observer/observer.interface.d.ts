export declare interface Observer {
    update(): void;
}
