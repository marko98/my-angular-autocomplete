import { Observer } from './observer.interface';
export declare abstract class Observable {
    protected observers: Set<Observer | Function>;
    attach: (observer: Function | Observer) => void;
    dettach: (observer: Function | Observer) => void;
    protected notify: () => void;
}
