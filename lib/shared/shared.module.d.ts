import * as i0 from "@angular/core";
import * as i1 from "./directive/height.directive";
import * as i2 from "./directive/width.directive";
import * as i3 from "./directive/opacity-transition.directive";
import * as i4 from "@angular/common";
import * as i5 from "@angular/forms";
import * as i6 from "./material.module";
import * as i7 from "@angular/flex-layout";
import * as i8 from "@angular/cdk/scrolling";
import * as i9 from "@angular/cdk/drag-drop";
export declare class SharedModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<SharedModule, [typeof i1.HeightDirective, typeof i2.WidthDirective, typeof i3.OpacityTransitionDirective], [typeof i4.CommonModule, typeof i5.FormsModule, typeof i5.ReactiveFormsModule, typeof i6.MaterialModule, typeof i7.FlexLayoutModule, typeof i8.ScrollingModule, typeof i9.DragDropModule], [typeof i1.HeightDirective, typeof i2.WidthDirective, typeof i3.OpacityTransitionDirective, typeof i4.CommonModule, typeof i5.FormsModule, typeof i5.ReactiveFormsModule, typeof i6.MaterialModule, typeof i7.FlexLayoutModule, typeof i8.ScrollingModule, typeof i9.DragDropModule]>;
    static ɵinj: i0.ɵɵInjectorDef<SharedModule>;
}
