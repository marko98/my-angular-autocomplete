import { PageDataSource } from '../page-data-source.model';
import { Entitet } from './entitet.interface';
import { ENTITET_MODEL_INTERFACE } from './entitet.model-interface';
import { OptionItem } from './option-item.interface';
import { Subject, BehaviorSubject } from 'rxjs';
export declare interface AutocompleteInterface {
    pageDataSource: PageDataSource<Entitet<any, any>, any, ENTITET_MODEL_INTERFACE>;
    criteria: string;
    disabled?: boolean;
    appearance?: 'legacy' | 'standard' | 'fill' | 'outline';
    sortingOptions: string[];
    inputPlaceHolder: string;
    valueSubject: Subject<OptionItem>;
    value?: OptionItem;
    hiddenSubject: BehaviorSubject<boolean>;
}
