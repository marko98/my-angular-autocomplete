import { Identifikacija } from './identifikacija.interface';
import { OptionItem } from './option-item.interface';
export declare interface Entitet<T, ID> extends Identifikacija<ID>, OptionItem {
}
