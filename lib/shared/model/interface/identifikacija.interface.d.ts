export declare interface Identifikacija<ID> {
    getId(): ID;
    setId(id: ID): void;
}
