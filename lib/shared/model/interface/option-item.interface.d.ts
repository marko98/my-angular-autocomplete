import { Entitet } from './entitet.interface';
export declare interface OptionItem {
    getContextToShow(): string;
    getEntitet(): Entitet<any, any>;
}
