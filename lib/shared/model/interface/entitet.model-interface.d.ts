import { StanjeModela } from '../enum/stanje_modela.enum';
export declare interface ENTITET_MODEL_INTERFACE {
    id?: number;
    stanje?: StanjeModela;
    version?: number;
}
