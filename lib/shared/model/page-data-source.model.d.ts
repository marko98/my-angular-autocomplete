import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { BehaviorSubject, Subscription, Observable, Subject } from 'rxjs';
import { SortParam } from '../service/pageable-read.service';
import { OptionItem } from './interface/option-item.interface';
import { Entitet } from './interface/entitet.interface';
import { ENTITET_MODEL_INTERFACE } from './interface/entitet.model-interface';
import { PageableReadServiceInterface } from '../service/interface/pageable-read-service.interface';
export declare class PageDataSource<T extends Entitet<T, ID>, ID, G extends ENTITET_MODEL_INTERFACE> extends DataSource<OptionItem> {
    protected pageableCrudService: PageableReadServiceInterface<T, ID>;
    protected _pageSize: number;
    protected _cachedData: OptionItem[];
    protected _fetchedPages: Set<number>;
    protected _dataStream: BehaviorSubject<OptionItem[]>;
    protected _sentRequests: number;
    protected _subscription: Subscription;
    private _hasDataToShowSubject;
    private _isLoadingSubject;
    private _hasFetchedDataSubject;
    private _totalItemsSubject;
    private _sortParams;
    /**
     * @param pageableCrudService posto ce nam trebati service(tipa PageableCrudService) za dobavljanje
     * stranica zahtevamo ga kao argument u konstruktoru
     *
     * @param _pageSize koliko item-a zelimo da prikazemo po stranici
     */
    constructor(pageableCrudService: PageableReadServiceInterface<T, ID>, _pageSize?: number);
    connect(collectionViewer: CollectionViewer): Observable<OptionItem[]>;
    disconnect(): void;
    myDisconnect(): void;
    protected _getPageForIndex(index: number): number;
    protected _fetchPage(pageNumber: number, criteria?: string): void;
    isLoading: () => Subject<boolean>;
    getHasDataToShowSubject: () => Subject<boolean>;
    getHasFetchedDataSubject: () => Subject<boolean>;
    getTotalItemsSubject: () => Subject<number>;
    changePath: (path: string) => void;
    changeCriteriaPath: (criteriaPath: string) => void;
    resetAndFetch: (criteria: string, sortParams?: SortParam[], pageSize?: number) => void;
}
