(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('gsap'), require('@angular/cdk/collections'), require('rxjs'), require('@angular/common/http'), require('@angular/cdk/drag-drop'), require('@angular/flex-layout/flex'), require('@angular/material/form-field'), require('@angular/material/input'), require('@angular/forms'), require('@angular/common'), require('@angular/material/slide-toggle'), require('@angular/material/progress-bar'), require('@angular/cdk/scrolling'), require('@angular/material/list'), require('@angular/material/progress-spinner'), require('@angular/flex-layout'), require('@angular/material/autocomplete'), require('@angular/platform-browser'), require('@angular/platform-browser/animations')) :
    typeof define === 'function' && define.amd ? define('autocomplete', ['exports', '@angular/core', 'gsap', '@angular/cdk/collections', 'rxjs', '@angular/common/http', '@angular/cdk/drag-drop', '@angular/flex-layout/flex', '@angular/material/form-field', '@angular/material/input', '@angular/forms', '@angular/common', '@angular/material/slide-toggle', '@angular/material/progress-bar', '@angular/cdk/scrolling', '@angular/material/list', '@angular/material/progress-spinner', '@angular/flex-layout', '@angular/material/autocomplete', '@angular/platform-browser', '@angular/platform-browser/animations'], factory) :
    (global = global || self, factory(global.autocomplete = {}, global.ng.core, global.gsap, global.ng.cdk.collections, global.rxjs, global.ng.common.http, global.ng.cdk.dragDrop, global.ng.flexLayout.flex, global.ng.material.formField, global.ng.material.input, global.ng.forms, global.ng.common, global.ng.material.slideToggle, global.ng.material.progressBar, global.ng.cdk.scrolling, global.ng.material.list, global.ng.material.progressSpinner, global.ng.flexLayout, global.ng.material.autocomplete, global.ng.platformBrowser, global.ng.platformBrowser.animations));
}(this, (function (exports, core, gsap, collections, rxjs, http, dragDrop, flex, formField, input, forms, common, slideToggle, progressBar, scrolling, list, progressSpinner, flexLayout, autocomplete, platformBrowser, animations) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var HeightDirective = /** @class */ (function () {
        function HeightDirective(elRef) {
            this.elRef = elRef;
        }
        HeightDirective.prototype.ngAfterViewInit = function () {
            this.elRef.nativeElement.style.height = this.height + 'px';
        };
        HeightDirective.ɵfac = function HeightDirective_Factory(t) { return new (t || HeightDirective)(core.ɵɵdirectiveInject(core.ElementRef)); };
        HeightDirective.ɵdir = core.ɵɵdefineDirective({ type: HeightDirective, selectors: [["", "appHeight", ""]], inputs: { height: "height" } });
        return HeightDirective;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(HeightDirective, [{
            type: core.Directive,
            args: [{
                    selector: '[appHeight]',
                }]
        }], function () { return [{ type: core.ElementRef }]; }, { height: [{
                type: core.Input
            }] }); })();

    var WidthDirective = /** @class */ (function () {
        function WidthDirective(elRef) {
            this.elRef = elRef;
        }
        WidthDirective.prototype.ngAfterViewInit = function () {
            this.elRef.nativeElement.style.width = this.width + 'vw';
        };
        WidthDirective.ɵfac = function WidthDirective_Factory(t) { return new (t || WidthDirective)(core.ɵɵdirectiveInject(core.ElementRef)); };
        WidthDirective.ɵdir = core.ɵɵdefineDirective({ type: WidthDirective, selectors: [["", "appWidth", ""]], inputs: { width: "width" } });
        return WidthDirective;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(WidthDirective, [{
            type: core.Directive,
            args: [{
                    selector: '[appWidth]',
                }]
        }], function () { return [{ type: core.ElementRef }]; }, { width: [{
                type: core.Input
            }] }); })();

    var OpacityTransitionDirective = /** @class */ (function () {
        function OpacityTransitionDirective(_el) {
            this._el = _el;
            this.appOpacityDuration = 1;
            this._timeline = new gsap.TimelineMax();
        }
        OpacityTransitionDirective.prototype.ngAfterViewInit = function () {
            this._timeline.fromTo(this._el.nativeElement, this.appOpacityDuration, { opacity: 0 }, {
                opacity: 1,
                onComplete: function () {
                    // console.log('animacija gotova :)');
                },
            });
        };
        OpacityTransitionDirective.ɵfac = function OpacityTransitionDirective_Factory(t) { return new (t || OpacityTransitionDirective)(core.ɵɵdirectiveInject(core.ElementRef)); };
        OpacityTransitionDirective.ɵdir = core.ɵɵdefineDirective({ type: OpacityTransitionDirective, selectors: [["", "appOpacityTransition", ""]], inputs: { appOpacityDuration: "appOpacityDuration" } });
        return OpacityTransitionDirective;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(OpacityTransitionDirective, [{
            type: core.Directive,
            args: [{
                    selector: '[appOpacityTransition]',
                }]
        }], function () { return [{ type: core.ElementRef }]; }, { appOpacityDuration: [{
                type: core.Input
            }] }); })();


    (function (StanjeModela) {
        StanjeModela["AKTIVAN"] = "AKTIVAN";
        StanjeModela["NEAKTIVAN"] = "NEAKTIVAN";
        StanjeModela["OBRISAN"] = "OBRISAN";
    })(exports.StanjeModela || (exports.StanjeModela = {}));

    var PageDataSource = /** @class */ (function (_super) {
        __extends(PageDataSource, _super);
        /**
         * @param pageableCrudService posto ce nam trebati service(tipa PageableCrudService) za dobavljanje
         * stranica zahtevamo ga kao argument u konstruktoru
         *
         * @param _pageSize koliko item-a zelimo da prikazemo po stranici
         */
        function PageDataSource(pageableCrudService, _pageSize) {
            if (_pageSize === void 0) { _pageSize = 10; }
            var _this = _super.call(this) || this;
            _this.pageableCrudService = pageableCrudService;
            _this._pageSize = _pageSize;
            _this._cachedData = [];
            _this._fetchedPages = new Set(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
            _this._dataStream = new rxjs.BehaviorSubject(_this._cachedData);
            _this._sentRequests = 0;
            _this._subscription = new rxjs.Subscription();
            _this._hasDataToShowSubject = new rxjs.Subject();
            _this._isLoadingSubject = new rxjs.Subject();
            _this._hasFetchedDataSubject = new rxjs.Subject();
            _this._totalItemsSubject = new rxjs.Subject();
            _this._sortParams = [];
            _this.isLoading = function () {
                return _this._isLoadingSubject;
            };
            _this.getHasDataToShowSubject = function () {
                return _this._hasDataToShowSubject;
            };
            _this.getHasFetchedDataSubject = function () {
                return _this._hasFetchedDataSubject;
            };
            _this.getTotalItemsSubject = function () {
                return _this._totalItemsSubject;
            };
            _this.changePath = function (path) {
                _this.pageableCrudService.setPath(path);
            };
            _this.changeCriteriaPath = function (criteriaPath) {
                _this.pageableCrudService.setCriteriaPath(criteriaPath);
            };
            _this.resetAndFetch = function (criteria, sortParams, pageSize) {
                if (pageSize)
                    _this._pageSize = pageSize;
                if (sortParams)
                    _this._sortParams = sortParams;
                _this._cachedData.splice(0, _this._cachedData.length);
                _this._fetchedPages.clear(); // skup brojeva koji predstavljaju brojeve dohvacenih stranica
                _this._dataStream.next(_this._cachedData);
                _this._hasFetchedDataSubject.next(false);
                // console.log(criteria, this._pageSize);
                _this._fetchPage(0, criteria);
            };
            return _this;
            // this._fetchPage(0);
        }
        PageDataSource.prototype.connect = function (collectionViewer) {
            var _this = this;
            this._subscription.add(collectionViewer.viewChange.subscribe(function (range) {
                // console.log('Range: ', range);
                var startPage = _this._getPageForIndex(range.start);
                // console.log('Start page: ', startPage);
                var endPage = _this._getPageForIndex(range.end - 1);
                // console.log('End page: ', endPage);
                for (var pageNumber = startPage; pageNumber <= endPage; pageNumber++) {
                    // console.log('pageNumber: ', pageNumber);
                    _this._fetchPage(pageNumber);
                }
            }));
            return this._dataStream;
        };
        PageDataSource.prototype.disconnect = function () {
            // console.log(
            //   'disconnected, obrati paznju na *ngIf nad cdk-virtual-scroll-viewport diskonektovaces se, koristi hidden ili napisi svoju metodu za diskonektovanje'
            // );
            // this._subscription.unsubscribe();
        };
        PageDataSource.prototype.myDisconnect = function () {
            this._subscription.unsubscribe();
        };
        PageDataSource.prototype._getPageForIndex = function (index) {
            return Math.floor(index / this._pageSize);
        };
        PageDataSource.prototype._fetchPage = function (pageNumber, criteria) {
            var _this = this;
            if (this._fetchedPages.has(pageNumber)) {
                return;
            }
            // console.log(
            //   'Fetching a next page, page number: ',
            //   pageNumber,
            //   ', criteria: ',
            //   criteria
            // );
            this._fetchedPages.add(pageNumber);
            this._sentRequests += 1;
            // console.log('zahtevi na cekanju: ', this._sentRequests);
            this._isLoadingSubject.next(this._sentRequests > 0);
            this.pageableCrudService
                .findPageWithPageNumberAndSizeAndSort(pageNumber, this._pageSize, this._sortParams, criteria)
                .subscribe(function (page) {
                // console.log(page);
                // trenutno tu da bismo napravili loading
                setTimeout(function () {
                    var _a;
                    if (!(_this._cachedData.length > 0)) {
                        _this._cachedData = Array.from({
                            length: page.totalElements,
                        });
                        _this._hasFetchedDataSubject.next(true);
                        _this._hasDataToShowSubject.next(page.totalElements > 0);
                        _this._totalItemsSubject.next(page.totalElements);
                    }
                    if (page.content) {
                        (_a = _this._cachedData).splice.apply(_a, __spread([pageNumber * _this._pageSize,
                            _this._pageSize], page.content.map(function (g) {
                            return _this.pageableCrudService.buildEntitet(g);
                        })));
                    }
                    _this._dataStream.next(_this._cachedData);
                    // console.log(this._cachedData);
                    _this._sentRequests -= 1;
                    // console.log('zahtevi na cekanju: ', this._sentRequests);
                    _this._isLoadingSubject.next(_this._sentRequests > 0);
                }, 200);
            }, function (err) {
                _this._hasFetchedDataSubject.next(true);
                console.log(err);
                _this._sentRequests -= 1;
                // console.log('zahtevi na cekanju: ', this._sentRequests);
                _this._isLoadingSubject.next(_this._sentRequests > 0);
            }, function () {
                // completed subscription
            });
        };
        return PageDataSource;
    }(collections.DataSource));

    /*
        observers nisu nista drugo do same funkcije koja treba da se pozove kada nad
        objektom tipa Observable na koju smo subscribe-ovani dodje do promena

        primer observer-a:

    export class AppComponent implements OnInit, OnDestroy {

        articleObserver = (observable: ArticleRepository): void => {
            console.log(observable);
            this.articles = observable.getArticles();
        }

        // interfaces
        ngOnInit() {
            this.articleRepository.attach(this.articleObserver);

            this.articles = this.articleRepository.getArticles();
            console.log("AppComponent init");
        }

        ngOnDestroy() {
            if (this.articleRepository)
                this.articleRepository.dettach(this.articleObserver);
            console.log("AppComponent destroyed");
        }
    }
    */
    var Observable = /** @class */ (function () {
        function Observable() {
            var _this = this;
            this.observers = new Set();
            this.attach = function (observer) {
                if (!_this.observers.has(observer))
                    _this.observers.add(observer);
            };
            this.dettach = function (observer) {
                if (_this.observers.has(observer))
                    _this.observers.delete(observer);
            };
            this.notify = function () {
                var e_1, _a;
                try {
                    for (var _b = __values(_this.observers), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var observer = _c.value;
                        // ne znam bas koliko ima smisla
                        if (observer) {
                            try {
                                observer.update();
                            }
                            catch (error) {
                                observer();
                            }
                        }
                        else
                            _this.dettach(observer);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            };
        }
        return Observable;
    }());

    var PageMap = /** @class */ (function () {
        function PageMap() {
            var _this = this;
            this.map = new Map();
            /**
             * prva implementacija metode nikada ne prazni mapu iako se promeni kriterijum za
             * dobavljanje entiteta
             */
            // has = (key: PageKey): boolean => {
            //   // iteriramo kroz sve elemente
            //   for (const [existingKey, value] of this.map) {
            //     if (
            //       existingKey.pageNumber === key.pageNumber &&
            //       existingKey.pageSize === key.pageSize &&
            //       existingKey.sort.empty === key.sort.empty &&
            //       existingKey.sort.sorted === key.sort.sorted &&
            //       existingKey.sort.unsorted === key.sort.unsorted
            //     )
            //       return true;
            //   }
            //   return false;
            // };
            /**
             * druga implementacija metode prazni mapu ako dodje do promene kriterijuma za dobavljanje entiteta
             */
            this.has = function (key) {
                var e_1, _a;
                if (!_this._hasCriteriaChanged(key)) {
                    try {
                        for (var _b = __values(_this.map), _c = _b.next(); !_c.done; _c = _b.next()) {
                            var _d = __read(_c.value, 2), existingKey = _d[0], value = _d[1];
                            if (existingKey.pageNumber === key.pageNumber)
                                return true;
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                }
                else {
                    _this.map.clear();
                    return false;
                }
            };
            this._hasCriteriaChanged = function (key) {
                var e_2, _a;
                try {
                    for (var _b = __values(_this.map), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var _d = __read(_c.value, 2), existingKey = _d[0], value = _d[1];
                        if (existingKey.pageSize === key.pageSize &&
                            existingKey.sort.empty === key.sort.empty &&
                            existingKey.sort.sorted === key.sort.sorted &&
                            existingKey.sort.unsorted === key.sort.unsorted)
                            return false;
                        else
                            return true;
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            };
            this.getMap = function () {
                return _this.map;
            };
            this.clone = function () {
                // vracamo clone
                return new Map(_this.map);
            };
            this.clear = function () {
                _this.map.clear();
            };
        }
        return PageMap;
    }());

    var AutocompleteService = /** @class */ (function () {
        function AutocompleteService() {
        }
        AutocompleteService.ɵfac = function AutocompleteService_Factory(t) { return new (t || AutocompleteService)(); };
        AutocompleteService.ɵprov = core.ɵɵdefineInjectable({ token: AutocompleteService, factory: AutocompleteService.ɵfac, providedIn: 'root' });
        return AutocompleteService;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(AutocompleteService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return []; }, null); })();

    var PageableReadService = /** @class */ (function (_super) {
        __extends(PageableReadService, _super);
        function PageableReadService(httpClient, schema, host, port, defaultPath, defaultCriteriaPath, factory, pageSize) {
            if (pageSize === void 0) { pageSize = 20; }
            var _this = _super.call(this) || this;
            _this.httpClient = httpClient;
            _this.factory = factory;
            _this.pageSize = pageSize;
            _this.pages = new PageMap();
            // ----------------- interface PageableCrudService -----------------
            //   read pages
            _this.findPageWithPageNumber = function (page) {
                if (page === void 0) { page = 0; }
                var url = _this.url + _this.path;
                _this.httpClient
                    .get(url, {
                    params: new http.HttpParams()
                        .set('page', page.toString())
                        .set('size', _this.pageSize.toString()),
                })
                    .subscribe(function (page) {
                    if (_this._addPage(page))
                        _this.notify();
                }, function (err) {
                    _this.showError(err);
                });
            };
            //   read pages
            _this.findPageWithPageNumberAndSize = function (page, pageSize, criteriaValue) {
                if (page === void 0) { page = 0; }
                if (pageSize === void 0) { pageSize = 20; }
                var url = _this.url + _this.path;
                if (criteriaValue &&
                    criteriaValue !== '' &&
                    criteriaValue.replace(/\s/g, ''))
                    url = _this.url + _this.criteriaPath + criteriaValue;
                var observable = _this.httpClient.get(url, {
                    params: new http.HttpParams()
                        .set('page', page.toString())
                        .set('size', pageSize.toString()),
                });
                observable.subscribe(function (page) {
                    if (_this._addPage(page))
                        _this.notify();
                }, function (err) {
                    _this.showError(err);
                });
                return observable;
            };
            // read pages
            _this.findPageWithPageNumberAndSizeAndSort = function (page, pageSize, sortParams, criteriaValue) {
                if (page === void 0) { page = 0; }
                if (pageSize === void 0) { pageSize = 20; }
                if (sortParams === void 0) { sortParams = []; }
                var url = _this.url + _this.path;
                if (criteriaValue &&
                    criteriaValue !== '' &&
                    criteriaValue.replace(/\s/g, ''))
                    url = _this.url + _this.criteriaPath + criteriaValue;
                var httpParams = new http.HttpParams()
                    .set('page', page.toString())
                    .set('size', pageSize.toString());
                sortParams.forEach(function (sortParam) {
                    httpParams = httpParams.append('sort', sortParam.field + ',' + sortParam.order);
                });
                var observable = _this.httpClient.get(url, {
                    params: httpParams,
                });
                observable.subscribe(function (page) {
                    if (_this._addPage(page))
                        _this.notify();
                }, function (err) {
                    _this.showError(err);
                });
                return observable;
            };
            _this.showError = function (err) {
                console.log(err);
            };
            _this.getPages = function () {
                // vracamo clone
                return _this.pages.clone();
            };
            _this._addPage = function (page) {
                if (!_this.pages.has({
                    pageNumber: page.pageable.pageNumber,
                    pageSize: page.pageable.pageSize,
                    sort: page.pageable.sort,
                })) {
                    _this.pages.getMap().set({
                        pageNumber: page.pageable.pageNumber,
                        pageSize: page.pageable.pageSize,
                        sort: page.pageable.sort,
                    }, page);
                    return true;
                }
                return false;
            };
            _this.getPageSize = function () {
                return _this.pageSize;
            };
            _this.setPageSize = function (pageSize) {
                _this.pageSize = pageSize;
            };
            _this.setPath = function (path) {
                _this.path = path;
            };
            _this.getPath = function () {
                return _this.path;
            };
            _this.setCriteriaPath = function (criteriaPath) {
                _this.criteriaPath = criteriaPath;
            };
            _this.getCriteriaPath = function () {
                return _this.criteriaPath;
            };
            _this.resetPathAndCriteriaPathToDefault = function () {
                _this.path = _this._defaultPath;
                _this.criteriaPath = _this._defaultCriteriaPath;
            };
            _this.buildEntitet = function (e) {
                return _this.factory.build(e);
            };
            _this.url = schema + '://' + host + ':' + port.toString() + '/';
            _this._defaultPath = defaultPath;
            _this.path = defaultPath;
            _this._defaultCriteriaPath = defaultCriteriaPath;
            _this.criteriaPath = defaultCriteriaPath;
            return _this;
        }
        return PageableReadService;
    }(Observable));

    function AutocompleteComponent_mat_slide_toggle_8_Template(rf, ctx) { if (rf & 1) {
        var _r6 = core.ɵɵgetCurrentView();
        core.ɵɵelementStart(0, "mat-slide-toggle", 8);
        core.ɵɵlistener("change", function AutocompleteComponent_mat_slide_toggle_8_Template_mat_slide_toggle_change_0_listener($event) { core.ɵɵrestoreView(_r6); var ctx_r5 = core.ɵɵnextContext(); return ctx_r5.onSlideToggleChange($event); });
        core.ɵɵtext(1, "sort?");
        core.ɵɵelementEnd();
    } if (rf & 2) {
        core.ɵɵproperty("appOpacityDuration", 1)("color", "primary")("checked", false)("disabled", false);
    } }
    function AutocompleteComponent_section_9_div_6_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "div", 16);
        core.ɵɵtext(1);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var item_r13 = ctx.$implicit;
        core.ɵɵadvance(1);
        core.ɵɵtextInterpolate(item_r13);
    } }
    function AutocompleteComponent_section_9_div_12_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "div", 16);
        core.ɵɵtext(1);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var item_r14 = ctx.$implicit;
        core.ɵɵadvance(1);
        core.ɵɵtextInterpolate(item_r14);
    } }
    function AutocompleteComponent_section_9_div_18_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "div", 16);
        core.ɵɵtext(1);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var item_r15 = ctx.$implicit;
        core.ɵɵadvance(1);
        core.ɵɵtextInterpolate(item_r15);
    } }
    var _c0 = function (a0, a1) { return [a0, a1]; };
    function AutocompleteComponent_section_9_Template(rf, ctx) { if (rf & 1) {
        var _r17 = core.ɵɵgetCurrentView();
        core.ɵɵelementStart(0, "section", 9);
        core.ɵɵelementStart(1, "section", 10);
        core.ɵɵelementStart(2, "h2");
        core.ɵɵtext(3, "criteria");
        core.ɵɵelementEnd();
        core.ɵɵelementStart(4, "div", 11, 12);
        core.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_4_listener($event) { core.ɵɵrestoreView(_r17); var ctx_r16 = core.ɵɵnextContext(); return ctx_r16.drop($event); });
        core.ɵɵtemplate(6, AutocompleteComponent_section_9_div_6_Template, 2, 1, "div", 13);
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
        core.ɵɵelementStart(7, "section", 10);
        core.ɵɵelementStart(8, "h2");
        core.ɵɵtext(9, "asc");
        core.ɵɵelementEnd();
        core.ɵɵelementStart(10, "div", 11, 14);
        core.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_10_listener($event) { core.ɵɵrestoreView(_r17); var ctx_r18 = core.ɵɵnextContext(); return ctx_r18.drop($event); });
        core.ɵɵtemplate(12, AutocompleteComponent_section_9_div_12_Template, 2, 1, "div", 13);
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
        core.ɵɵelementStart(13, "section", 10);
        core.ɵɵelementStart(14, "h2");
        core.ɵɵtext(15, "desc");
        core.ɵɵelementEnd();
        core.ɵɵelementStart(16, "div", 11, 15);
        core.ɵɵlistener("cdkDropListDropped", function AutocompleteComponent_section_9_Template_div_cdkDropListDropped_16_listener($event) { core.ɵɵrestoreView(_r17); var ctx_r19 = core.ɵɵnextContext(); return ctx_r19.drop($event); });
        core.ɵɵtemplate(18, AutocompleteComponent_section_9_div_18_Template, 2, 1, "div", 13);
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var _r7 = core.ɵɵreference(5);
        var _r9 = core.ɵɵreference(11);
        var _r11 = core.ɵɵreference(17);
        var ctx_r3 = core.ɵɵnextContext();
        core.ɵɵproperty("appOpacityDuration", 1);
        core.ɵɵadvance(4);
        core.ɵɵproperty("cdkDropListData", ctx_r3.autocomplete.sortingOptions)("cdkDropListConnectedTo", core.ɵɵpureFunction2(10, _c0, _r9, _r11));
        core.ɵɵadvance(2);
        core.ɵɵproperty("ngForOf", ctx_r3.autocomplete.sortingOptions);
        core.ɵɵadvance(4);
        core.ɵɵproperty("cdkDropListData", ctx_r3.ascList)("cdkDropListConnectedTo", core.ɵɵpureFunction2(13, _c0, _r7, _r11));
        core.ɵɵadvance(2);
        core.ɵɵproperty("ngForOf", ctx_r3.ascList);
        core.ɵɵadvance(4);
        core.ɵɵproperty("cdkDropListData", ctx_r3.descList)("cdkDropListConnectedTo", core.ɵɵpureFunction2(16, _c0, _r7, _r9));
        core.ɵɵadvance(2);
        core.ɵɵproperty("ngForOf", ctx_r3.descList);
    } }
    function AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelement(0, "mat-progress-bar", 20);
    } }
    function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "h2");
        core.ɵɵtext(1);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var item_r25 = core.ɵɵnextContext().$implicit;
        core.ɵɵadvance(1);
        core.ɵɵtextInterpolate(item_r25.getContextToShow());
    } }
    function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelement(0, "mat-spinner", 26);
    } if (rf & 2) {
        core.ɵɵproperty("diameter", 20);
    } }
    function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template(rf, ctx) { if (rf & 1) {
        var _r36 = core.ɵɵgetCurrentView();
        core.ɵɵelementStart(0, "mat-list-item", 24);
        core.ɵɵlistener("click", function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template_mat_list_item_click_0_listener() { core.ɵɵrestoreView(_r36); var item_r25 = ctx.$implicit; var ctx_r35 = core.ɵɵnextContext(3); return ctx_r35.itemSelected(item_r25); });
        core.ɵɵtemplate(1, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_h2_1_Template, 2, 1, "h2", 7);
        core.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_mat_spinner_2_Template, 1, 1, "mat-spinner", 25);
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var item_r25 = ctx.$implicit;
        var observables_r20 = core.ɵɵnextContext(2).ngIf;
        core.ɵɵadvance(1);
        core.ɵɵproperty("ngIf", !observables_r20.isLoading && item_r25);
        core.ɵɵadvance(1);
        core.ɵɵproperty("ngIf", observables_r20.isLoading);
    } }
    function AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "cdk-virtual-scroll-viewport", 21);
        core.ɵɵelementStart(1, "mat-list", 22);
        core.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_mat_list_item_2_Template, 3, 2, "mat-list-item", 23);
        core.ɵɵelementEnd();
        core.ɵɵelementEnd();
    } if (rf & 2) {
        var observables_r20 = core.ɵɵnextContext().ngIf;
        var ctx_r22 = core.ɵɵnextContext();
        core.ɵɵproperty("appOpacityDuration", 1)("itemSize", observables_r20.totalItems)("height", observables_r20.totalItems * 60 > 200 ? 200 : observables_r20.totalItems * 60);
        core.ɵɵadvance(2);
        core.ɵɵproperty("cdkVirtualForOf", ctx_r22.autocomplete.pageDataSource)("cdkVirtualForTemplateCacheSize", 0);
    } }
    function AutocompleteComponent_ng_container_10_h2_3_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementStart(0, "h2", 27);
        core.ɵɵtext(1, "No data to display");
        core.ɵɵelementEnd();
    } if (rf & 2) {
        core.ɵɵproperty("appOpacityDuration", 1);
    } }
    function AutocompleteComponent_ng_container_10_Template(rf, ctx) { if (rf & 1) {
        core.ɵɵelementContainerStart(0);
        core.ɵɵtemplate(1, AutocompleteComponent_ng_container_10_mat_progress_bar_1_Template, 1, 0, "mat-progress-bar", 17);
        core.ɵɵtemplate(2, AutocompleteComponent_ng_container_10_cdk_virtual_scroll_viewport_2_Template, 3, 5, "cdk-virtual-scroll-viewport", 18);
        core.ɵɵtemplate(3, AutocompleteComponent_ng_container_10_h2_3_Template, 2, 1, "h2", 19);
        core.ɵɵelementContainerEnd();
    } if (rf & 2) {
        var observables_r20 = ctx.ngIf;
        var ctx_r4 = core.ɵɵnextContext();
        core.ɵɵadvance(1);
        core.ɵɵproperty("ngIf", ctx_r4.showScrollView && !observables_r20.hasFetchedData);
        core.ɵɵadvance(1);
        core.ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && observables_r20.hasDataToShow);
        core.ɵɵadvance(1);
        core.ɵɵproperty("ngIf", ctx_r4.showScrollView && observables_r20.hasFetchedData && !observables_r20.hasDataToShow);
    } }
    var _c1 = function (a0, a1, a2, a3) { return { hasFetchedData: a0, hasDataToShow: a1, isLoading: a2, totalItems: a3 }; };
    var AutocompleteComponent = /** @class */ (function () {
        function AutocompleteComponent(elRef) {
            var _this = this;
            this.elRef = elRef;
            this.showScrollView = false;
            this.sort = false;
            this.ascList = [];
            this.descList = [];
            this._previousCriteria = undefined;
            this.onFocus = function (el) {
                // console.log('focus on ', el);
                _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria);
                _this.showScrollView = true;
            };
            this.onModelChange = function (criteria, ngModel) {
                _this.autocomplete.criteria = _this.criteria;
                if (_this._timeoutForFetching)
                    clearTimeout(_this._timeoutForFetching);
                _this._timeoutForFetching = setTimeout(function () {
                    if (_this._previousCriteria !== _this.criteria) {
                        _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria, _this._getSortingParams());
                    }
                    _this._previousCriteria = _this.criteria;
                }, 500);
                _this.autocomplete.value = undefined;
                _this.autocomplete.valueSubject.next(_this.autocomplete.value);
                // console.log(this.autocomplete);
            };
            this.itemSelected = function (optionItem) {
                // console.log(optionItem);
                if (optionItem) {
                    // console.log(optionItem);
                    _this.autocomplete.criteria = optionItem.getContextToShow();
                    _this.autocomplete.value = optionItem;
                    _this.autocomplete.valueSubject.next(_this.autocomplete.value);
                    _this.criteria = _this.autocomplete.criteria;
                    _this.showScrollView = false;
                    _this.sort = false;
                    // console.log(this.autocomplete);
                }
            };
            this.onSlideToggleChange = function (matSlideToggleChange) {
                _this.sort = matSlideToggleChange.checked;
                // console.log(this.sort);
                if (!_this.sort)
                    _this.autocomplete.pageDataSource.resetAndFetch(_this.criteria, []);
            };
            this._getSortingParams = function () {
                var sortParams = [];
                _this.ascList.forEach(function (field) {
                    sortParams.push({ field: field, order: 'asc' });
                });
                _this.descList.forEach(function (field) {
                    sortParams.push({ field: field, order: 'desc' });
                });
                return sortParams;
            };
        }
        AutocompleteComponent.prototype.clickout = function (event) {
            if (this.elRef.nativeElement.contains(event.target)) {
                // console.log('focus on Autocomplete2Component');
            }
            else {
                // console.log('blur on Autocomplete2Component');
                this.showScrollView = false;
                this.sort = false;
            }
        };
        AutocompleteComponent.prototype.drop = function (event) {
            if (event.previousContainer === event.container) {
                dragDrop.moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            }
            else {
                dragDrop.transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
                this.autocomplete.pageDataSource.resetAndFetch(this.criteria, this._getSortingParams());
            }
        };
        AutocompleteComponent.prototype.ngOnInit = function () {
            this.criteria = this.autocomplete.criteria;
            this._previousCriteria = this.criteria;
            // console.log('Autocomplete2Component init');
        };
        AutocompleteComponent.prototype.ngOnDestroy = function () {
            if (this._timeoutForFetching)
                clearTimeout(this._timeoutForFetching);
            if (this.autocomplete.pageDataSource)
                this.autocomplete.pageDataSource.myDisconnect();
            // console.log('Autocomplete2Component destroyed');
        };
        AutocompleteComponent.ɵfac = function AutocompleteComponent_Factory(t) { return new (t || AutocompleteComponent)(core.ɵɵdirectiveInject(core.ElementRef)); };
        AutocompleteComponent.ɵcmp = core.ɵɵdefineComponent({ type: AutocompleteComponent, selectors: [["lib-autocomplete"]], hostBindings: function AutocompleteComponent_HostBindings(rf, ctx) { if (rf & 1) {
                core.ɵɵlistener("click", function AutocompleteComponent_click_HostBindingHandler($event) { return ctx.clickout($event); }, false, core.ɵɵresolveDocument);
            } }, inputs: { autocomplete: "autocomplete" }, decls: 15, vars: 22, consts: [["fxLayout", "column", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 1, "full-width"], ["fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "4vw", 1, "full-width"], ["fxFlex", "", "appOpacityTransition", "", 3, "appearance", "appOpacityDuration"], ["type", "text", "matInput", "", 3, "ngModel", "disabled", "autocomplete", "ngModelChange", "focus"], ["inputCriteria", "ngModel", "inputEl", ""], ["class", "slide-toggle", "appOpacityTransition", "", 3, "appOpacityDuration", "color", "checked", "disabled", "change", 4, "ngIf"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration", 4, "ngIf"], [4, "ngIf"], ["appOpacityTransition", "", 1, "slide-toggle", 3, "appOpacityDuration", "color", "checked", "disabled", "change"], ["appOpacityTransition", "", "fxLayout", "", "fxLayoutAlign", "center center", "fxLayoutGap", "2vw", 3, "appOpacityDuration"], ["fxLayout", "column", "fxLayoutAlign", "start center", 1, "example-container"], ["cdkDropList", "", 1, "example-list", 3, "cdkDropListData", "cdkDropListConnectedTo", "cdkDropListDropped"], ["criteriaDropList", "cdkDropList"], ["class", "example-box", "cdkDrag", "", 4, "ngFor", "ngForOf"], ["ascDropList", "cdkDropList"], ["descDropList", "cdkDropList"], ["cdkDrag", "", 1, "example-box"], ["mode", "indeterminate", 4, "ngIf"], ["appOpacityTransition", "", "class", "my-viewport", "appHeight", "", 3, "appOpacityDuration", "itemSize", "height", 4, "ngIf"], ["appOpacityTransition", "", 3, "appOpacityDuration", 4, "ngIf"], ["mode", "indeterminate"], ["appOpacityTransition", "", "appHeight", "", 1, "my-viewport", 3, "appOpacityDuration", "itemSize", "height"], ["role", "list"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click", 4, "cdkVirtualFor", "cdkVirtualForOf", "cdkVirtualForTemplateCacheSize"], ["role", "listitem", "fxLayout", "", "fxLayoutAlign", "start center", 3, "click"], [3, "diameter", 4, "ngIf"], [3, "diameter"], ["appOpacityTransition", "", 3, "appOpacityDuration"]], template: function AutocompleteComponent_Template(rf, ctx) { if (rf & 1) {
                var _r39 = core.ɵɵgetCurrentView();
                core.ɵɵelementStart(0, "section", 0);
                core.ɵɵelementStart(1, "section", 1);
                core.ɵɵelementStart(2, "mat-form-field", 2);
                core.ɵɵelementStart(3, "mat-label");
                core.ɵɵtext(4);
                core.ɵɵelementEnd();
                core.ɵɵelementStart(5, "input", 3, 4);
                core.ɵɵlistener("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { return ctx.criteria = $event; })("ngModelChange", function AutocompleteComponent_Template_input_ngModelChange_5_listener($event) { core.ɵɵrestoreView(_r39); var _r0 = core.ɵɵreference(6); return ctx.onModelChange($event, _r0); })("focus", function AutocompleteComponent_Template_input_focus_5_listener() { core.ɵɵrestoreView(_r39); var _r1 = core.ɵɵreference(7); return ctx.onFocus(_r1); });
                core.ɵɵelementEnd();
                core.ɵɵelementEnd();
                core.ɵɵtemplate(8, AutocompleteComponent_mat_slide_toggle_8_Template, 2, 4, "mat-slide-toggle", 5);
                core.ɵɵelementEnd();
                core.ɵɵtemplate(9, AutocompleteComponent_section_9_Template, 19, 19, "section", 6);
                core.ɵɵtemplate(10, AutocompleteComponent_ng_container_10_Template, 4, 3, "ng-container", 7);
                core.ɵɵpipe(11, "async");
                core.ɵɵpipe(12, "async");
                core.ɵɵpipe(13, "async");
                core.ɵɵpipe(14, "async");
                core.ɵɵelementEnd();
            } if (rf & 2) {
                core.ɵɵadvance(2);
                core.ɵɵproperty("appearance", ctx.autocomplete.appearance ? ctx.autocomplete.appearance : "fill")("appOpacityDuration", 1);
                core.ɵɵadvance(2);
                core.ɵɵtextInterpolate(ctx.autocomplete.inputPlaceHolder);
                core.ɵɵadvance(1);
                core.ɵɵproperty("ngModel", ctx.criteria)("disabled", ctx.autocomplete.disabled)("autocomplete", "off");
                core.ɵɵadvance(3);
                core.ɵɵproperty("ngIf", ctx.showScrollView && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
                core.ɵɵadvance(1);
                core.ɵɵproperty("ngIf", ctx.showScrollView && ctx.sort && (ctx.autocomplete.sortingOptions.length > 0 || ctx.ascList.length > 0 || ctx.descList.length > 0));
                core.ɵɵadvance(1);
                core.ɵɵproperty("ngIf", core.ɵɵpureFunction4(17, _c1, core.ɵɵpipeBind1(11, 9, ctx.autocomplete.pageDataSource.getHasFetchedDataSubject()), core.ɵɵpipeBind1(12, 11, ctx.autocomplete.pageDataSource.getHasDataToShowSubject()), core.ɵɵpipeBind1(13, 13, ctx.autocomplete.pageDataSource.isLoading()), core.ɵɵpipeBind1(14, 15, ctx.autocomplete.pageDataSource.getTotalItemsSubject())));
            } }, directives: [flex.DefaultLayoutDirective, flex.DefaultLayoutAlignDirective, flex.DefaultLayoutGapDirective, formField.MatFormField, flex.DefaultFlexDirective, OpacityTransitionDirective, formField.MatLabel, input.MatInput, forms.DefaultValueAccessor, forms.NgControlStatus, forms.NgModel, common.NgIf, slideToggle.MatSlideToggle, dragDrop.CdkDropList, common.NgForOf, dragDrop.CdkDrag, progressBar.MatProgressBar, scrolling.CdkVirtualScrollViewport, scrolling.CdkFixedSizeVirtualScroll, HeightDirective, list.MatList, scrolling.CdkVirtualForOf, list.MatListItem, progressSpinner.MatSpinner], pipes: [common.AsyncPipe], styles: [".my-viewport[_ngcontent-%COMP%] {\n        height: 150px;\n        width: 100%;\n    }\n\n    mat-form-field[_ngcontent-%COMP%] {\n        min-width: 250px;\n    }\n\n    h2[_ngcontent-%COMP%] {\n        font: inherit;\n    }\n\n    mat-list-item[_ngcontent-%COMP%] {\n        cursor: pointer;\n    }\n\n    cdk-virtual-scroll-viewport[_ngcontent-%COMP%] {\n        box-shadow: 1px 2px 3px grey;\n    }\n\n    .full-width[_ngcontent-%COMP%] {\n        width: 100%;\n    }\n\n    \n\n    .example-container[_ngcontent-%COMP%] {\n        \n        margin: 0 25px 25px 0;\n        display: inline-block;\n        vertical-align: top;\n    }\n\n    .example-list[_ngcontent-%COMP%] {\n        border: solid 1px #ccc;\n        min-height: 60px;\n        min-width: 60px;\n        background: white;\n        border-radius: 4px;\n        overflow: hidden;\n        display: block;\n    }\n\n    .example-box[_ngcontent-%COMP%] {\n        padding: 20px 10px;\n        border-bottom: solid 1px #ccc;\n        color: rgba(0, 0, 0, 0.87);\n        display: flex;\n        flex-direction: row;\n        align-items: center;\n        justify-content: space-between;\n        box-sizing: border-box;\n        cursor: move;\n        background: white;\n        font-size: 14px;\n    }\n\n    .cdk-drag-preview[_ngcontent-%COMP%] {\n        box-sizing: border-box;\n        border-radius: 4px;\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\n    }\n\n    .cdk-drag-placeholder[_ngcontent-%COMP%] {\n        opacity: 0;\n    }\n\n    .cdk-drag-animating[_ngcontent-%COMP%] {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }\n\n    .example-box[_ngcontent-%COMP%]:last-child {\n        border: none;\n    }\n\n    .example-list.cdk-drop-list-dragging[_ngcontent-%COMP%]   .example-box[_ngcontent-%COMP%]:not(.cdk-drag-placeholder) {\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n    }"], changeDetection: 0 });
        return AutocompleteComponent;
    }());
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(AutocompleteComponent, [{
            type: core.Component,
            args: [{
                    selector: 'lib-autocomplete',
                    templateUrl: './autocomplete.component.html',
                    styles: [],
                    changeDetection: core.ChangeDetectionStrategy.OnPush,
                }]
        }], function () { return [{ type: core.ElementRef }]; }, { autocomplete: [{
                type: core.Input
            }], clickout: [{
                type: core.HostListener,
                args: ['document:click', ['$event']]
            }] }); })();

    var material = [
        formField.MatFormFieldModule,
        autocomplete.MatAutocompleteModule,
        slideToggle.MatSlideToggleModule,
        input.MatInputModule,
        list.MatListModule,
        progressSpinner.MatProgressSpinnerModule,
        progressBar.MatProgressBarModule,
    ];
    var MaterialModule = /** @class */ (function () {
        function MaterialModule() {
        }
        MaterialModule.ɵmod = core.ɵɵdefineNgModule({ type: MaterialModule });
        MaterialModule.ɵinj = core.ɵɵdefineInjector({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [__spread(material), formField.MatFormFieldModule,
                autocomplete.MatAutocompleteModule,
                slideToggle.MatSlideToggleModule,
                input.MatInputModule,
                list.MatListModule,
                progressSpinner.MatProgressSpinnerModule,
                progressBar.MatProgressBarModule] });
        return MaterialModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(MaterialModule, { imports: [formField.MatFormFieldModule,
            autocomplete.MatAutocompleteModule,
            slideToggle.MatSlideToggleModule,
            input.MatInputModule,
            list.MatListModule,
            progressSpinner.MatProgressSpinnerModule,
            progressBar.MatProgressBarModule], exports: [formField.MatFormFieldModule,
            autocomplete.MatAutocompleteModule,
            slideToggle.MatSlideToggleModule,
            input.MatInputModule,
            list.MatListModule,
            progressSpinner.MatProgressSpinnerModule,
            progressBar.MatProgressBarModule] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(MaterialModule, [{
            type: core.NgModule,
            args: [{
                    imports: __spread(material),
                    exports: __spread(material),
                }]
        }], null, null); })();

    var SharedModule = /** @class */ (function () {
        function SharedModule() {
        }
        SharedModule.ɵmod = core.ɵɵdefineNgModule({ type: SharedModule });
        SharedModule.ɵinj = core.ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
                    common.CommonModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule,
                    MaterialModule,
                    flexLayout.FlexLayoutModule,
                    scrolling.ScrollingModule,
                    dragDrop.DragDropModule,
                ],
                common.CommonModule,
                forms.FormsModule,
                forms.ReactiveFormsModule,
                MaterialModule,
                flexLayout.FlexLayoutModule,
                scrolling.ScrollingModule,
                dragDrop.DragDropModule] });
        return SharedModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(SharedModule, { declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective], imports: [common.CommonModule,
            forms.FormsModule,
            forms.ReactiveFormsModule,
            MaterialModule,
            flexLayout.FlexLayoutModule,
            scrolling.ScrollingModule,
            dragDrop.DragDropModule], exports: [HeightDirective,
            WidthDirective,
            OpacityTransitionDirective,
            common.CommonModule,
            forms.FormsModule,
            forms.ReactiveFormsModule,
            MaterialModule,
            flexLayout.FlexLayoutModule,
            scrolling.ScrollingModule,
            dragDrop.DragDropModule] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(SharedModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [HeightDirective, WidthDirective, OpacityTransitionDirective],
                    imports: [
                        common.CommonModule,
                        forms.FormsModule,
                        forms.ReactiveFormsModule,
                        MaterialModule,
                        flexLayout.FlexLayoutModule,
                        scrolling.ScrollingModule,
                        dragDrop.DragDropModule,
                    ],
                    exports: [
                        HeightDirective,
                        WidthDirective,
                        OpacityTransitionDirective,
                        common.CommonModule,
                        forms.FormsModule,
                        forms.ReactiveFormsModule,
                        MaterialModule,
                        flexLayout.FlexLayoutModule,
                        scrolling.ScrollingModule,
                        dragDrop.DragDropModule,
                    ],
                    entryComponents: [],
                }]
        }], null, null); })();

    var AutocompleteModule = /** @class */ (function () {
        function AutocompleteModule() {
        }
        AutocompleteModule.ɵmod = core.ɵɵdefineNgModule({ type: AutocompleteModule });
        AutocompleteModule.ɵinj = core.ɵɵdefineInjector({ factory: function AutocompleteModule_Factory(t) { return new (t || AutocompleteModule)(); }, imports: [[
                    platformBrowser.BrowserModule,
                    animations.BrowserAnimationsModule,
                    http.HttpClientModule,
                    SharedModule,
                ]] });
        return AutocompleteModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core.ɵɵsetNgModuleScope(AutocompleteModule, { declarations: [AutocompleteComponent], imports: [platformBrowser.BrowserModule,
            animations.BrowserAnimationsModule,
            http.HttpClientModule,
            SharedModule], exports: [AutocompleteComponent] }); })();
    /*@__PURE__*/ (function () { core.ɵsetClassMetadata(AutocompleteModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [AutocompleteComponent],
                    imports: [
                        platformBrowser.BrowserModule,
                        animations.BrowserAnimationsModule,
                        http.HttpClientModule,
                        SharedModule,
                    ],
                    exports: [AutocompleteComponent],
                }]
        }], null, null); })();

    exports.AutocompleteComponent = AutocompleteComponent;
    exports.AutocompleteModule = AutocompleteModule;
    exports.AutocompleteService = AutocompleteService;
    exports.HeightDirective = HeightDirective;
    exports.MaterialModule = MaterialModule;
    exports.Observable = Observable;
    exports.OpacityTransitionDirective = OpacityTransitionDirective;
    exports.PageDataSource = PageDataSource;
    exports.PageMap = PageMap;
    exports.PageableReadService = PageableReadService;
    exports.SharedModule = SharedModule;
    exports.WidthDirective = WidthDirective;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=autocomplete.umd.js.map
